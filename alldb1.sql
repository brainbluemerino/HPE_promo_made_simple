-- MySQL dump 10.13  Distrib 5.7.19, for Win64 (x86_64)
--
-- Host: localhost    Database: 
-- ------------------------------------------------------
-- Server version	5.7.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `cloud_configurator`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `cloud_configurator` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `cloud_configurator`;

--
-- Table structure for table `lenguaje`
--

DROP TABLE IF EXISTS `lenguaje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lenguaje` (
  `Id_lenguaje` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`Id_lenguaje`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lenguaje`
--

LOCK TABLES `lenguaje` WRITE;
/*!40000 ALTER TABLE `lenguaje` DISABLE KEYS */;
INSERT INTO `lenguaje` VALUES (1,'Español'),(2,'Inglés'),(3,'Portugués');
/*!40000 ALTER TABLE `lenguaje` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persons`
--

DROP TABLE IF EXISTS `persons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persons` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(255) NOT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `activo` int(11) DEFAULT NULL COMMENT '1 = activo 0 = inactivo',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persons`
--

LOCK TABLES `persons` WRITE;
/*!40000 ALTER TABLE `persons` DISABLE KEYS */;
INSERT INTO `persons` VALUES (1,'sapadmin','admin',1);
/*!40000 ALTER TABLE `persons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `solicitud`
--

DROP TABLE IF EXISTS `solicitud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `solicitud` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `producto` varchar(255) DEFAULT NULL,
  `ayuda` varchar(500) DEFAULT NULL,
  `tipo_industria` varchar(100) DEFAULT NULL,
  `tamanio` varchar(100) DEFAULT NULL,
  `factura_anual` varchar(100) DEFAULT NULL,
  `solucion` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=434 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `solicitud`
--

LOCK TABLES `solicitud` WRITE;
/*!40000 ALTER TABLE `solicitud` DISABLE KEYS */;
INSERT INTO `solicitud` VALUES (1,'ERP','Como responsable de la integración financiera de mi empresa quiero tener visibilidad en tiempo real.','Comercio Minorista',NULL,NULL,'buttonCard22'),(2,'ERP','Como responsable de los procesos operativos quiero alacanzar una mayor eficiencia.','Productos de Consumo',NULL,NULL,'Cloud'),(3,'ERP','Como responsable de la integración financiera de mi empresa quiero tener visibilidad en tiempo real.','Servicios Financieros',NULL,NULL,'On Premise'),(4,'ERP',NULL,NULL,NULL,NULL,NULL),(5,'CRM',NULL,NULL,NULL,NULL,NULL),(6,'ERP','Como responsable de los procesos operativos quiero alacanzar una mayor eficiencia.',NULL,NULL,NULL,NULL),(7,'ERP',NULL,NULL,NULL,NULL,NULL),(8,'CRM',NULL,NULL,NULL,NULL,NULL),(9,'ERP','Como responsable de los procesos operativos quiero alacanzar una mayor eficiencia.','Servicios Financieros',NULL,NULL,'Híbrida'),(10,'ERP','Como responsable del crecimiento de la empresa quiero lograr máxima rentabilidad.','Servicios Financieros',NULL,NULL,'Híbrida'),(11,'ERP','Como responsable de los procesos operativos quiero alacanzar una mayor eficiencia.','Comercio Mayorista',NULL,NULL,'Híbrida'),(12,'ERP','Como responsable de los procesos operativos quiero alacanzar una mayor eficiencia.',NULL,NULL,NULL,NULL),(13,'ERP','Como responsable de los procesos operativos quiero alacanzar una mayor eficiencia.',NULL,NULL,NULL,NULL),(14,'ERP','Como responsable de los procesos operativos quiero alacanzar una mayor eficiencia.',NULL,NULL,NULL,NULL),(15,'ERP','Como responsable de los procesos operativos quiero alacanzar una mayor eficiencia.','Servicios Profesionales','0 - 100','30 - 100 Millones de dólares','On Premise'),(16,'ERP','Como responsable de la integración financiera de mi empresa quiero tener visibilidad en tiempo real.',NULL,NULL,NULL,NULL),(17,'ERP','Como responsable de los procesos operativos quiero alacanzar una mayor eficiencia.',NULL,NULL,NULL,NULL),(18,'ERP','Como responsable de los procesos operativos quiero alacanzar una mayor eficiencia.',NULL,NULL,NULL,NULL),(19,'ERP','Como responsable de los procesos operativos quiero alacanzar una mayor eficiencia.',NULL,NULL,NULL,NULL),(20,'ERP','Como responsable de la integración financiera de mi empresa quiero tener visibilidad en tiempo real.',NULL,NULL,NULL,NULL),(21,'ERP','Como responsable de la integración financiera de mi empresa quiero tener visibilidad en tiempo real.',NULL,NULL,NULL,NULL),(22,'ERP','Como responsable de la integración financiera de mi empresa quiero tener visibilidad en tiempo real.',NULL,NULL,NULL,NULL),(23,'ERP','Como responsable de los procesos operativos quiero alacanzar una mayor eficiencia.','Comercio Minorista','0 - 100','10 - 30 Millones de dólares','On Premise'),(24,'ERP','Como responsable de la integración financiera de mi empresa quiero tener visibilidad en tiempo real.','Servicios Financieros','0 - 100','10 - 30 Millones de dólares','On Premise'),(25,'ERP','Como responsable de la integración financiera de mi empresa quiero tener visibilidad en tiempo real.','Servicios Financieros','0 - 100','30 - 100 Millones de dólares','Cloud'),(26,'ERP','Como responsable de la integración financiera de mi empresa quiero tener visibilidad en tiempo real.','Comercio Minorista','0 - 100','30 - 100 Millones de dólares','On Premise'),(27,'ERP','Como responsable de la integración financiera de mi empresa quiero tener visibilidad en tiempo real.','Servicios Financieros','0 - 100','30 - 100 Millones de dólares','On Premise'),(28,'ERP','Como responsable de los procesos operativos quiero alacanzar una mayor eficiencia.','Comercio Mayorista','0 - 100','100 Millones de dólares a más','Cloud'),(29,'ERP','Como responsable de los procesos operativos quiero alacanzar una mayor eficiencia.','Comercio Minorista','0 - 100','30 - 100 Millones de dólares','Cloud'),(30,'ERP','Como responsable del crecimiento de la empresa quiero lograr máxima rentabilidad.','Servicios Financieros','0 - 100','100 Millones de dólares a más','On Premise'),(31,'ERP','Como responsable del crecimiento de la empresa quiero lograr máxima rentabilidad.','Servicios Profesionales','0 - 100','100 Millones de dólares a más','On Premise'),(32,'ERP','Como responsable del crecimiento de la empresa quiero lograr máxima rentabilidad.','Servicios Financieros','0 - 100','30 - 100 Millones de dólares','On Premise'),(33,'ERP','Como responsable de los procesos operativos quiero alacanzar una mayor eficiencia.','Servicios Financieros','0 - 100','30 - 100 Millones de dólares','On Premise'),(34,'CRM','Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.','Comercio Minorista','0 - 100','10 - 30 Millones de dólares','Cloud'),(35,'ERP','Como responsable del crecimiento de la empresa quiero lograr máxima rentabilidad.','Servicios Financieros','0 - 100','30 - 100 Millones de dólares','On Premise'),(36,'ERP','Como responsable de la integración financiera de mi empresa quiero tener visibilidad en tiempo real.','Servicios Financieros','0 - 100','30 - 100 Millones de dólares','On Premise'),(37,'ERP','Como responsable de la integración financiera de mi empresa quiero tener visibilidad en tiempo real.','Servicios Financieros','0 - 100','100 Millones de dólares a más','On Premise'),(38,'ERP','Como responsable del crecimiento de la empresa quiero lograr máxima rentabilidad.','Servicios Financieros','0 - 100','30 - 100 Millones de dólares','On Premise'),(39,'ERP','Como responsable de la integración financiera de mi empresa quiero tener visibilidad en tiempo real.','Servicios Financieros','0 - 100','100 Millones de dólares a más','On Premise'),(40,'CRM','Como responsable de ventas quiero mejorar mi proceso de ventas.','Servicios Profesionales','0 - 100','30 - 100 Millones de dólares','Cloud'),(41,'ERP','Como responsable de la integración financiera de mi empresa quiero tener visibilidad en tiempo real.','Servicios Financieros','0 - 100','100 Millones de dólares a más','On Premise'),(42,'ERP','Como responsable de la integración financiera de mi empresa quiero tener visibilidad en tiempo real.','Comercio Minorista','0 - 100','30 - 100 Millones','Cloud'),(43,'ERP','Como responsable de los procesos operativos quiero alacanzar una mayor eficiencia.','Servicios Financieros','0 - 100','100 Millones a más','On Premise'),(44,'ERP','Como responsable de la integración financiera de mi empresa quiero tener visibilidad en tiempo real.','Servicios Financieros','0 - 100','100 Millones a más','On Premise'),(45,'ERP','Como responsable de los procesos operativos quiero alacanzar una mayor eficiencia.','Servicios Financieros','0 - 100','30 - 100 Millones','On Premise'),(46,'ERP','Como responsable de los procesos operativos quiero alacanzar una mayor eficiencia.','Comercio Minorista','0 - 100','100 Millones a más','On Premise'),(47,'ERP','Como responsable de los procesos operativos quiero alacanzar una mayor eficiencia.','Servicios Financieros','0 - 100','30 - 100 Millones','On Premise'),(48,'HR','Como responsable de HR quiero consolidar y optimizar los procesos centrales de RRHH.',NULL,NULL,NULL,NULL),(49,'CRM','Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.',NULL,NULL,NULL,NULL),(50,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.','cocina','0 - 100','30 - 100 Millones',NULL),(51,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.',NULL,NULL,NULL,NULL),(52,'CRM','Como responsable de ventas quiero mejorar mi proceso de ventas.',NULL,NULL,NULL,NULL),(53,'','',NULL,NULL,NULL,NULL),(54,'ERP','Como responsable de los procesos operativos quiero alacanzar una mayor eficiencia.',NULL,NULL,NULL,NULL),(55,'CRM','Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.',NULL,NULL,NULL,NULL),(56,'CRM','Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.',NULL,NULL,NULL,NULL),(57,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.',NULL,NULL,NULL,NULL),(58,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.',NULL,NULL,NULL,NULL),(59,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.',NULL,NULL,NULL,NULL),(60,'CRM','Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.','','Seleccione','',NULL),(61,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.',NULL,NULL,NULL,NULL),(62,'CRM','Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.',NULL,NULL,NULL,NULL),(63,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.',NULL,NULL,NULL,NULL),(64,'CRM','','','Seleccione','',''),(65,'CRM',NULL,NULL,NULL,NULL,NULL),(66,'CRM','Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.',NULL,NULL,NULL,NULL),(67,'CRM','Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.',NULL,NULL,NULL,NULL),(68,'CRM',NULL,NULL,NULL,NULL,NULL),(69,'CRM','Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.',NULL,NULL,NULL,''),(70,'CRM','Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.',NULL,NULL,NULL,''),(71,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.',NULL,NULL,NULL,NULL),(72,'CRM','Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.','','0 - 100','30 - 100 Millones','On Premise'),(73,'CRM','Como responsable de ventas quiero mejorar mi proceso de ventas.','','0 - 100','30 - 100 Millones','On Premise'),(74,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.','','0 - 100','30 - 100 Millones',NULL),(75,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.','','0 - 100','30 - 100 Millones',NULL),(76,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.','','0 - 100','30 - 100 Millones',NULL),(77,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.','','0 - 100','30 - 100 Millones',''),(78,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.','','0 - 100','10 - 30 Millones',''),(79,'CRM','Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.','aaa','0 - 100','30 - 100 Millones',NULL),(80,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.','','0 - 100','30 - 100 Millones',''),(81,'CRM','Como responsable de ventas quiero mejorar mi proceso de ventas.','aaaaa','0 - 100','30 - 100 Millones',NULL),(82,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.','aaa','0 - 100','30 - 100 Millones',NULL),(83,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.','aaaa','0 - 100','30 - 100 Millones',NULL),(84,'CRM','Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.','mmm','0 - 100','30 - 100 Millones',NULL),(85,'CRM','Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.','mmmm','0 - 100','30 - 100 Millones',NULL),(86,'CRM','Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.','aaaaa','0 - 100','30 - 100 Millones',NULL),(87,'ERP','Como responsable de los procesos operativos quiero alacanzar una mayor eficiencia.','mmmm','0 - 100','30 - 100 Millones',NULL),(88,'CRM','Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.','mmm','0 - 100','30 - 100 Millones','Cloud'),(89,'CRM','Como responsable de ventas quiero mejorar mi proceso de ventas.','mmm','0 - 100','30 - 100 Millones','On Premise'),(90,'ERP','Como responsable de la integración financiera de mi empresa quiero tener visibilidad en tiempo real.','Servicios Financieros','0 - 100','10 - 30 Millones',''),(91,'CRM','Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.','Comercio Minorista','0 - 100','30 - 100 Millones',NULL),(92,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.','Comercio Mayorista','0 - 100','30 - 100 Millones','Cloud'),(93,'ERP','Como responsable de la integración financiera de mi empresa quiero tener visibilidad en tiempo real.','Comercio Mayorista','0 - 100','30 - 100 Millones','On Premise'),(94,'ERP','Como responsable de la integración financiera de mi empresa quiero tener visibilidad en tiempo real.','Comercio Minorista','0 - 100','100 Millones a más','On Premise'),(95,'ERP','Como responsable de los procesos operativos quiero alacanzar una mayor eficiencia.','Servicios Profesionales','0 - 100','30 - 100 Millones','On Premise'),(96,'ERP','Como responsable del crecimiento de la empresa quiero lograr máxima rentabilidad.','Comercio Mayorista','0 - 100','100 Millones a más','Cloud'),(97,'ERP','Como responsable del crecimiento de la empresa quiero lograr máxima rentabilidad.',NULL,NULL,NULL,NULL),(98,'ERP','Como responsable de los procesos operativos quiero alacanzar una mayor eficiencia.',NULL,NULL,NULL,NULL),(99,'ERP','Como responsable de los procesos operativos quiero alacanzar una mayor eficiencia.','Productos de Consumo','0 - 100','30 - 100 Millones','On Premise'),(100,'HR','Como responsable de HR quiero desarrollar a mis empleados de una manera simple y colaborativa.',NULL,NULL,NULL,NULL),(101,'HR','Como responsable de HR quiero consolidar y optimizar los procesos centrales de RRHH.',NULL,NULL,NULL,NULL),(102,'CRM','Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.',NULL,NULL,NULL,NULL),(103,'CRM','Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.',NULL,NULL,NULL,NULL),(104,'CRM','Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.',NULL,NULL,NULL,NULL),(105,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.','Servicios Financieros','0 - 100','30 - 100 Millones','Cloud'),(106,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.','Comercio Mayorista','0 - 100','30 - 100 Millones','Cloud'),(107,'HR',NULL,NULL,NULL,NULL,NULL),(108,'HR',NULL,NULL,NULL,NULL,NULL),(109,'HR',NULL,NULL,NULL,NULL,NULL),(110,'HR',NULL,NULL,NULL,NULL,NULL),(111,'HR',NULL,NULL,NULL,NULL,NULL),(112,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.',NULL,NULL,NULL,NULL),(113,'ERP','Como responsable del crecimiento de la empresa quiero lograr máxima rentabilidad.',NULL,NULL,NULL,NULL),(114,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.','Comercio Minorista','0 - 100','100 Millones a más','Cloud'),(115,'CRM','Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.','Comercio Minorista','0 - 100','100 Millones a más','Cloud'),(116,'CRM','Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.','Servicios Financieros','0 - 100','10 - 30 Millones','Cloud'),(117,'ERP','Como responsable de la integración financiera de mi empresa quiero tener visibilidad en tiempo real.','Comercio Mayorista','0 - 100','30 - 100 Millones','On Premise'),(118,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.','Servicios Profesionales','0 - 100','30 - 100 Millones','Híbrida'),(119,'CRM','Como responsable de ventas quiero mejorar mi proceso de ventas.','Servicios Profesionales','0 - 100','30 - 100 Millones','On Premise'),(120,'ERP','Como responsable del crecimiento de la empresa quiero lograr máxima rentabilidad.','Comercio Minorista','0 - 100','30 - 100 Millones','Híbrida'),(121,'ERP','Como responsable de la integración financiera de mi empresa quiero tener visibilidad en tiempo real.','Servicios Profesionales','0 - 100','30 - 100 Millones',NULL),(122,'ERP','Como responsable de la integración financiera de mi empresa quiero tener visibilidad en tiempo real.','Servicios Financieros','0 - 100','30 - 100 Millones','Cloud'),(123,'ERP','Como responsable de la integración financiera de mi empresa quiero tener visibilidad en tiempo real.','Servicios Profesionales','0 - 100','30 - 100 Millones','On Premise'),(124,'ERP','Como responsable del crecimiento de la empresa quiero lograr máxima rentabilidad.','Servicios Financieros','0 - 100','10 - 30 Millones',NULL),(125,'ERP','Como responsable del crecimiento de la empresa quiero lograr máxima rentabilidad.','Servicios Financieros','0 - 100','10 - 30 Millones','Cloud'),(126,'ERP','Como responsable del crecimiento de la empresa quiero lograr máxima rentabilidad.','Servicios Profesionales','0 - 100','10 - 30 Millones','Híbrida'),(127,'ERP','Como responsable del crecimiento de la empresa quiero lograr máxima rentabilidad.','Servicios Profesionales','0 - 100','30 - 100 Millones','On Premise'),(128,'ERP','Como responsable del crecimiento de la empresa quiero lograr máxima rentabilidad.','Servicios Financieros','0 - 100','30 - 100 Millones','On Premise'),(129,'ERP','Como responsable de la integración financiera de mi empresa quiero tener visibilidad en tiempo real.','Servicios Profesionales','0 - 100','30 - 100 Millones','On Premise'),(130,'ERP',NULL,NULL,NULL,NULL,NULL),(131,'ERP',NULL,NULL,NULL,NULL,NULL),(132,'ERP','Como responsable de los procesos operativos quiero alacanzar una mayor eficiencia.','Comercio Mayorista','0 - 100','30 - 100 Millones','Cloud'),(133,'ERP','Como responsable del crecimiento de la empresa quiero lograr máxima rentabilidad.','Servicios Profesionales','0 - 100','30 - 100 Millones','On Premise'),(134,'ERP','Como responsable del crecimiento de la empresa quiero lograr máxima rentabilidad.','Comercio Minorista','0 - 100','30 - 100 Millones','Híbrida'),(135,'ERP','Como responsable del crecimiento de la empresa quiero lograr máxima rentabilidad.','Servicios Profesionales','0 - 100','10 - 30 Millones','On Premise'),(136,'ERP','Como responsable de la integración financiera de mi empresa quiero tener visibilidad en tiempo real.','Servicios Profesionales','0 - 100','30 - 100 Millones','On Premise'),(137,'ERP',NULL,NULL,NULL,NULL,NULL),(138,'CRM','Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.','Servicios Financieros','0 - 100','30 - 100 Millones','On Premise'),(139,'ERP','Como responsable de la integración financiera de mi empresa quiero tener visibilidad en tiempo real.','Servicios Profesionales','0 - 100','10 - 30 Millones',NULL),(140,'CRM',NULL,NULL,NULL,NULL,NULL),(141,'HR','Como responsable de los procesos operativos quiero alacanzar una mayor eficiencia.','Servicios Profesionales','0 - 100','10 - 30 Millones','Cloud'),(142,'ERP','Como responsable de los procesos operativos quiero alacanzar una mayor eficiencia.','Servicios Profesionales','100 - 500','100 Millones a más','On Premise'),(143,'CRM',NULL,NULL,NULL,NULL,NULL),(144,'CRM',NULL,NULL,NULL,NULL,NULL),(145,'CRM',NULL,NULL,NULL,NULL,NULL),(146,'CRM',NULL,NULL,NULL,NULL,NULL),(147,'CRM',NULL,NULL,NULL,NULL,NULL),(148,'CRM',NULL,NULL,NULL,NULL,NULL),(149,'CRM',NULL,NULL,NULL,NULL,NULL),(150,'CRM',NULL,NULL,NULL,NULL,NULL),(151,'CRM',NULL,NULL,NULL,NULL,NULL),(152,'CRM',NULL,NULL,NULL,NULL,NULL),(153,'HR',NULL,NULL,NULL,NULL,NULL),(154,'CRM',NULL,NULL,NULL,NULL,NULL),(155,'CRM','Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.','Productos de Consumo','0 - 100','No poseo información','Híbrida'),(156,'ERP',NULL,NULL,NULL,NULL,NULL),(157,'HR',NULL,NULL,NULL,NULL,NULL),(158,'CRM',NULL,NULL,NULL,NULL,NULL),(159,'CRM',NULL,NULL,NULL,NULL,NULL),(160,'HR',NULL,NULL,NULL,NULL,NULL),(161,'CRM',NULL,NULL,NULL,NULL,NULL),(162,'CRM',NULL,NULL,NULL,NULL,NULL),(163,'HR',NULL,NULL,NULL,NULL,NULL),(164,'HR',NULL,NULL,NULL,NULL,NULL),(165,'ERP',NULL,NULL,NULL,NULL,NULL),(166,'HR',NULL,NULL,NULL,NULL,NULL),(167,'HR',NULL,NULL,NULL,NULL,NULL),(168,'HR',NULL,NULL,NULL,NULL,NULL),(169,'HR',NULL,NULL,NULL,NULL,NULL),(170,'HR',NULL,NULL,NULL,NULL,NULL),(171,'HR','Quiero retener a los mejores talentos con un plan de compensación atractivo. Quiero desarrollar a mis empleados de una manera simple y colaborativa.,Quiero consolidar y optimizar los procesos centrales de RRHH.','Comercio Minorista','0 - 100','No poseo información','Cloud'),(172,'HR',NULL,NULL,NULL,NULL,NULL),(173,'HR',NULL,NULL,NULL,NULL,NULL),(174,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa. Quiero planificar el desarrollo y sucesión de mis mejores talentos','Comercio Minorista','0 - 100','10 - 30 Millones','Cloud'),(175,'CRM',NULL,NULL,NULL,NULL,NULL),(176,'CRM',NULL,NULL,NULL,NULL,NULL),(177,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente. Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.','Servicios Financieros','0 - 100','30 - 100 Millones','On Premise'),(178,'HR','Quiero retener a los mejores talentos con un plan de compensación atractivo.','Productos de Consumo','0 - 100','30 - 100 Millones','Cloud'),(179,'HR',NULL,NULL,NULL,NULL,NULL),(180,'HR',NULL,NULL,NULL,NULL,NULL),(181,'HR','','Productos de Consumo','0 - 100','10 - 30 Millones','Cloud'),(182,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.','Mineria','100 - 500','30 - 100 Millones','Cloud'),(183,'HR','Quiero consolidar y optimizar los procesos centrales de RRHH. Quiero alinear las metas de la empresa con la recompensa de mis empleados.','Servicios Profesionales','0 - 100','No poseo información','Híbrida'),(184,'HR','Quiero consolidar y optimizar los procesos centrales de RRHH. Quiero desarrollar a mis empleados de una manera simple y colaborativa. Quiero alinear las metas de la empresa con la recompensa de mis empleados.','Servicios Profesionales','0 - 100','No poseo información','Cloud'),(185,'CRM','Como responsable de ventas quiero mejorar mi proceso de ventas. Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.','Manufactura','0 - 100','No poseo información','Híbrida'),(186,'HR','Quiero consolidar y optimizar los procesos centrales de RRHH. Quiero alinear las metas de la empresa con la recompensa de mis empleados.','Servicios Financieros','0 - 100','10 - 30 Millones','On Premise'),(187,'HR','Quiero consolidar y optimizar los procesos centrales de RRHH.','Servicios Profesionales','0 - 100','10 - 30 Millones','Híbrida'),(188,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente. Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.','Servicios Profesionales','0 - 100','No poseo información','On Premise'),(189,'HR','Quiero alinear las metas de la empresa con la recompensa de mis empleados. Quiero planificar el desarrollo y sucesión de mis mejores talentos.','Servicios Profesionales','0 - 100','10 - 30 Millones','Cloud'),(190,'HR','Quiero planificar el desarrollo y sucesión de mis mejores talentos. Quiero consolidar y optimizar los procesos centrales de RRHH. Quiero retener a los mejores talentos con un plan de compensación atractivo.','Servicios Financieros','0 - 100','10 - 30 Millones','Cloud'),(191,'HR','Quiero alinear las metas de la empresa con la recompensa de mis empleados. Quiero consolidar y optimizar los procesos centrales de RRHH. Quiero retener a los mejores talentos con un plan de compensación atractivo.','Comercio Minorista','0 - 100','No poseo información','Híbrida'),(192,'HR','','Servicios Financieros','0 - 100','30 - 100 Millones','Cloud'),(193,'HR','Quiero consolidar y optimizar los procesos centrales de RRHH.','Servicios Financieros','0 - 100','100 Millones a más','Híbrida'),(194,'HR','',NULL,NULL,NULL,NULL),(195,'HR',NULL,NULL,NULL,NULL,NULL),(196,'HR',NULL,NULL,NULL,NULL,NULL),(197,'HR',NULL,NULL,NULL,NULL,NULL),(198,'CRM','',NULL,NULL,NULL,NULL),(199,'HR','',NULL,NULL,NULL,NULL),(200,'HR','',NULL,NULL,NULL,NULL),(201,'HR',NULL,NULL,NULL,NULL,NULL),(202,'HR',NULL,NULL,NULL,NULL,NULL),(203,'HR',NULL,NULL,NULL,NULL,NULL),(204,'HR',NULL,NULL,NULL,NULL,NULL),(205,'HR',NULL,NULL,NULL,NULL,NULL),(206,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.','Comercio Minorista','0 - 100','10 - 30 Millones','Híbrida'),(207,'HR','Quiero consolidar y optimizar los procesos centrales de RRHH. Quiero planificar el desarrollo y sucesión de mis mejores talentos. Quiero alinear las metas de la empresa con la recompensa de mis empleados. Quiero desarrollar a mis empleados de una manera simple y colaborativa.','Comercio Minorista','0 - 100','No poseo información','Híbrida'),(208,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa. Quiero alinear las metas de la empresa con la recompensa de mis empleados. Quiero consolidar y optimizar los procesos centrales de RRHH.','Comercio Minorista','0 - 100','30 - 100 Millones','Híbrida'),(209,'HR','Quiero consolidar y optimizar los procesos centrales de RRHH. Quiero alinear las metas de la empresa con la recompensa de mis empleados.','Comercio Minorista','0 - 100','10 - 30 Millones','Híbrida'),(210,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.','Servicios Profesionales','0 - 100','100 Millones a más','Híbrida'),(211,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.','Servicios Financieros','0 - 100','10 - 30 Millones','On Premise'),(212,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa. Quiero alinear las metas de la empresa con la recompensa de mis empleados.','Comercio Minorista','0 - 100','10 - 30 Millones','Cloud'),(213,'HR',NULL,NULL,NULL,NULL,NULL),(214,'HR',NULL,NULL,NULL,NULL,NULL),(215,'HR',NULL,NULL,NULL,NULL,NULL),(216,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa. Quiero alinear las metas de la empresa con la recompensa de mis empleados.','Comercio Minorista','0 - 100','100 Millones a más',NULL),(217,'CRM',NULL,NULL,NULL,NULL,NULL),(218,'CRM',NULL,NULL,NULL,NULL,NULL),(219,'CRM',NULL,NULL,NULL,NULL,NULL),(220,'CRM','Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.',NULL,NULL,NULL,NULL),(221,'HR',NULL,NULL,NULL,NULL,NULL),(222,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.',NULL,NULL,NULL,NULL),(223,'HR',NULL,NULL,NULL,NULL,NULL),(224,'HR',NULL,NULL,NULL,NULL,NULL),(225,'HR',NULL,NULL,NULL,NULL,NULL),(226,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.',NULL,NULL,NULL,NULL),(227,'HR',NULL,NULL,NULL,NULL,NULL),(228,'HR',NULL,NULL,NULL,NULL,NULL),(229,'HR',NULL,NULL,NULL,NULL,NULL),(230,'HR',NULL,NULL,NULL,NULL,NULL),(231,'HR',NULL,NULL,NULL,NULL,NULL),(232,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.','Comercio Minorista','0 - 100','10 - 30 Millones',NULL),(233,'HR',NULL,NULL,NULL,NULL,NULL),(234,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.',NULL,NULL,NULL,NULL),(235,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.','Comercio Mayorista','0 - 100','10 - 30 Millones',NULL),(236,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.','Servicios Financieros','0 - 100','30 - 100 Millones',NULL),(237,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa. Quiero alinear las metas de la empresa con la recompensa de mis empleados.','Comercio Minorista','0 - 100','10 - 30 Millones',NULL),(238,'CRM','Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas. Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.','Servicios Financieros','0 - 100','10 - 30 Millones',''),(239,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa. Quiero planificar el desarrollo y sucesión de mis mejores talentos.','Servicios Financieros','0 - 100','10 - 30 Millones',NULL),(240,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.','Comercio Minorista','0 - 100','10 - 30 Millones','Híbrida'),(241,'HR',NULL,NULL,NULL,NULL,NULL),(242,'HR',NULL,NULL,NULL,NULL,NULL),(243,'HR',NULL,NULL,NULL,NULL,NULL),(244,'HR',NULL,NULL,NULL,NULL,NULL),(245,'HR',NULL,NULL,NULL,NULL,NULL),(246,'HR',NULL,NULL,NULL,NULL,NULL),(247,'HR',NULL,NULL,NULL,NULL,NULL),(248,'HR',NULL,NULL,NULL,NULL,NULL),(249,'HR',NULL,NULL,NULL,NULL,NULL),(250,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.',NULL,NULL,NULL,NULL),(251,'HR',NULL,NULL,NULL,NULL,NULL),(252,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.',NULL,NULL,NULL,NULL),(253,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.','Comercio Minorista','0 - 100','100 Millones a más','Cloud'),(254,'CRM','Como responsable de ventas quiero mejorar mi proceso de ventas. Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.','Servicios Profesionales','100 - 500','10 - 30 Millones','Cloud'),(255,'ERP','Como responsable de la integración financiera de mi empresa quiero tener visibilidad en tiempo real. Como responsable de los procesos operativos quiero alacanzar una mayor eficiencia.','Servicios Profesionales','100 - 500','10 - 30 Millones','Cloud'),(256,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa. Quiero planificar el desarrollo y sucesión de mis mejores talentos.',NULL,NULL,NULL,NULL),(257,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.',NULL,NULL,NULL,NULL),(258,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.',NULL,NULL,NULL,NULL),(259,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.',NULL,NULL,NULL,NULL),(260,'CRM','Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.',NULL,NULL,NULL,NULL),(261,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.',NULL,NULL,NULL,NULL),(262,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.',NULL,NULL,NULL,NULL),(263,'HR','Quiero alinear las metas de la empresa con la recompensa de mis empleados.',NULL,NULL,NULL,NULL),(264,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.',NULL,NULL,NULL,NULL),(265,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.',NULL,NULL,NULL,NULL),(266,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.','Comercio Minorista','0 - 100','10 - 30 Millones',NULL),(267,'HR','Quiero alinear las metas de la empresa con la recompensa de mis empleados. Quiero desarrollar a mis empleados de una manera simple y colaborativa.',NULL,NULL,NULL,NULL),(268,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.',NULL,NULL,NULL,NULL),(269,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.',NULL,NULL,NULL,NULL),(270,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.',NULL,NULL,NULL,NULL),(271,'HR','Quiero alinear las metas de la empresa con la recompensa de mis empleados.','Comercio Minorista','0 - 100','10 - 30 Millones',NULL),(272,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente. Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.','Servicios Financieros','0 - 100','10 - 30 Millones',NULL),(273,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa. Quiero retener a los mejores talentos con un plan de compensación atractivo.','Servicios Financieros','0 - 100','No poseo información',NULL),(274,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa. Quiero retener a los mejores talentos con un plan de compensación atractivo.','Servicios Financieros','0 - 100','No poseo información',NULL),(275,'HR','Quiero alinear las metas de la empresa con la recompensa de mis empleados.','Comercio Minorista','0 - 100','100 Millones a más',NULL),(276,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.','Comercio Mayorista','0 - 100','10 - 30 Millones',NULL),(277,'ERP','Como responsable de los procesos operativos quiero alacanzar una mayor eficiencia.','Servicios Financieros','0 - 100','No poseo información',NULL),(278,'HR','Quiero alinear las metas de la empresa con la recompensa de mis empleados. Quiero retener a los mejores talentos con un plan de compensación atractivo.',NULL,NULL,NULL,NULL),(279,'CRM','Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.','Comercio Minorista','0 - 100','No poseo información',NULL),(280,'HR','Quiero alinear las metas de la empresa con la recompensa de mis empleados. Quiero retener a los mejores talentos con un plan de compensación atractivo.','Servicios Financieros','0 - 100','No poseo información',NULL),(281,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.',NULL,NULL,NULL,NULL),(282,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.',NULL,NULL,NULL,NULL),(283,'ERP','Como responsable de la integración financiera de mi empresa quiero tener visibilidad en tiempo real. Como responsable del crecimiento de la empresa quiero lograr máxima rentabilidad.',NULL,NULL,NULL,NULL),(284,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.','Comercio Minorista','0 - 100','No poseo información','Híbrida'),(285,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.',NULL,NULL,NULL,NULL),(286,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.',NULL,NULL,NULL,NULL),(287,'HR','Quiero alinear las metas de la empresa con la recompensa de mis empleados.','Comercio Minorista','0 - 100','No poseo información',NULL),(288,'CRM','Como responsable de ventas quiero mejorar mi proceso de ventas. Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.','Servicios Profesionales','0 - 100','No poseo información',NULL),(289,'HR','Quiero consolidar y optimizar los procesos centrales de RRHH. Quiero desarrollar a mis empleados de una manera simple y colaborativa.','Servicios Profesionales','0 - 100','No poseo información','Híbrida'),(290,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.',NULL,NULL,NULL,NULL),(291,'ERP','Como responsable del crecimiento de la empresa quiero lograr máxima rentabilidad.','Productos de Consumo','0 - 100','No poseo información','Híbrida'),(292,'HR','Como responsable de ventas quiero mejorar mi proceso de ventas. Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.',NULL,NULL,NULL,NULL),(293,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.',NULL,NULL,NULL,NULL),(294,'CRM','Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.','Comercio Minorista','0 - 100','10 - 30 Millones',NULL),(295,'CRM','Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.','Servicios Financieros','0 - 100','No poseo información',NULL),(296,'CRM','Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.','Productos de Consumo','0 - 100','No poseo información',NULL),(297,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.',NULL,NULL,NULL,NULL),(298,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa. Quiero retener a los mejores talentos con un plan de compensación atractivo.',NULL,NULL,NULL,NULL),(299,'ERP','Como responsable de los procesos operativos quiero alacanzar una mayor eficiencia.',NULL,NULL,NULL,NULL),(300,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.',NULL,NULL,NULL,NULL),(301,'HR','Quiero planificar el desarrollo y sucesión de mis mejores talentos.',NULL,NULL,NULL,NULL),(302,'CRM','Como responsable de ventas quiero mejorar mi proceso de ventas.',NULL,NULL,NULL,NULL),(303,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.',NULL,NULL,NULL,NULL),(304,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.',NULL,NULL,NULL,NULL),(305,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.',NULL,NULL,NULL,NULL),(306,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.',NULL,NULL,NULL,NULL),(307,'CRM','Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.',NULL,NULL,NULL,NULL),(308,'HR','Quiero retener a los mejores talentos con un plan de compensación atractivo.',NULL,NULL,NULL,NULL),(309,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.',NULL,NULL,NULL,NULL),(310,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.','Comercio Minorista','0 - 100','No poseo información',NULL),(311,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.','Comercio Minorista','0 - 100','No poseo información',NULL),(312,'ERP','Como responsable de la integración financiera de mi empresa quiero tener visibilidad en tiempo real. Como responsable del crecimiento de la empresa quiero lograr máxima rentabilidad.','Servicios Financieros','0 - 100','30 - 100 Millones','Cloud'),(313,'HR','Quiero consolidar y optimizar los procesos centrales de RRHH. Quiero desarrollar a mis empleados de una manera simple y colaborativa.','Servicios Financieros','0 - 100','100 Millones a más','Cloud'),(314,'CRM','Como responsable de ventas quiero mejorar mi proceso de ventas. Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.','Comercio Minorista','0 - 100','No poseo información','Híbrida'),(315,'CRM',NULL,NULL,NULL,NULL,NULL),(316,'CRM',NULL,NULL,NULL,NULL,NULL),(317,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.',NULL,NULL,NULL,NULL),(318,'HR',NULL,NULL,NULL,NULL,NULL),(319,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.','Servicios Financieros','0 - 100','10 - 30 Millones',NULL),(320,'CRM',NULL,NULL,NULL,NULL,NULL),(321,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente. Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.','Comercio Minorista','0 - 100','10 - 30 Millones',NULL),(322,'CRM',NULL,NULL,NULL,NULL,NULL),(323,'HR',NULL,NULL,NULL,NULL,NULL),(324,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa. Quiero alinear las metas de la empresa con la recompensa de mis empleados.','Comercio Minorista','0 - 100','10 - 30 Millones',NULL),(325,'HR',NULL,NULL,NULL,NULL,NULL),(326,'CRM','Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.','Servicios Financieros','0 - 100','10 - 30 Millones','Cloud'),(327,'CRM',NULL,NULL,NULL,NULL,NULL),(328,'ERP',NULL,NULL,NULL,NULL,NULL),(329,'HR',NULL,NULL,NULL,NULL,NULL),(330,'HR',NULL,NULL,NULL,NULL,NULL),(331,'CRM',NULL,NULL,NULL,NULL,NULL),(332,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente. Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.',NULL,NULL,NULL,NULL),(333,'CRM',NULL,NULL,NULL,NULL,NULL),(334,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.','Comercio Minorista','0 - 100','10 - 30 Millones',NULL),(335,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa. Quiero retener a los mejores talentos con un plan de compensación atractivo.','Comercio Minorista','0 - 100','30 - 100 Millones',''),(336,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa. Quiero retener a los mejores talentos con un plan de compensación atractivo.',NULL,NULL,NULL,NULL),(337,'HR',NULL,NULL,NULL,NULL,NULL),(338,'HR','Quiero retener a los mejores talentos con un plan de compensación atractivo.',NULL,NULL,NULL,NULL),(339,'HR',NULL,NULL,NULL,NULL,NULL),(340,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa. Quiero retener a los mejores talentos con un plan de compensación atractivo.',NULL,NULL,NULL,NULL),(341,'HR',NULL,NULL,NULL,NULL,NULL),(342,'HR',NULL,NULL,NULL,NULL,NULL),(343,'CRM',NULL,NULL,NULL,NULL,NULL),(344,'CRM',NULL,NULL,NULL,NULL,NULL),(345,'ERP',NULL,NULL,NULL,NULL,NULL),(346,'ERP',NULL,NULL,NULL,NULL,NULL),(347,'ERP',NULL,NULL,NULL,NULL,NULL),(348,'HR',NULL,NULL,NULL,NULL,NULL),(349,'ERP',NULL,NULL,NULL,NULL,NULL),(350,'CRM',NULL,NULL,NULL,NULL,NULL),(351,'ERP','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente. Como responsable de la integración financiera de mi empresa quiero tener visibilidad en tiempo real.',NULL,NULL,NULL,NULL),(352,'HR','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente. Como responsable de la integración financiera de mi empresa quiero tener visibilidad en tiempo real. Quiero desarrollar a mis empleados de una manera simple y colaborativa.',NULL,NULL,NULL,NULL),(353,'CRM',NULL,NULL,NULL,NULL,NULL),(354,'CRM',NULL,NULL,NULL,NULL,NULL),(355,'ERP','Como responsable de la integración financiera de mi empresa quiero tener visibilidad en tiempo real.',NULL,NULL,NULL,NULL),(356,'CRM','Como responsable de la integración financiera de mi empresa quiero tener visibilidad en tiempo real. Como responsable de ventas quiero mejorar mi proceso de ventas.',NULL,NULL,NULL,NULL),(357,'ERP','Como responsable de ventas quiero mejorar mi proceso de ventas. Como responsable de la integración financiera de mi empresa quiero tener visibilidad en tiempo real.','Servicios Profesionales','0 - 100','10 - 30 Millones','Cloud'),(358,'HR','Quiero consolidar y optimizar los procesos centrales de RRHH.','Comercio Minorista','0 - 100','10 - 30 Millones',NULL),(359,'ERP','Quiero consolidar y optimizar los procesos centrales de RRHH. Como responsable de la integración financiera de mi empresa quiero tener visibilidad en tiempo real.','Comercio Minorista','0 - 100','10 - 30 Millones',NULL),(360,'CRM','Quiero consolidar y optimizar los procesos centrales de RRHH. Como responsable de la integración financiera de mi empresa quiero tener visibilidad en tiempo real. Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.',NULL,NULL,NULL,NULL),(361,'ERP',NULL,NULL,NULL,NULL,NULL),(362,'ERP','Como responsable de la integración financiera de mi empresa quiero tener visibilidad en tiempo real.','Servicios Profesionales','0 - 100','No poseo información','On Premise'),(363,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.','Servicios Profesionales','0 - 100','No poseo información',NULL),(364,'ERP',NULL,NULL,NULL,NULL,NULL),(365,'ERP','Como responsable de la integración financiera de mi empresa quiero tener visibilidad en tiempo real.','Servicios Profesionales','0 - 100','10 - 30 Millones','On Premise'),(366,'CRM','Como responsable de ventas quiero mejorar mi proceso de ventas.','Servicios Profesionales','0 - 100','10 - 30 Millones','On Premise'),(367,'ERP',NULL,NULL,NULL,NULL,NULL),(368,'ERP','Como responsable de la integración financiera de mi empresa quiero tener visibilidad en tiempo real.','Servicios Profesionales','0 - 100','10 - 30 Millones','On Premise'),(369,'CRM',NULL,NULL,NULL,NULL,NULL),(370,'ERP',NULL,NULL,NULL,NULL,NULL),(371,'ERP','Como responsable de la integración financiera de mi empresa quiero tener visibilidad en tiempo real.','Servicios Profesionales','0 - 100','10 - 30 Millones',NULL),(372,'CRM','Como responsable de ventas quiero mejorar mi proceso de ventas.','Servicios Profesionales','0 - 100','10 - 30 Millones',NULL),(373,'ERP','Como responsable de la integración financiera de mi empresa quiero tener visibilidad en tiempo real.','Servicios Profesionales','0 - 100','10 - 30 Millones','Cloud'),(374,'CRM','Como responsable de ventas quiero mejorar mi proceso de ventas.',NULL,NULL,NULL,NULL),(375,'ERP','Como responsable de los procesos operativos quiero alacanzar una mayor eficiencia. Como responsable del crecimiento de la empresa quiero lograr máxima rentabilidad.',NULL,NULL,NULL,NULL),(376,'CRM',NULL,NULL,NULL,NULL,NULL),(377,'ERP',NULL,NULL,NULL,NULL,NULL),(378,'HR',NULL,NULL,NULL,NULL,NULL),(379,'HR',NULL,NULL,NULL,NULL,NULL),(380,'HR',NULL,NULL,NULL,NULL,NULL),(381,'HR',NULL,NULL,NULL,NULL,NULL),(382,'CRM',NULL,NULL,NULL,NULL,NULL),(383,'HR',NULL,NULL,NULL,NULL,NULL),(384,'HR',NULL,NULL,NULL,NULL,NULL),(385,'RH',NULL,NULL,NULL,NULL,NULL),(386,'RH','Quero desenvolver meus colaboradores de maneira simples e colaborativa.','Varejo','500 - 1000','100 Milhões para mais','Nuvem'),(387,'RH','Quero centralizar e agilizar meus processos de aquisição e contratação de talentos.',NULL,NULL,NULL,NULL),(388,'RH','Quero desenvolver meus colaboradores de maneira simples e colaborativa.',NULL,NULL,NULL,NULL),(389,'RH','Quero desenvolver meus colaboradores de maneira simples e colaborativa.',NULL,NULL,NULL,NULL),(390,'CRM','Como responsável de vendas  quero criar experiências de compra únicas e simplificadas.',NULL,NULL,NULL,NULL),(391,'RH','Quero desenvolver meus colaboradores de maneira simples e colaborativa.',NULL,NULL,NULL,NULL),(392,'CRM','Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.','Comercio Minorista','0 - 100','30 - 100 Millones','Híbrida'),(393,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.','Comercio Minorista','0 - 100','10 - 30 Millones','Cloud'),(394,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.','Servicios Financieros','0 - 100','30 - 100 Millones','Híbrida'),(395,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.','Comercio Minorista','0 - 100','10 - 30 Millones','Cloud'),(396,'HR','I want to develop my employees in a simple and collaborative way.','Financial Services','0 - 100','10 - 30 Millions','Cloud'),(397,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.','Comercio Minorista','0 - 100','10 - 30 Millones','Cloud'),(398,'HR','I want to develop my employees in a simple and collaborative way.','Retail','0 - 100','10 - 30 Millions','Cloud'),(399,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.','Comercio Minorista','0 - 100','30 - 100 Millones','Híbrida'),(400,'RH','Quero desenvolver meus colaboradores de maneira simples e colaborativa.','Varejo','0 - 100','10 - 30 Milhões','Híbrida'),(401,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.','Comercio Minorista','0 - 100','10 - 30 Millones','Híbrida'),(402,'CRM',NULL,NULL,NULL,NULL,NULL),(403,'HR',NULL,NULL,NULL,NULL,NULL),(404,'CRM',NULL,NULL,NULL,NULL,NULL),(405,'ERP',NULL,NULL,NULL,NULL,NULL),(406,'CRM',NULL,NULL,NULL,NULL,NULL),(407,'CRM','Como responsável de serviços  quero melhorar nossos processos de suporte ao cliente.',NULL,NULL,NULL,NULL),(408,'CRM','Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.',NULL,NULL,NULL,NULL),(409,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.',NULL,NULL,NULL,NULL),(410,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.',NULL,NULL,NULL,NULL),(411,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.',NULL,NULL,NULL,NULL),(412,'CRM',NULL,NULL,NULL,NULL,NULL),(413,'ERP',NULL,NULL,NULL,NULL,NULL),(414,'CRM','As an online sales manager I want to create unique and simplified shopping experiences.','Financial Services','','30 - 100 Millions','Hybrid'),(415,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.',NULL,NULL,NULL,NULL),(416,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.','Servicios Financieros','0 - 100','10 - 30 Millones','Híbrida'),(417,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.','Comercio Minorista','0 - 100','10 - 30 Millones','Híbrida'),(418,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.','Comercio Minorista','0 - 100','10 - 30 Millones','Cloud'),(419,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.',NULL,NULL,NULL,NULL),(420,'CRM','Como responsable de servicios quiero mejorar nuestros procesos de soporte al cliente.',NULL,NULL,NULL,NULL),(421,'CRM','Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.',NULL,NULL,NULL,NULL),(422,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.',NULL,NULL,NULL,NULL),(423,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.',NULL,NULL,NULL,NULL),(424,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.',NULL,NULL,NULL,NULL),(425,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.','Servicios Profesionales','0 - 100','30 - 100 Millones',NULL),(426,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.',NULL,NULL,NULL,NULL),(427,'HR','Quiero alinear las metas de la empresa con la recompensa de mis empleados.','Servicios Financieros','0 - 100','0 - 100','Híbrida'),(428,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.','Comercio Minorista','0 - 100','10 - 30 Millones','Híbrida'),(429,'HR','Quiero desarrollar a mis empleados de una manera simple y colaborativa.','Comercio Minorista','500 - 1000','30 - 100 Millones','Cloud'),(430,'CRM','Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.','','','',NULL),(431,'CRM',NULL,NULL,NULL,NULL,NULL),(432,'CRM',NULL,NULL,NULL,NULL,NULL),(433,'CRM','Como responsable de ventas online quiero crear experiencias de compra únicas e simplificadas.','fgdfg','0 - 100','No poseo información','');
/*!40000 ALTER TABLE `solicitud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `Id_persona` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_completo` varchar(255) DEFAULT NULL,
  `Empresa` varchar(255) DEFAULT NULL,
  `Email` text,
  `Pais` varchar(50) DEFAULT NULL,
  `Cargo` varchar(100) DEFAULT NULL,
  `Telefono` int(9) DEFAULT NULL,
  `Terminos` int(11) DEFAULT NULL COMMENT '1 = SÍ O 0 = NO',
  `Relacion` varchar(100) DEFAULT NULL,
  `Contactado` int(9) DEFAULT NULL COMMENT '1 = Por email, 2 = Por teléfono, 3 = Ambos',
  `Id_solicitud` int(11) NOT NULL,
  `fecha_sol` datetime DEFAULT NULL,
  `checks` text,
  `idioma` text NOT NULL,
  `lugar_aceptacion` text NOT NULL,
  PRIMARY KEY (`Id_persona`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'test','test','jhonatanibericom@gmail.com','perú','test',45454,1,'Empleado SAP',0,44,'2018-05-16 17:13:00','VERDADERO','Español','Cloud Configurator'),(2,'test','test','jhonatanibericom@gmail.com','perú','test',544454,1,'Consultor',1,45,'2018-05-16 17:15:03',NULL,'Español','Cloud Configurator'),(3,'dfgg','dfgd','jhiberico@hotmail.com','dgdf','dfgd',54,1,'Cliente potencial',1,46,'2018-05-16 17:45:16',NULL,'Español','Cloud Configurator'),(4,'asdasd','asdsa','jhonatanibericom@gmail.com','dfgdf','asdasd',44454,1,'Empleado SAP',0,93,'2018-05-18 08:54:06','Acepto','Español','Cloud Configurator'),(5,'test','test','jhonatanibericom@gmail.com','perú','test',545454,1,'Estudiante',1,94,'2018-05-18 08:56:58',NULL,'Español','Cloud Configurator'),(6,'dfg','dfgdf','jhonatanibericom@gmail.com','perú','dfgdf',545,1,'Empleado SAP',0,96,'2018-05-21 11:20:18',NULL,'Español','Cloud Configurator'),(7,'test','test','jhonatanibericom@gmail.com','Brazil','test',54456465,1,'Consultor',0,106,'2018-06-01 16:38:35','Si','Español','Cloud Configurator'),(8,'aaa','aaa','jhonatanibericom@gmail.com','Puerto Rico','aaa',5455,1,'Empleado SAP',1,114,'2018-06-04 08:13:52',NULL,'Español','Cloud Configurator'),(9,'test','test','jhonatanibericom@gmail.com','Costa Rica','test',654565465,1,'Estudiante',1,115,'2018-06-04 08:18:12','Si','Español','Cloud Configurator'),(10,'test','test','jhonatanibericom@gmail.com','Mexico','test',45464,1,'Empleado SAP',1,116,'2018-06-04 08:20:17',NULL,'Español','Cloud Configurator'),(11,'prueba','prueba s.a.c ','pyf136@gmail.com','Peru','prueba',2791339,1,'Partner',1,312,'2018-07-03 18:21:54','General Marketing, Sharing data','Español','Cloud Configurator'),(12,'probando','probando','pyf136@gmail.com','Peru','probando',922491412,1,'Estudiante',1,313,'2018-07-03 18:54:04','General Marketing, Sharing data','Español','Cloud Configurator'),(13,'test','test','jose.minayac15@gmail.com','USA','test',0,0,'Cliente',NULL,428,'2018-08-09 23:39:24','','Español','Cloud Configurator');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `costarica`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `costarica` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `costarica`;

--
-- Table structure for table `participante`
--

DROP TABLE IF EXISTS `participante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `participante` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `telefono` varchar(50) NOT NULL,
  `empresa` varchar(50) NOT NULL,
  `cargo` varchar(50) NOT NULL,
  `pais` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participante`
--

LOCK TABLES `participante` WRITE;
/*!40000 ALTER TABLE `participante` DISABLE KEYS */;
INSERT INTO `participante` VALUES (1,'test','test','jose.minayac15@gmail.com','test','test','test','test','2018-08-02'),(2,'TEST','TEST','jose2.minayac15@gmail.com','TEST','TEST','TEST','TEST','2018-08-02'),(3,'test','test','jose3.minayac15@gmail.com','test','test','test','test','2018-08-02'),(4,'test','test','jose4.minayac15@gmail.com','test','test','test','test','2018-08-02'),(5,'test','test','jos3e.minayac15@gmail.com','test','test','test','test','2018-08-03'),(6,'test','test','jose.minaya2222c15@gmail.com','test','test','test','test','2018-08-03'),(7,'test','test','jose33.minayac15@gmail.com','test','test','test','test','2018-08-03'),(8,'test','test','jose3322.minayac15@gmail.com','test','test','test','test','2018-08-03'),(9,'test','test','jose22.minayac15@gmail.com','test','test','test','test','2018-08-03');
/*!40000 ALTER TABLE `participante` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `ebook`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `ebook` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `ebook`;

--
-- Table structure for table `costo`
--

DROP TABLE IF EXISTS `costo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `costo` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `precio` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `costo`
--

LOCK TABLES `costo` WRITE;
/*!40000 ALTER TABLE `costo` DISABLE KEYS */;
INSERT INTO `costo` VALUES (1,'Sin budget'),(2,'Budget < 5K Euros'),(3,'Budget  5K - 10K Euros'),(4,'Budget > 10K Euros'),(5,'EUR 728');
/*!40000 ALTER TABLE `costo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paises`
--

DROP TABLE IF EXISTS `paises`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paises` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(255) DEFAULT NULL,
  `link` text,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paises`
--

LOCK TABLES `paises` WRITE;
/*!40000 ALTER TABLE `paises` DISABLE KEYS */;
INSERT INTO `paises` VALUES (1,'Colombia','https://partnerbenefitscatalog.sap.com/Partner-Benefits-Catalog/Marketing/Lead-Generation/p/1000432'),(2,'Brasil','https://partnerbenefitscatalog.sap.com/Partner-Benefits-Catalog/Marketing/Digital-Advertising-%E2%80%93-Google-AdWords-and-Facebook-Ads/p/1000480'),(3,'Bolivia','https://partnerbenefitscatalog.sap.com/Partner-Benefits-Catalog/Marketing/Lead-Generation/p/1000470'),(4,'Chile','https://partnerbenefitscatalog.sap.com/Partner-Benefits-Catalog/Marketing/Lead-Generation/p/1000470'),(5,'Costa Rica',NULL),(6,'Ecuador',NULL),(7,'El Salvador',NULL),(8,'Guatemala',NULL),(9,'Honduras',NULL),(10,'Islas Vírgenes',NULL),(11,'Nicaragua',NULL),(12,'Panamá',NULL),(13,'Paraguay','https://partnerbenefitscatalog.sap.com/Partner-Benefits-Catalog/Marketing/Lead-Generation/p/1000470'),(14,'Perú','https://partnerbenefitscatalog.sap.com/Partner-Benefits-Catalog/Marketing/Lead-Generation/p/1000470'),(15,'Puerto Rico',NULL),(16,'República Dominicana',NULL),(17,'Trinidad y Tobago',NULL),(18,'Uruguay','https://partnerbenefitscatalog.sap.com/Partner-Benefits-Catalog/Marketing/Lead-Generation/p/1000470'),(19,'Venezuela',NULL),(20,'Argentina','https://partnerbenefitscatalog.sap.com/Partner-Benefits-Catalog/Marketing/Lead-Generation/p/1000470'),(21,'México',NULL);
/*!40000 ALTER TABLE `paises` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partners`
--

DROP TABLE IF EXISTS `partners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partners` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `pais` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `categoria` varchar(100) DEFAULT NULL,
  `id_partner` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=147 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partners`
--

LOCK TABLES `partners` WRITE;
/*!40000 ALTER TABLE `partners` DISABLE KEYS */;
INSERT INTO `partners` VALUES (1,'Chile','Adexus S.A.','Silver',335390),(2,'Colombia','Neoris Colombia S.A.S.','Gold',445061),(3,'Argentina','ADP Consultores S.R.L.','Gold',452461),(4,'Chile','Deloitte Servicios Profesionales Ltda','Platinum',472268),(5,'Mexico','Enable, S.C.','Gold',505660),(6,'Mexico','Neoris de México, S.A. de C.V.','Gold',614725),(7,'Mexico','Tesselar Soluciones, S.A. de C.V.','Gold',651160),(8,'Colombia','Consultoría Organizacional S.A.S.','Gold',664870),(9,'Peru','Omnia Solution S.A.C.','Gold',672351),(10,'Colombia','MQA BUSINESS CONSULTANTS S A','Gold',698634),(11,'Mexico','Link Technologies, S. A. P. I. de C.V.','Gold',708379),(12,'Chile','Seidor Chile S.A.','Platinum',750824),(13,'Argentina','abc Consulting S.A.','Gold',757182),(14,'Brazil','ITS Tecnologia e Informação Ltda','Gold',766294),(15,'Argentina','Process Technologies S.A.','Silver',773804),(16,'Brazil','Megawork Consultoria e Sistemas Ltda','Gold',832647),(17,'Mexico','OptiSoft, S.A. De C.V.','Gold',851178),(18,'Panama','Integrated Systems','Silver',871640),(19,'Argentina','BAITCON S.A.','Gold',923988),(20,'Peru','Seidor Consulting Perú SAC.','Platinum',970697),(21,'Panama','MQA AMERICAS CORP','Gold',1048960),(22,'Brazil','Spektrum Com?rcio e Consultoria','Gold',1106782),(23,'Brazil','Seidor Crystalis - Tecnologia','Platinum',1138761),(24,'Brazil','Tivit Terceirizacao de Processos','Silver',1139442),(25,'Brazil','Essence Consultoria','Gold',1146842),(26,'Brazil','SUM Reseller S/A','Silver',1147676),(27,'Brazil','VAR3F  CONSULTORIA INFORMATICA E','Silver',1193042),(28,'Argentina','Nearshore Argentina S.A.','Gold',1199947),(29,'Brazil','Resource Americana LTDA','Gold',1252359),(30,'Peru','Everis Peru Sociedad Anonima Cerrada','Platinum',1310156),(31,'Brazil','Adopti Consultoria em Informatica Ltda','Silver',1347715),(32,'Brazil','STAR - Soluções Tec. Avançada Retail Lt','Gold',1397902),(33,'Brazil','Deloitte Assessoria e Consultoria Ltda','Platinum',1496733),(34,'Peru','Top Consulting Group Peru S.A.C','Silver',1500788),(35,'Mexico','Celeritech Mexico SAPI de CV','Gold',1650311),(36,'Guatemala','SOLUCIONES FORMALES EN SISTEMAS, S.A.','Gold',1682688),(37,'Ecuador','T&SCorp. Cia. LTDA','Silver',1709339),(38,'Brazil','Meta Serviços em Informática S/A','Gold',1715527),(39,'Colombia','IBM De Colombia & Cia. S.C.A.','Platinum',33661),(40,'Brazil','IBM Brasil - Indústria, Máquinas','Platinum',35202),(41,'Chile','IBM de Chile S.A.C.','Platinum',165391),(42,'Peru','IBM del Peru S.A.C.','Platinum',182375),(43,'Argentina','Seidor Consulting SA','Platinum',197692),(44,'Peru','Deloitte & Touche S.R.L.','Platinum',278533),(45,'Colombia','Softtek Renovation Ltda.','Silver',282172),(46,'Venezuela','SOFOS, C.A.','Silver',362109),(47,'Brazil','Neoris do Brasil Ltda','Silver',484358),(48,'Colombia','TIVIT COLOMBIA TERCERIZACION DE PROCESOS','Silver',498849),(49,'Argentina','Deloitte & Co. S.A.','Platinum',513420),(50,'Peru','Rivercon.com S.A.C.','Gold',614852),(51,'Colombia','CompuNet, S.A.','Gold',635303),(52,'Argentina','Decision Support S.A.','Silver',642634),(53,'Colombia','Deloitte Asesores y Consultores Ltda','Platinum',650331),(54,'Colombia','Projection Core Consulting S.A.S.','Silver',657493),(55,'Colombia','HR Solutions S.A.S.','Silver',684898),(56,'Dominican Rep.','DT Solutions C. por A.','Gold',698710),(57,'Brazil','META SERVICOS EM INFORMATICA S/A','Silver',699483),(58,'Mexico','SEIDOR MEXICO SAPI de CV','Platinum',704091),(59,'Chile','Neoris de Chile Ltda','Silver',705762),(60,'Venezuela','LatCapital de Venezuela, C.A.','Gold',705915),(61,'Brazil','Sonda Procwork Inform?tica Ltda','Gold',709829),(62,'Mexico','Corponet Implements, S.A. de C.V.','Gold',715105),(63,'Bri. Virgin Is.','LatCapital Solutions INC','Gold',722571),(64,'Argentina','IBM ARGENTINA SRL','Platinum',726439),(65,'Brazil','Cast Informática S.A.','Silver',752656),(66,'Uruguay','Invenzis (Marbus S.A.)','Gold',756299),(67,'Mexico','Grupo ASSA México Soluciones','Silver',758259),(68,'Peru','Celeritech Solutions S.A.C.','Gold',759692),(69,'Bolivia','Datec Ltda.','Gold',803802),(70,'Venezuela','Celeritech Solutions C.A.','Silver',821107),(71,'Chile','Quintec Chile S.A.','Gold',826671),(72,'Chile','ADP Consultores Ltda.','Gold',828111),(73,'Peru','Sypsoft S.A.C.','Gold',829654),(74,'Chile','ALYNEA Asesorias y Consultorias Limitada','Silver',847784),(75,'Chile','STK Softtek Chile Ltda','Silver',850849),(76,'Brazil','FH Consultoria Empresarial Ltda.','Gold',855982),(77,'Brazil','PGT Solucoes Comercio e Servicos','Silver',858356),(78,'Brazil','Discover Technology Informatica','Silver',860281),(79,'Mexico','Epi-use México, S.A. de C.V.','Silver',881088),(80,'Argentina','Neoris Consulting Argentina S.A','Silver',881376),(81,'Paraguay','Infocenter','Gold',886235),(82,'Trinidad/Tobago','Nspire Management Consultants','Silver',896333),(83,'Mexico','Enapsys México, S.A. de C.V.','Silver',898051),(84,'Argentina','Crystal Solutions S.A.','Platinum',922593),(85,'Uruguay','Seidor Uruguay Informatica S.A','Silver',922594),(86,'Mexico','Softtek Servicios y Tecnologia S.A de CV','Gold',926787),(87,'Venezuela','CORPORACION SYBVEN C.A.','Silver',987087),(88,'Ecuador','Noux C.A.','Silver',999062),(89,'Guatemala','B&R Ingeniería de Sistemas, S.A.','Silver',999063),(90,'Trinidad/Tobago','Infotech Caribbean Limited','Silver',1004011),(91,'Peru','BCTS Consulting S.A.','Silver',1010552),(92,'Panama','Cibernetica, S.A.','Silver',1010561),(93,'Venezuela','IT Sales, C.A.','Silver',1026824),(94,'Costa Rica','Crystalis Centroam?rica, S.A.','Silver',1040817),(95,'Costa Rica','Seidor Crystalis Costa Rica','Platinum',1041408),(96,'Mexico','Everis México, S. de R.L. de C.V.','Platinum',1047982),(97,'Mexico','Organización de Conocimiento','Gold',1049826),(98,'Brazil','SPRO CONSULTORIA E INFORMATICA LTDA.','Gold',1060671),(99,'Brazil','ABC TECHNOLOGY DESENVOLVIMENTO','Silver',1076618),(100,'Mexico','Dintec Consulting SA de CV','Silver',1113124),(101,'Puerto Rico','N&B Team Consulting','Silver',1115777),(102,'Colombia','ADP Consultores S.A.S.','Silver',1139181),(103,'Brazil','First Decision Tecnologias','Gold',1169371),(104,'Dominican Rep.','Crystal Solutions del Caribe, SRL','Silver',1170966),(105,'Chile','sdfsdf',NULL,98765),(106,'Bolivia','sdfadsfsdaf',NULL,3621094),(107,'Bri. Virgin Is.','sfdgsfdg',NULL,2147483647),(108,'Bri. Virgin Is.','sfdgsfdg',NULL,2147483647),(109,'Bri. Virgin Is.','sfdgsfdg',NULL,2147483647),(110,'Bri. Virgin Is.','sfdgsfdg',NULL,2147483647),(111,'Bri. Virgin Is.','sfdgsfdg',NULL,2147483647),(112,'Bri. Virgin Is.','sfdgsfdg',NULL,2147483647),(113,'Bri. Virgin Is.','sfdgsfdg',NULL,2147483647),(114,'Bri. Virgin Is.','sfdgsfdg',NULL,2147483647),(115,'Bri. Virgin Is.','sfdgsfdg',NULL,2147483647),(116,'Bri. Virgin Is.','sfdgsfdg',NULL,2147483647),(117,'Bri. Virgin Is.','sfdgsfdg',NULL,2147483647),(118,'Bri. Virgin Is.','sfdgsfdg',NULL,2147483647),(119,'Bri. Virgin Is.','sfdgsfdg',NULL,2147483647),(120,'Bolivia','aasdfasfdasdf',NULL,987654321),(121,'Bri. Virgin Is.','dsfasdf',NULL,2147483647),(122,'Bri. Virgin Is.','654646',NULL,2147483647),(123,'Bolivia','sdfsfd',NULL,987654),(124,'Puerto Rico','sfdgsdfgsdfgs',NULL,2147483647),(125,'Bolivia','sfdgsdfgsdfg',NULL,2147483647),(126,'Colombia','asfsdfasdf',NULL,2147483647),(127,'Bri. Virgin Is.','fdsgsfdgsdfg',NULL,2147483647),(128,'Argentina','ricv informatic',NULL,2147483647),(129,'Bri. Virgin Is.','SADASDSAD',NULL,2147483647),(130,'Bri. Virgin Is.','SDFSDF',NULL,2147483647),(131,'Bri. Virgin Is.','SDFSDF',NULL,9987654),(132,'Colombia','ASDASDSAD',NULL,2147483647),(133,'Colombia','DASDSADSA',NULL,2147483647),(134,'Colombia','SDFSDG',NULL,98765987),(135,'Argentina','SDFASDF',NULL,987654324),(136,'Chile','RICV',NULL,2147483647),(137,'Costa Rica','978ASDA',NULL,999999999),(138,'Bolivia','SDFSDAFADSF',NULL,1000000000),(139,'Bolivia','SDFAADSF',NULL,1999999999),(140,'Bri. Virgin Is.','SDFSGDF',NULL,2147483647),(141,'Bri. Virgin Is.','SDFSDAFASDF',NULL,2147483646),(142,'Bolivia','asdasd',NULL,2147483647),(143,'Bolivia','sfdgsfdgsfdgsdfg',NULL,2147483647),(144,'Bri. Virgin Is.','9879797979',NULL,2147483647),(145,'Bri. Virgin Is.','sdfsdfsdf',NULL,2147483647),(146,'Bolivia','ricv',NULL,23456789);
/*!40000 ALTER TABLE `partners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servicio`
--

DROP TABLE IF EXISTS `servicio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servicio` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(255) DEFAULT NULL,
  `Id_pais` int(11) DEFAULT NULL,
  `Id_costo` int(11) DEFAULT NULL,
  `id_tipo_servi` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=423 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servicio`
--

LOCK TABLES `servicio` WRITE;
/*!40000 ALTER TABLE `servicio` DISABLE KEYS */;
INSERT INTO `servicio` VALUES (1,'Grow you Pipeline with Virtual Agency',20,1,1),(2,'Social Selling Program ',20,1,1),(3,'Link Bulding',20,1,1),(4,'Customer Success Stories',20,1,1),(5,'Always On - Webinar On24',20,1,1),(6,'EveryOne Social',20,1,1),(7,'Marketing Development Funds Training',20,1,1),(8,'Servicio especifico para companias ',20,1,3),(9,'Innovation Program4Partners',20,1,3),(10,'EUR 728.00',20,1,3),(11,'Marketing Booster',20,2,1),(12,'SEM for SAP Partners: Effectiveness and Collaboration',20,2,1),(13,'Data Profiling',20,2,2),(14,'Social Selling Concierge',20,2,2),(15,'Cloud Configurator Campaign',20,2,2),(16,'Blitz & Appointment Settings',20,2,2),(17,'Telemarketing',20,2,2),(18,'Always On Webinars',20,2,3),(19,'B1 Marketing Plan with SAP Guidance',20,2,4),(20,'Premium Marketing Services ',20,2,4),(21,'Grow you Pipeline with Virtual Agency',2,1,1),(22,'Social Selling Program ',2,1,1),(23,'Link Bulding',2,1,1),(24,'Customer Success Stories',2,1,1),(25,'Always On - Webinar On24',2,1,1),(26,'EveryOne Social',2,1,1),(27,'Marketing Development Funds Training',2,1,1),(28,'Servicio especifico para companias ',2,1,3),(29,'Innovation Program4Partners',2,1,3),(30,'EUR 728.00',2,1,3),(31,'Marketing Booster',2,2,1),(32,'SEM for SAP Partners: Effectiveness and Collaboration',2,2,1),(33,'Data Profiling',2,2,2),(34,'Social Selling Concierge',2,2,2),(35,'Cloud Configurator Campaign',2,2,2),(36,'Neoway ? Demand Generation',2,2,2),(37,'Telemarketing',2,2,2),(38,'Lead Generation',2,2,2),(39,'Testimonial Videos',2,2,3),(40,'Always On Webinars',2,2,3),(41,'Videolicious',2,2,3),(42,'Testimonial Video',2,2,3),(43,'B1 Marketing Plan with SAP Guidance',2,2,4),(44,'Premium Marketing Services ',2,2,4),(45,'Selling Partner Package Solutions',2,2,4),(46,'Grow you Pipeline with Virtual Agency',3,1,1),(47,'Social Selling Program ',3,1,1),(48,'Link Bulding',3,1,1),(49,'Customer Success Stories',3,1,1),(50,'Always On - Webinar On24',3,1,1),(51,'EveryOne Social',3,1,1),(52,'Marketing Development Funds Training',3,1,1),(53,'Servicio especifico para companias ',3,1,3),(54,'Innovation Program4Partners',3,1,3),(55,'EUR 728.00',3,1,3),(56,'Marketing Booster',3,2,1),(57,'SEM for SAP Partners: Effectiveness and Collaboration',3,2,1),(58,'Data Profiling',3,2,2),(59,'Telemarketing',3,2,2),(60,'Always On Webinars',3,2,3),(61,'Videolicious',3,2,3),(62,'Premium Marketing Services ',3,2,4),(63,'Grow you Pipeline with Virtual Agency',4,1,1),(64,'Social Selling Program ',4,1,1),(65,'Link Bulding',4,1,1),(66,'Customer Success Stories',4,1,1),(67,'Always On - Webinar On24',4,1,1),(68,'EveryOne Social',4,1,1),(69,'Marketing Development Funds Training',4,1,1),(70,'Servicio especifico para companias ',4,1,3),(71,'Innovation Program4Partners',4,1,3),(72,'EUR 728.00',4,1,3),(73,'Marketing Booster',4,2,1),(74,'SEM for SAP Partners: Effectiveness and Collaboration',4,2,1),(75,'Data Profiling',4,2,2),(76,'Social Selling Concierge',4,2,2),(77,'Cloud Configurator Campaign',4,2,2),(78,'Blitz & Appointment Settings',4,2,2),(79,'Telemarketing',4,2,2),(80,'Always On Webinars',4,2,3),(81,'Customer Testimonial Videos',4,2,3),(82,'Videolicious',4,2,3),(83,'B1 Marketing Plan with SAP Guidance',4,2,4),(84,'Premium Marketing Services ',4,2,4),(85,'Grow you Pipeline with Virtual Agency',1,1,1),(86,'Social Selling Program ',1,1,1),(87,'Link Bulding',1,1,1),(88,'Customer Success Stories',1,1,1),(89,'Always On - Webinar On24',1,1,1),(90,'EveryOne Social',1,1,1),(91,'Marketing Development Funds Training',1,1,1),(92,'Servicio especifico para companias ',1,1,3),(93,'Innovation Program4Partners',1,1,3),(94,'EUR 728.00',1,1,3),(95,'Marketing Booster',1,2,1),(96,'SEM for SAP Partners: Effectiveness and Collaboration',1,2,1),(97,'Data Profiling',1,2,2),(98,'Social Selling Concierge',1,2,2),(99,'Cloud Configurator Campaign',1,2,2),(100,'Blitz & Appointment Settings',1,2,2),(101,'Telemarketing',1,2,2),(102,'Always On Webinars',1,2,3),(103,'Videolicious',1,2,3),(104,'B1 Marketing Plan with SAP Guidance',1,2,4),(105,'Premium Marketing Services ',1,2,4),(106,'Grow you Pipeline with Virtual Agency',5,1,1),(107,'Social Selling Program ',5,1,1),(108,'Link Bulding',5,1,1),(109,'Customer Success Stories',5,1,1),(110,'Always On - Webinar On24',5,1,1),(111,'EveryOne Social',5,1,1),(112,'Marketing Development Funds Training',5,1,1),(113,'Servicio especifico para companias ',5,1,3),(114,'Innovation Program4Partners',5,1,3),(115,'EUR 728.00',5,1,3),(116,'Marketing Booster',5,2,1),(117,'SEM for SAP Partners: Effectiveness and Collaboration',5,2,1),(118,'Data Profiling',5,2,2),(119,'Social Selling Concierge',5,2,2),(120,'Blitz & Appointment Settings',5,2,2),(121,'Telemarketing',5,2,2),(122,'Always On Webinars',5,2,3),(123,'Customer Testimonial Videos',5,2,3),(124,'Videolicious',5,2,3),(125,'B1 Marketing Plan with SAP Guidance',5,2,4),(126,'Premium Marketing Services ',5,2,4),(127,'Digital Advertising ',5,4,3),(128,'Grow you Pipeline with Virtual Agency',6,1,1),(129,'Social Selling Program ',6,1,1),(130,'Link Bulding',6,1,1),(131,'Customer Success Stories',6,1,1),(132,'Always On - Webinar On24',6,1,1),(133,'EveryOne Social',6,1,1),(134,'Marketing Development Funds Training',6,1,1),(135,'Servicio especifico para companias ',6,1,3),(136,'Innovation Program4Partners',6,1,3),(137,'EUR 728.00',6,1,3),(138,'Marketing Booster',6,2,1),(139,'SEM for SAP Partners: Effectiveness and Collaboration',6,2,1),(140,'Data Profiling',6,2,2),(141,'Social Selling Concierge',6,2,2),(142,'Blitz & Appointment Settings',6,2,2),(143,'Telemarketing',6,2,2),(144,'Always On Webinars',6,2,3),(145,'Customer Testimonial Videos',6,2,3),(146,'Videolicious',6,2,3),(147,'B1 Marketing Plan with SAP Guidance',6,2,4),(148,'Premium Marketing Services ',6,2,4),(149,'Digital Advertising ',6,4,3),(150,'Grow you Pipeline with Virtual Agency',7,1,1),(151,'Social Selling Program ',7,1,1),(152,'Link Bulding',7,1,1),(153,'Customer Success Stories',7,1,1),(154,'Always On - Webinar On24',7,1,1),(155,'EveryOne Social',7,1,1),(156,'Marketing Development Funds Training',7,1,1),(157,'Servicio especifico para companias ',7,1,3),(158,'Innovation Program4Partners',7,1,3),(159,'EUR 728.00',7,1,3),(160,'Marketing Booster',7,2,1),(161,'SEM for SAP Partners: Effectiveness and Collaboration',7,2,1),(162,'Data Profiling',7,2,2),(163,'Social Selling Concierge',7,2,2),(164,'Blitz & Appointment Settings',7,2,2),(165,'Telemarketing',7,2,2),(166,'Always On Webinars',7,2,3),(167,'Customer Testimonial Videos',7,2,3),(168,'Videolicious',7,2,3),(169,'Premium Marketing Services ',7,2,4),(170,'Digital Advertising ',7,4,3),(171,'Grow you Pipeline with Virtual Agency',8,1,1),(172,'Social Selling Program ',8,1,1),(173,'Link Bulding',8,1,1),(174,'Customer Success Stories',8,1,1),(175,'Always On - Webinar On24',8,1,1),(176,'EveryOne Social',8,1,1),(177,'Marketing Development Funds Training',8,1,1),(178,'Servicio especifico para companias ',8,1,3),(179,'Innovation Program4Partners',8,1,3),(180,'EUR 728.00',8,1,3),(181,'Marketing Booster',8,2,1),(182,'Data Profiling',8,2,2),(183,'Social Selling Concierge',8,2,2),(184,'Blitz & Appointment Settings',8,2,2),(185,'Telemarketing',8,2,2),(186,'Always On Webinars',8,2,3),(187,'Customer Testimonial Videos',8,2,3),(188,'Videolicious',8,2,3),(189,'B1 Marketing Plan with SAP Guidance',8,2,4),(190,'Premium Marketing Services ',8,2,4),(191,'Digital Advertising ',8,4,3),(192,'Grow you Pipeline with Virtual Agency',9,1,1),(193,'Social Selling Program ',9,1,1),(194,'Link Bulding',9,1,1),(195,'Customer Success Stories',9,1,1),(196,'Always On - Webinar On24',9,1,1),(197,'EveryOne Social',9,1,1),(198,'Marketing Development Funds Training',9,1,1),(199,'Servicio especifico para companias ',9,1,3),(200,'Innovation Program4Partners',9,1,3),(201,'EUR 728.00',9,1,3),(202,'Marketing Booster',9,2,1),(203,'SEM for SAP Partners: Effectiveness and Collaboration',9,2,1),(204,'Data Profiling',9,2,2),(205,'Social Selling Concierge',9,2,2),(206,'Blitz & Appointment Settings',9,2,2),(207,'Telemarketing',9,2,2),(208,'Always On Webinars',9,2,3),(209,'Customer Testimonial Videos',9,2,3),(210,'Videolicious',9,2,3),(211,'B1 Marketing Plan with SAP Guidance',9,2,4),(212,'Premium Marketing Services ',9,2,4),(213,'Digital Advertising ',9,4,3),(214,'Grow you Pipeline with Virtual Agency',10,1,1),(215,'Social Selling Program ',10,1,1),(216,'Link Bulding',10,1,1),(217,'Customer Success Stories',10,1,1),(218,'Always On - Webinar On24',10,1,1),(219,'EveryOne Social',10,1,1),(220,'Marketing Development Funds Training',10,1,1),(221,'Servicio especifico para companias ',10,1,3),(222,'Innovation Program4Partners',10,1,3),(223,'EUR 728.00',10,1,3),(224,'SAP Business One Configurator',10,2,4),(225,'Grow you Pipeline with Virtual Agency',21,1,1),(226,'Social Selling Program ',21,1,1),(227,'Link Bulding',21,1,1),(228,'Customer Success Stories',21,1,1),(229,'Always On - Webinar On24',21,1,1),(230,'EveryOne Social',21,1,1),(231,'Marketing Development Funds Training',21,1,1),(232,'Servicio especifico para companias ',21,1,3),(233,'Innovation Program4Partners',21,1,3),(234,'EUR 728.00',21,1,3),(235,'Marketing Booster',21,2,1),(236,'SEM for SAP Partners: Effectiveness and Collaboration',21,2,1),(237,'Data Profiling',21,2,2),(238,'Social Selling Concierge',21,2,2),(239,'Cloud Configurator Campaign',21,2,2),(240,'Blitz & Appointment Settings',21,2,2),(241,'Telemarketing',21,2,2),(242,'Always On Webinars',21,2,3),(243,'Videolicious',21,2,3),(244,'B1 Marketing Plan with SAP Guidance',21,2,4),(245,'Premium Marketing Services ',21,2,4),(246,'Grow you Pipeline with Virtual Agency',11,1,1),(247,'Social Selling Program ',11,1,1),(248,'Link Bulding',11,1,1),(249,'Customer Success Stories',11,1,1),(250,'Always On - Webinar On24',11,1,1),(251,'EveryOne Social',11,1,1),(252,'Marketing Development Funds Training',11,1,1),(253,'Servicio especifico para companias ',11,1,3),(254,'Innovation Program4Partners',11,1,3),(255,'EUR 728.00',11,1,3),(256,'Marketing Booster',11,2,1),(257,'SEM for SAP Partners: Effectiveness and Collaboration',11,2,1),(258,'Data Profiling',11,2,2),(259,'Social Selling Concierge',11,2,2),(260,'Blitz & Appointment Settings',11,2,2),(261,'Telemarketing',11,2,2),(262,'Always On Webinars',11,2,3),(263,'Customer Testimonial Videos',11,2,3),(264,'Videolicious',11,2,3),(265,'Premium Marketing Services ',11,2,4),(266,'Digital Advertising ',11,4,3),(267,'Grow you Pipeline with Virtual Agency',12,1,1),(268,'Social Selling Program ',12,1,1),(269,'Link Bulding',12,1,1),(270,'Customer Success Stories',12,1,1),(271,'Always On - Webinar On24',12,1,1),(272,'EveryOne Social',12,1,1),(273,'Marketing Development Funds Training',12,1,1),(274,'Servicio especifico para companias ',12,1,3),(275,'Innovation Program4Partners',12,1,3),(276,'EUR 728.00',12,1,3),(277,'Marketing Booster',12,2,1),(278,'SEM for SAP Partners: Effectiveness and Collaboration',12,2,1),(279,'Data Profiling',12,2,2),(280,'Social Selling Concierge',12,2,2),(281,'Blitz & Appointment Settings',12,2,2),(282,'Telemarketing',12,2,2),(283,'Always On Webinars',12,2,3),(284,'Customer Testimonial Videos',12,2,3),(285,'Videolicious',12,2,3),(286,'B1 Marketing Plan with SAP Guidance',12,2,4),(287,'Premium Marketing Services ',12,2,4),(288,'Digital Advertising ',12,4,3),(289,'Grow you Pipeline with Virtual Agency',13,1,1),(290,'Social Selling Program ',13,1,1),(291,'Link Bulding',13,1,1),(292,'Customer Success Stories',13,1,1),(293,'Always On - Webinar On24',13,1,1),(294,'EveryOne Social',13,1,1),(295,'Marketing Development Funds Training',13,1,1),(296,'Servicio especifico para companias ',13,1,3),(297,'Innovation Program4Partners',13,1,3),(298,'EUR 728.00',13,1,3),(299,'Marketing Booster',13,2,1),(300,'SEM for SAP Partners: Effectiveness and Collaboration',13,2,1),(301,'Data Profiling',13,2,2),(302,'Social Selling Concierge',13,2,2),(303,'Blitz & Appointment Settings',13,2,2),(304,'Telemarketing',13,2,2),(305,'Always On Webinars',13,2,3),(306,'Videolicious',13,2,3),(307,'Premium Marketing Services ',13,2,4),(308,'Grow you Pipeline with Virtual Agency',14,1,1),(309,'Social Selling Program ',14,1,1),(310,'Link Bulding',14,1,1),(311,'Customer Success Stories',14,1,1),(312,'Always On - Webinar On24',14,1,1),(313,'EveryOne Social',14,1,1),(314,'Marketing Development Funds Training',14,1,1),(315,'Servicio especifico para companias ',14,1,3),(316,'Innovation Program4Partners',14,1,3),(317,'EUR 728.00',14,1,3),(318,'SEM for SAP Partners: Effectiveness and Collaboration',14,2,1),(319,'Data Profiling',14,2,2),(320,'Social Selling Concierge',14,2,2),(321,'Cloud Configurator Campaign',14,2,2),(322,'Blitz & Appointment Settings',14,2,2),(323,'Telemarketing',14,2,2),(324,'Always On Webinars',14,2,3),(325,'Testimonial Video',14,2,3),(326,'Videolicious',14,2,3),(327,'B1 Marketing Plan with SAP Guidance',14,2,4),(328,'Premium Marketing Services ',14,2,4),(329,'Grow you Pipeline with Virtual Agency',15,1,1),(330,'Social Selling Program ',15,1,1),(331,'Link Bulding',15,1,1),(332,'Customer Success Stories',15,1,1),(333,'Always On - Webinar On24',15,1,1),(334,'EveryOne Social',15,1,1),(335,'Marketing Development Funds Training',15,1,1),(336,'Servicio especifico para companias ',15,1,3),(337,'Innovation Program4Partners',15,1,3),(338,'EUR 728.00',15,1,3),(339,'Marketing Booster',15,2,1),(340,'SEM for SAP Partners: Effectiveness and Collaboration',15,2,1),(341,'Data Profiling',15,2,2),(342,'Social Selling Concierge',15,2,2),(343,'Blitz & Appointment Settings',15,2,2),(344,'Always On Webinars',15,2,3),(345,'Customer Testimonial Videos',15,2,3),(346,'Videolicious',15,2,3),(347,'B1 Marketing Plan with SAP Guidance',15,2,4),(348,'SAP Busines One Configurator',15,2,4),(349,'Premium Marketing Services ',15,2,4),(350,'Digital Advertising ',15,4,3),(351,'Grow you Pipeline with Virtual Agency',16,1,1),(352,'Social Selling Program ',16,1,1),(353,'Link Bulding',16,1,1),(354,'Customer Success Stories',16,1,1),(355,'Always On - Webinar On24',16,1,1),(356,'EveryOne Social',16,1,1),(357,'Marketing Development Funds Training',16,1,1),(358,'Servicio especifico para companias ',16,1,3),(359,'Innovation Program4Partners',16,1,3),(360,'EUR 728.00',16,1,3),(361,'Marketing Booster',16,2,1),(362,'SEM for SAP Partners: Effectiveness and Collaboration',16,2,1),(363,'Data Profiling',16,2,2),(364,'Social Selling Concierge',16,2,2),(365,'Blitz & Appointment Settings',16,2,2),(366,'Telemarketing',16,2,2),(367,'Always On Webinars',16,2,3),(368,'Customer Testimonial Videos',16,2,3),(369,'Videolicious',16,2,3),(370,'Premium Marketing Services ',16,2,4),(371,'Digital Advertising ',16,4,3),(372,'Grow you Pipeline with Virtual Agency',17,1,1),(373,'Social Selling Program ',17,1,1),(374,'Link Bulding',17,1,1),(375,'Customer Success Stories',17,1,1),(376,'Always On - Webinar On24',17,1,1),(377,'EveryOne Social',17,1,1),(378,'Marketing Development Funds Training',17,1,1),(379,'Servicio especifico para companias ',17,1,3),(380,'Innovation Program4Partners',17,1,3),(381,'EUR 728.00',17,1,3),(382,'Social Selling Concierge',17,2,2),(383,'B1 Marketing Plan with SAP Guidance',17,2,4),(384,'SAP Business One Configurator',17,2,4),(385,'Premium Marketing Services ',17,2,4),(386,'Grow you Pipeline with Virtual Agency',18,1,1),(387,'Social Selling Program ',18,1,1),(388,'Link Bulding',18,1,1),(389,'Customer Success Stories',18,1,1),(390,'Always On - Webinar On24',18,1,1),(391,'EveryOne Social',18,1,1),(392,'Marketing Development Funds Training',18,1,1),(393,'Servicio especifico para companias ',18,1,3),(394,'Innovation Program4Partners',18,1,3),(395,'EUR 728.00',18,1,3),(396,'Marketing Booster',18,2,1),(397,'SEM for SAP Partners: Effectiveness and Collaboration',18,2,1),(398,'Data Profiling',18,2,2),(399,'Social Selling Concierge',18,2,2),(400,'Telemarketing',18,2,2),(401,'Videolicious',18,2,3),(402,'Premium Marketing Services ',18,2,4),(403,'Grow you Pipeline with Virtual Agency',19,1,1),(404,'Social Selling Program ',19,1,1),(405,'Link Bulding',19,1,1),(406,'Customer Success Stories',19,1,1),(407,'Always On - Webinar On24',19,1,1),(408,'EveryOne Social',19,1,1),(409,'Marketing Development Funds Training',19,1,1),(410,'Servicio especifico para companias ',19,1,3),(411,'Innovation Program4Partners',19,1,3),(412,'EUR 728.00',19,1,3),(413,'Data Profiling',19,2,2),(414,'Social Selling Concierge',19,2,2),(415,'Customer Testimonial Videos',19,2,3),(416,'Videolicious',19,2,3),(417,'B1 Marketing Plan with SAP Guidance',19,2,4),(418,'Premium Marketing Services ',19,2,4),(422,'Innovation Program4Partners',1,5,5);
/*!40000 ALTER TABLE `servicio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_servicio`
--

DROP TABLE IF EXISTS `tipo_servicio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_servicio` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Tipo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_servicio`
--

LOCK TABLES `tipo_servicio` WRITE;
/*!40000 ALTER TABLE `tipo_servicio` DISABLE KEYS */;
INSERT INTO `tipo_servicio` VALUES (1,'Digital Optimization'),(2,'Demand Generation'),(3,'Digital Content'),(4,'Marketing Strategy'),(5,'Innovation Program4Partners');
/*!40000 ALTER TABLE `tipo_servicio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombres` varchar(255) DEFAULT NULL,
  `Apellidos` varchar(255) DEFAULT NULL,
  `Pais` varchar(100) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `Usuario` varchar(100) DEFAULT NULL,
  `partner_id` text,
  `pass` text,
  `partner_name` varchar(100) DEFAULT NULL,
  `monto` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (2,'Jhonatan','Iberico','Colombia','jhiberico@gmail.com','Disponible','698634','123','Jsociety','4,500'),(3,'Jose','Minaya','Perú','jose.minayac15@gmail.com','N/A','201411688','','Jsociety','10,000');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `ecb_palo_alto`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `ecb_palo_alto` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `ecb_palo_alto`;

--
-- Table structure for table `contacto_emergencia`
--

DROP TABLE IF EXISTS `contacto_emergencia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacto_emergencia` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `relacion` varchar(50) NOT NULL,
  `adicional` varchar(250) NOT NULL,
  `_id_negocio` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacto_emergencia`
--

LOCK TABLES `contacto_emergencia` WRITE;
/*!40000 ALTER TABLE `contacto_emergencia` DISABLE KEYS */;
INSERT INTO `contacto_emergencia` VALUES (7,'Olivia Frank','12687207832','Wife','N/A',11);
/*!40000 ALTER TABLE `contacto_emergencia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itinerario`
--

DROP TABLE IF EXISTS `itinerario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itinerario` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_reconocimiento` varchar(35) NOT NULL,
  `restriccion_dieta` varchar(150) NOT NULL,
  `fecha_check_in` date NOT NULL,
  `fecha_check_out` date NOT NULL,
  `necesidad_especial` varchar(50) NOT NULL,
  `vuelo_ida_origen` varchar(75) NOT NULL,
  `vuelo_ida_destino` varchar(75) NOT NULL,
  `dia_vuelo_ida` date NOT NULL,
  `hora_vuelo_ida` varchar(15) NOT NULL,
  `vuelo_retorno_origen` varchar(75) NOT NULL,
  `vuelo_retorno_destino` varchar(75) NOT NULL,
  `dia_vuelo_retorno` date NOT NULL,
  `hora_vuelo_retorno` varchar(15) NOT NULL,
  `aerolinea1` varchar(50) NOT NULL,
  `codigo_aero_1` varchar(30) NOT NULL,
  `aerolinea2` varchar(50) NOT NULL,
  `codigo_aero_2` varchar(30) NOT NULL,
  `aerolinea3` varchar(50) NOT NULL,
  `codigo_aero_3` varchar(30) NOT NULL,
  `confirmacion` varchar(75) NOT NULL,
  `ultima_dia_modificacion` date NOT NULL,
  `ultimo_dia_cancela` date NOT NULL,
  `_id_negocio` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itinerario`
--

LOCK TABLES `itinerario` WRITE;
/*!40000 ALTER TABLE `itinerario` DISABLE KEYS */;
INSERT INTO `itinerario` VALUES (7,'Sidlow Frank','N/A','2018-07-23','2018-07-26','N/A','ANU','aa','2018-07-23','afternon','SJC','aa','2018-07-26','evening','Delta','N/A','American','N/A','Jet Blue','N/A','aa','0000-00-00','0000-00-00',11);
/*!40000 ALTER TABLE `itinerario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `negocio`
--

DROP TABLE IF EXISTS `negocio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `negocio` (
  `id_negocio` int(11) NOT NULL AUTO_INCREMENT,
  `primer_nombre` varchar(25) NOT NULL,
  `segundo_nombre` varchar(25) NOT NULL,
  `apellidos` varchar(50) NOT NULL,
  `empresa` varchar(35) NOT NULL,
  `cargo` varchar(35) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `correo` varchar(50) NOT NULL,
  `ciudad` varchar(30) NOT NULL,
  `pais` varchar(30) NOT NULL,
  `fecha_registro` date DEFAULT NULL,
  PRIMARY KEY (`id_negocio`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `negocio`
--

LOCK TABLES `negocio` WRITE;
/*!40000 ALTER TABLE `negocio` DISABLE KEYS */;
INSERT INTO `negocio` VALUES (11,'Sidlow','E','Frank','Antigua Commercial Bank','Manager - Information Systems','12687644371','sfrank@acbonline.com','St. John\'s','Antigua and Barbuda','0000-00-00');
/*!40000 ALTER TABLE `negocio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persona`
--

DROP TABLE IF EXISTS `persona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persona` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `nu_pasaporte` varchar(15) NOT NULL,
  `fecha_emision` date NOT NULL,
  `fecha_expiro` date NOT NULL,
  `pais_emisor` varchar(25) NOT NULL,
  `genero` varchar(15) NOT NULL COMMENT '1: femenino ; 0: Masculino',
  `fecha_cumple` date NOT NULL,
  `nacionalidad` varchar(25) NOT NULL,
  `ciudad_referencia` varchar(30) NOT NULL,
  `flg_invitacion` int(11) NOT NULL COMMENT '1: Si quiere invitacion;  0: no quiere invitacion',
  `asiento_preferencia` varchar(30) NOT NULL,
  `talla_polo` varchar(15) NOT NULL,
  `_id_negocio` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persona`
--

LOCK TABLES `persona` WRITE;
/*!40000 ALTER TABLE `persona` DISABLE KEYS */;
INSERT INTO `persona` VALUES (10,'RE0065256','2017-12-27','2027-12-26','St. Kitts and Nevis','Male','1975-10-15','Kittitian','Antigua and Barbuda',0,'Aisle','XX-Large',11);
/*!40000 ALTER TABLE `persona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `evento_aruba_training`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `evento_aruba_training` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `evento_aruba_training`;

--
-- Table structure for table `participante`
--

DROP TABLE IF EXISTS `participante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `participante` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `telefono` varchar(50) NOT NULL,
  `empresa` varchar(50) NOT NULL,
  `cargo` varchar(50) NOT NULL,
  `pais` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participante`
--

LOCK TABLES `participante` WRITE;
/*!40000 ALTER TABLE `participante` DISABLE KEYS */;
/*!40000 ALTER TABLE `participante` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `gana_por_goleada`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `gana_por_goleada` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `gana_por_goleada`;

--
-- Table structure for table `anotaciones`
--

DROP TABLE IF EXISTS `anotaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anotaciones` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Empresa` varchar(100) DEFAULT NULL,
  `Deal_registration` int(11) DEFAULT NULL,
  `Pais` varchar(100) DEFAULT NULL,
  `Flag` int(11) DEFAULT NULL COMMENT '1 es  PENDIENTE; 2 es ACEPTADO; 3 es RECHAZADO; 4 es OBSERVADO',
  `Goles` int(11) DEFAULT NULL,
  `Id_serv` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `alertas` int(11) DEFAULT NULL,
  `flg_pais` int(11) DEFAULT NULL,
  `descripcion` varchar(500) DEFAULT NULL,
  `fecha` date NOT NULL,
  `lenguaje` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=165 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anotaciones`
--

LOCK TABLES `anotaciones` WRITE;
/*!40000 ALTER TABLE `anotaciones` DISABLE KEYS */;
INSERT INTO `anotaciones` VALUES (4,'Test',172367,'Peru',3,3,1,3,3,1,NULL,'0000-00-00','Español'),(5,'pepe lepu',170987,'arg',3,2,2,5,NULL,1,NULL,'0000-00-00','Español'),(12,'test',655656,'test',1,3,5,2,NULL,1,'','0000-00-00','Inglés'),(13,'test',656521,'test',2,3,1,2,NULL,1,'','0000-00-00','Español'),(14,'test',0,'test',2,4,1,2,NULL,1,'test','0000-00-00','Español'),(15,'AAF Brasil Comércio de Alimentos para animais Ltda.',0,'Brasil',2,4,1,12,2,1,'New name','0000-00-00','Español'),(16,'Artha Capital ',286106,'México',2,4,1,17,NULL,1,'','0000-00-00','Español'),(17,'Termomecanica do Brasil S.A.',292528,'Brasil',4,4,5,19,4,1,'','0000-00-00','Español'),(18,'Termomecanica',292528,'Brasil',1,4,5,19,NULL,1,'','0000-00-00','Español'),(19,'Termomecanica',292528,'Brasil',3,3,1,19,3,1,'','0000-00-00','Español'),(20,'Termomecanica',292528,'Brasil',3,3,3,19,3,1,'','0000-00-00','Español'),(21,'Dr Promaq',0,'Brasil',2,4,1,13,NULL,1,'\"Fabricamos máquinas e equipamentos sob encomenda e gostaria de saber se vocês possuem algum software desenvolvido para este tipo de segmento\"','0000-00-00','Español'),(22,'Copasa do Brasil',0,'Brasil',3,4,1,13,NULL,1,'Lead carrossel SAP - [ALFA ERP BRA] Lead:64095 - Sociedad Anonima De Obras Y Servicios Copasa Do Brasil','0000-00-00','Español'),(23,'Cereais Signor',0,'Brasil',2,4,1,13,NULL,1,'Lead carrossel SAP - [ALFA ERP Brasil]   Lead #64213# | CEREAIS SIGNOR LTDA.','0000-00-00','Español'),(24,'JTP Transportes',0,'Brasil',2,4,1,13,NULL,1,'Lead carrossel SAP - [ALFA ERP BRA] Lead #64079# | JTP Transportes, Serviços, Gerenciamento e Recursos Humanos LTDA.','0000-00-00','Español'),(25,'Suplax',0,'Brasil',2,4,1,13,NULL,1,'Felipe - Proprietário, entrou em contato interessado no SAP Business One ','0000-00-00','Español'),(26,'CAF MAQUINAS',0,'Brasil',1,4,6,13,NULL,1,'Lead carrossel SAP - [ALFA ERP BRA] Lead #64082# | Cristina Aparecida Frederich & Cia Ltda ','0000-00-00','Español'),(27,'CAF',0,'Brasil',1,4,1,13,1,1,'Lead carrossel SAP - [ALFA ERP BRA] Lead #64082# | Cristina Aparecida Frederich & Cia Ltda.','0000-00-00','Español'),(28,'Max Vinil',0,'Brasil',2,4,1,13,NULL,1,'Outbound marketing - Reunião agendada para dia 07/06','0000-00-00','Español'),(29,'Bizup',0,'Brasil',2,4,1,13,NULL,1,'Lead outbound marketing - reunião agendada para 06 de junho','0000-00-00','Español'),(30,'Fette Compacting',0,'Brasil',2,4,1,13,NULL,1,'Outbound marketing - Reunião agendada para 05 de Junho','0000-00-00','Español'),(31,'Replace',0,'Brasil',2,4,1,13,NULL,1,'Lead carrossel SAP - [ALFA ERP BRA] Lead #63880# | REPLACE PROJETOS E CONSULTORIA EM ENERGIA LTDA.','0000-00-00','Español'),(32,'Alchemy',0,'Brasil',1,4,1,13,NULL,1,'Outbound marketing - Reunião em 22 de maio.','0000-00-00','Español'),(33,'Universo Tintas',0,'Brasil',2,4,1,13,NULL,1,'Outbound marketing - Reunião em 23 de maio','0000-00-00','Español'),(34,'SAP Business One Configurator',0,'Bolivia',1,3,7,22,NULL,1,'Personalizacion para Actualisap','0000-00-00','Español'),(35,'CABLES DE ENERGIA Y TELECOMUNICACIONES CENTELSA S.A. ',283682,'Colombia',3,3,3,23,NULL,1,'','0000-00-00','Español'),(36,'CABLES DE ENERGIA Y TELECOMUNICACIONES CENTELSA S.A. ',302960,'Colombia',3,3,3,23,NULL,1,'','0000-00-00','Español'),(37,'Laboratorios Delta',0,'Bolivia',2,2,8,22,2,1,'Caso de Exito SAP Business One','0000-00-00','Español'),(38,'Colegio Aleman',0,'Bolivia',2,2,8,22,2,1,'Caso de éxito SAP Business One','0000-00-00','Español'),(39,'CIES',0,'Bolivia',2,2,8,22,2,1,'Caso de Exito Iniciativa Stand as One','0000-00-00','Español'),(40,'Fundacion Arco Iris',0,'Bolivia',2,2,8,22,2,1,'Caso de éxito iniciativa Stand as One','0000-00-00','Español'),(41,'ABE',0,'Bolivia',2,2,8,22,2,1,'Caso de Exito SAP Business One','0000-00-00','Español'),(42,'Fundacion Arco Iris',0,'Bolivia',3,2,8,22,3,1,'Caso de Exito Iniciativa Stand as One','0000-00-00','Español'),(43,'Business One Configurator',0,'Bolivia',1,3,7,22,3,1,'Campaña personalizada para Actualisap','0000-00-00','Español'),(44,'CABLES DE ENERGIA Y TELECOMUNICACIONES CENTELSA S.A. ',302943,'Colombia',1,3,3,NULL,NULL,1,'','0000-00-00',NULL),(45,'Productos RAMO',302980,'Colombia',1,3,3,NULL,NULL,1,'','0000-00-00',NULL),(46,'ADAMA',302981,'Colombia',3,4,5,23,NULL,1,'','0000-00-00','Español'),(47,'ADAMA',302975,' Colombia',3,3,3,23,NULL,1,'','0000-00-00','Español'),(48,'Mitsubishi',302981,'Colombia',3,3,3,23,NULL,1,'','0000-00-00','Español'),(49,'Mitsubishi',981286,'Colombia',3,3,3,23,3,1,'','0000-00-00','Español'),(50,'ADAMA',975026,'Colombia',3,3,3,23,NULL,1,'','0000-00-00','Español'),(51,'Apolo Spumas',0,'Brasil',1,4,1,13,NULL,1,'Outbound Marketing','0000-00-00','Portugués'),(52,'Inovar Predial',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(53,'Apice Grafica',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(54,'Borboleta Beauty',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(55,'Caterplast',0,'Brasil',1,4,1,13,NULL,1,'Lead encaminhado pela SAP - [ALFA ERP BRA] - Lead: 62103 - Caterplast Comercio de Plasticos e Borrachas','0000-00-00','Portugués'),(56,'Sandvik',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(57,'Hipcom Informatica',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(58,'Fanem',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(59,'Boening ',0,'Brasil',1,4,1,13,NULL,1,'Lead encaminhado pela SAP -  [ALFA ERP BRA] - L#62275# - Boening Imp E Com De Equip Nau Ltda','0000-00-00','Portugués'),(60,'Acqua Import',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(61,'Mazars',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(62,'Pamfis',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(63,'NDBIM',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(64,'Laticínios São Vicente',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(65,'Grupo Alísios',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(66,'Plavitec Adesivos',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(67,'Siarcon',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(68,'CCM Tecnologia',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(69,'THT Logistics',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(70,'Editora Referencia ',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(71,'Gyn Consult',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(72,'Clean Medical',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(73,'Sofist',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(74,'LinSel',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(75,'Focus Fit',0,'Brasil',1,2,8,13,NULL,1,'Marketing','0000-00-00','Portugués'),(76,'Havik',0,'Brasil',1,2,8,13,NULL,1,'Marketing','0000-00-00','Portugués'),(77,'LinSel',0,'Brasil',1,4,6,13,NULL,1,'Marketing','0000-00-00','Portugués'),(78,'Focus Fit',0,'Brasil',1,4,6,13,NULL,1,'Marketing','0000-00-00','Portugués'),(79,'Havik',0,'Brasil',1,4,6,13,NULL,1,'Marketing','0000-00-00','Portugués'),(80,'Grupo Oriente',0,'Brasil',1,4,6,13,NULL,1,'Marketing','0000-00-00','Portugués'),(81,'Wtime',0,'Brasil',1,4,6,13,NULL,1,'Marketing','0000-00-00','Portugués'),(82,'Delpim',0,'Brasil',1,4,6,13,NULL,1,'Lead encaminhado pela SAP -  [ALFA ERP BRA] Lead#60434# | Delpim Automação Comercial','0000-00-00','Portugués'),(83,'Pop Trade',0,'Brasil',1,4,6,13,NULL,1,'Marketing','0000-00-00','Portugués'),(84,'Plan Minas',0,'Brasil',1,4,6,13,NULL,1,'Marketing','0000-00-00','Portugués'),(85,'Plan Minas',0,'Brasil',1,4,6,13,NULL,1,'Marketing','0000-00-00','Portugués'),(86,'Mac Dermid',0,'Brasil',1,4,6,13,NULL,1,'Marketing','0000-00-00','Portugués'),(87,'Universo Tintas',0,'Brasil',1,4,6,13,NULL,1,'Marketing','0000-00-00','Portugués'),(88,'Alchemy',0,'Brasil',1,4,6,13,NULL,1,'Marketing','0000-00-00','Portugués'),(89,'Replace',0,'Brasil',1,4,6,13,NULL,1,'lead enviado pela SAP -  [ALFA ERP BRA] Lead #63880# | REPLACE PROJETOS E CONSULTORIA EM ENERGIA LTDA','0000-00-00','Portugués'),(90,'Fette Compacting ',0,'Brasil',1,4,6,13,NULL,1,'Marketing','0000-00-00','Portugués'),(91,'Integra Voip',0,'Brasil',1,4,6,13,NULL,1,'Marketing','0000-00-00','Portugués'),(92,'Max Vinil',0,'Brasil',1,4,6,13,NULL,1,'Marketing','0000-00-00','Portugués'),(93,'Suplax ',0,'Brasil',1,4,6,13,NULL,1,'Marketing','0000-00-00','Portugués'),(94,'CAF Maquinas',0,'Brasil',1,4,6,13,NULL,1,'Lead encaminhado pela SAP - [ALFA ERP BRA] Lead #64082# | Cristina Aparecida Frederich & Cia Ltda ','0000-00-00','Portugués'),(95,'JTP Transportes',0,'Brasil',1,4,6,13,NULL,1,'lead enviado pela SAP -  [ALFA ERP BRA] Lead #64079# | JTP Transportes, Serviços, Gerenciamento e Recursos Humanos LTDA','0000-00-00','Portugués'),(96,'Cereais Signor',0,'Brasil',1,4,6,13,NULL,1,'Lead encaminhado pela SAP - [ALFA ERP Brasil]   Lead #64213# | CEREAIS SIGNOR LTDA','0000-00-00','Portugués'),(97,'Copasa do Brasil',0,'Brasil',1,4,6,13,NULL,1,'Lead encaminhado pela SAP - [ALFA ERP BRA] Lead:64095 - Sociedad Anonima De Obras Y Servicios Copasa Do Brasil','0000-00-00','Portugués'),(98,'Dr Promaq',0,'Brasil',1,4,6,13,NULL,1,'Marketing','0000-00-00','Portugués'),(99,'Correias Sincron',0,'Brasil',1,4,6,13,NULL,1,'Marketing','0000-00-00','Portugués'),(100,'Agricola Himalaya S.A.',294102,'Colombia',2,3,1,23,NULL,1,'','0000-00-00','Español'),(101,'ZippClub',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(102,'A2 Vidros',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(103,'K2TEC',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(104,'Omelet',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(105,'Escudero',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(106,'Massa X',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(107,'NetWan',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(108,'Uniformes e Cia',0,'Brasil',1,4,1,13,NULL,1,'Lead encaminhado pela SAP - Lead#58944# -  Uniformes e Cia','0000-00-00','Portugués'),(109,'MECFLU',0,'Brasil',1,4,1,13,NULL,1,'Lead encaminhado pela SAP -  Lead #60336# | MECFLU - INDUSTRIA, COMERCIO E PRESTACAO DE SERVICOS DE VEDACOES INDUSTRIAIS LTDA','0000-00-00','Portugués'),(110,'KLG Comercio',0,'Brasil',1,4,1,13,NULL,1,'Lead encaminhado pela SAP - [ALFA ERP BRA] Lead #60325# | KLG COMERCIO & MANUTENCAO DE MAQUINAS LTDA','0000-00-00','Portugués'),(111,'BWB Embalagens',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(112,'Agropecuria Santa Teresinha',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(113,'Grupo Bringel',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(114,'Amendoas do Brasil',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(115,'Harty Cosméticos (Nick Vick)',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(116,'Azimute',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(117,'Ag Gerenciamento',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(118,'Marbo',0,'Brasil',1,4,1,13,NULL,1,'Nerus','0000-00-00','Portugués'),(119,'Artrans',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(120,'Sage',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(121,'PWR Comercializadora',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(122,'Jemp',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(123,'Grupo Ad2M',0,'Brasil',1,4,1,13,NULL,1,'Nerus','0000-00-00','Portugués'),(124,'Oriê',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(125,'Grupo Siqueira',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(126,'CNPH ',0,'Brasil',1,4,1,13,NULL,1,'Lead enviado pela SAP -  [ALFA ERP BRA] Lead #61431# | CNPH Logística e Serviços Ltda','0000-00-00','Portugués'),(127,'Grupo Vivo Sabor',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(128,'Gourmet Sports',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(129,'Casa do Braille',0,'Brasil',1,4,1,13,NULL,1,'Lead encaminhado pela SAP - [Alfa ERP BR] Lead #55070# | Casa do Braille Sinalizacao Visual e Tatil Ltda - ME','0000-00-00','Portugués'),(130,'HS Lingerie ',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(131,'DDTudo',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(132,'El shaddai',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(133,'PinBank',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(134,'LumiTotem',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(135,'Expreso Brasilia ',294362,'Colombia',3,3,3,23,NULL,1,'','0000-00-00','Español'),(136,'Rossbach sa de cv',323232,'mexico',1,2,2,48,NULL,1,'','0000-00-00','Español'),(137,'CREDIBROKER SA DE CV',292060,'México',3,4,1,17,3,1,'','0000-00-00','Español'),(138,'TSA Cargo',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(139,'Transciard',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(140,'Atletico Mineiro',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(141,'Eletronica 43',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(142,'Pare Bem',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(143,'Qualy Cestas',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(144,'Marrucci',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(145,'Dristrifilm',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(146,'Lola Cosméticos',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(147,'Boyler Alimentos',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(148,'Obra Prima',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(149,'Acqualur',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(150,'Pharma Essencia',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(151,'Puket',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(152,'Minnas',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(153,'ControllTec ',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(154,'Shinsung',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(155,'Qualitec',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(156,'Eplast',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(157,'Lilibox',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(158,'Click Bus',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(159,'Domi Nick',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(160,'Nutri Saúde',0,'Brasil',1,4,1,13,NULL,1,'Marketing','0000-00-00','Portugués'),(161,'Seguricel',201412,'Perú',1,4,1,1,NULL,1,'','0000-00-00','Español'),(162,'Seguricel',201412,'Perú',1,4,5,1,NULL,1,'','2018-06-20','Español'),(163,'jose',172223,'peru',2,4,5,2,2,1,'','2018-08-31','Español'),(164,'jose',171234,'peru',1,4,5,2,NULL,1,'','2018-08-31','Español');
/*!40000 ALTER TABLE `anotaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servicios`
--

DROP TABLE IF EXISTS `servicios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servicios` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Tipo_serv` varchar(100) DEFAULT NULL,
  `Total` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servicios`
--

LOCK TABLES `servicios` WRITE;
/*!40000 ALTER TABLE `servicios` DISABLE KEYS */;
INSERT INTO `servicios` VALUES (1,'Cuentas nuevas (NNN)',0),(2,'Oportunidades generadas de Social Selling',0),(3,'Oportunidades generadas para Cloud',0),(4,'Casos de éxitos de clientes aprobados',0),(5,'Won & Booked (W/B)',0),(6,'# Oportunidades generadas para B1',0),(7,'# Campañas ejecutadas desde la Agencia Virtual',0),(8,'Casos de Referencias de B1 aprobados por SAP',0),(9,'Won & Booked (W/B) pt',0);
/*!40000 ALTER TABLE `servicios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre_capitan` varchar(255) DEFAULT NULL,
  `usuario` varchar(255) DEFAULT NULL,
  `pass` text,
  `Nombre_canal` varchar(255) DEFAULT NULL,
  `Pais` varchar(100) DEFAULT NULL,
  `partner_id` text,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Pedro Yturrizaga','pyf136@gmail.com','MTIz','Merino','Perù','201412259'),(2,'Mauricio Merino','mauricio@merino.com.pe','MTIzNDU2','Merino','Perù','1310156'),(3,'anali vidal','anali.vidal@sap.com','MTcwOTA2anA=','test','Perù','761121'),(4,'Katia Alarcon','katia.alarcon@sap.com','SWlzbGVtMDQ=','GC&GB Mexico','México','1241412'),(5,'Carina Gonzalez','carina.gonzalez@sap.com','Y2FyaTEyMw==','SAP','Argentina','452461'),(6,'Bruna Goncalves','bruna.goncalves@sap.com','TWFya2V0aW5n','SAP','Brasil','1076618'),(7,'Carina Gonzalez','carugonzalez@gmail.com','QWxpY2lhMTQw','SAP','Argentina','452461'),(8,'Giovanna Bergamo','giovanna.bergamo@sap.com','MTIzNDU=','aaaa','Brasil','1076618'),(11,'Bruna Goncalves','bruna.goncalves@sap.com','TWlsbHlsaW5kYTEw','Teste','Brasil','1076618'),(12,'Priscila Massuda','priscila@g2tecnologia.com.br','ZzJzYXBnMg==','G2 Tecnologia','Brasil','862540'),(13,'Alexandro Dias','fabiane.nunes@alfaerp.com.br','MjIwMzA4','ALFA Sistemas','Brasil','1076618'),(14,'Gabriela Ortega ','ortega.gabriela@dintec.com','R29sZWFkYTE1Mw==','Dintec','México','1241412'),(15,'Luis Alejandro Suárez Arrañaga','luis.suarez@interlatin.com.mx','bHVpc3VhcmV6Ljg5','Partner','México','805234'),(16,'Grethel Alfaro Rojas','galfaro@scgint.com','ZXN0YWNpb242Mg==','SCG','Costa Rica','1381993'),(17,'Eduardo Herrera Acero','eherrera@tesselar.mx','VGVzczIwMTg=','Tesselar','México','651160'),(18,'Juliana Miler','juliana.miler@megawork.com','bWVnYXdvcmsxMA==','Megawork ','Brasil','832647'),(19,'Ricardo Nobrega da Silva','ricardo.nobrega@intelligenzait.com','U2FwU0ZhY3RvcnMh','Intelligenza','Brasil','1306824'),(20,'Alexander ','adelacuadra@seidor.com','c2lzdGVtYXMx','De la Cuadra','Colombia','1357254'),(21,'Pablo Valero','pablo.valero@inflexyon.mx','QmFyY2Vsb25hNzA=','Inflexyon','México','651160'),(22,'Alejandra Sanchez','asanchez@actualisapbolivia.com','QWN0dWFsaXNhcDIwMTg=','ACTUALISAP CONSULTORES BOLIVIA SA','Bolivia','1252859'),(23,'Luz Orfid Giraldo','luz.giraldo@grupocnet.com','Q29sb21iaWExMw==','COMPUNET S.A.','Colombia','635303'),(24,'Jorge Roca','jroca@seidor.com','QWRpb3MyMDE4','SEIDOR','Perú','1348080'),(25,'Juan Miranda','miranda.juan@dintec.com','Y29uc3VsdG9yMTk3Nw==','Dintec','México','1113124'),(26,'Paola Bustos','pbustos@seidor.com','TWFya2V0aW5nMjAxOCo=','Seidor Colombia SAS','Colombia','1357254'),(27,'Julio Tomé Isidro','jtome@inxap.cl','TGVzdGVyMTcxMQ==','Inxap','Chile','1361695'),(28,'Fernando Sciaccaluga','fernando.sciaccaluga@bghtechpartner.com','QXJpYXMxNjM5','Latinware S.A.','Argentina','1178339'),(29,'Arthur Berni de Marque','arthur_bat@hotmail.com','Rm9ydGFsMDAx','FH Consulting','Brasil','855982'),(30,'Guilherme Vissotto','gvissotto@bci-consulting.com','YnlhN3FpZjc=','BCI Consulting','Brasil','1106782'),(31,'RICARDO FIRMADO','ricardo.firmado@capgemini.com','VDN0cmlzMSo=','CAPGEMINI','Brasil','486145'),(32,'Arthur Lima da Silva','arthur.silva@meta.com.br','YXJpc3RpZGVzMQ==','Meta','Brasil','699483'),(33,'Ivonn Flores ','aflores@agasys.mx','QWdhc3lzMjAxNw==','agasys','México','826672'),(34,'Graciela Vianey Luna Ramos','vianey.luna@i-growconsulting.com','cmVnaW5hLTA0','i-Grow Consulting','México','1585825'),(35,'Guadalupe Ruiz Padilla','guadalupe_ruiz@adop-tec.com','T3JhY2xlMTIz','AdopTec','México','1253685'),(36,'Cleiton Silva','cleiton.silva@runsistemas.com.br','MjM1Njg5','Run Sistemas','Brasil','1598903'),(37,'Loreto Valencia ','lvalencia@henley.cl','ZWxlZ3VyLjI=','HENLEY','Chile','1201134'),(38,'Yanira Felix','respinal@gbm.net','RG9taW5vZ2JtMCQ=','GBM Dominicana','República Dominicana','782187'),(39,'HENRIQUE OGAWA','Henrique.ogawa@fh.com.br','RjNycmFyaUAj','FH CONSULTING','Brasil','855982'),(40,'Deborah Machuret','deborah.machuret@linktech.com.mx','UzAwMTQyNTU4NjU=','Link Technologies S.A.P:I de C.V','México','708379'),(41,'Cristian Orellana ','cristian.orellana@exxis-group.com','K3NhcDIwMTg=','EXXIS Ecuador','Ecuador','1252117'),(42,'Alejandra Del Valle','alejandra.delvalle@cvmanagement.com.mx','MDFDdm1nMTg=','CVM Group & Management SA de CV','México','1427226'),(43,'cristian orellana','cristian.orellana@exxis-group.com','K3NhcDIwMTk=','EXXIS Chile','Chile','697739'),(44,'Claudia Rosario Flores Vidal ','cflores@topconsultingroup.com','U2FudGlhZ28yNQ==','Top Consulting Group ','Perú','1539487'),(45,'Claudia Flores Vidal ','cflores@topconsultingroup.com','Y2xhdWRpYTI1Lg==','Top Consulting Group ','Perú','1539487'),(46,'Marcelo Gaban','marcelo.gaban@cidic.com.br','QnVzc29jYWJhMDU=','CIDIC Consultoria','Brasil','709829'),(47,'OSCAR MOJICA L','oscar.mojica@techedgegroup.com','Y29uZmlkZW5jaWFs','Techedge, Colombia','Colombia','657493'),(48,'Diana Lizbeth Arambula Rios','diana.arambula@interlatin.com.mx','UGVnYXNvMzI=','InterLatin','México','805234'),(49,'Ana Laura Hernández Camacho','ana.camacho@scanada.com.mx','c2NhbmRhMjA=','XAMAI','México','1241412'),(50,'Mayra Escandon','mayra.escandon@exxis-group.com','K3NhcDIwMTg=','ECUATECXXIS','Ecuador','1252117');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `heroes2.0`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `heroes2.0` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `heroes2.0`;

--
-- Table structure for table `detalle_puntaje`
--

DROP TABLE IF EXISTS `detalle_puntaje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_puntaje` (
  `id_detalle` int(11) NOT NULL AUTO_INCREMENT,
  `id_tipo_solicitud` int(11) DEFAULT NULL,
  `flg_aprobado` int(11) NOT NULL COMMENT '2: RECHAZADO ; 1: APROBADO; 0: NO APROBADO',
  `id_heroe` int(11) NOT NULL,
  `id_negocio` varchar(15) DEFAULT NULL,
  `nombre_cliente` varchar(75) DEFAULT NULL,
  `msj_victoria` varchar(150) DEFAULT NULL,
  `id_tipo_medalla` int(11) DEFAULT NULL,
  `url_contenido` varchar(75) DEFAULT NULL,
  `titulo_enlace` varchar(75) DEFAULT NULL,
  `fecha_ins` date DEFAULT NULL,
  `user_updt` int(11) DEFAULT NULL,
  `fecha_updt` date DEFAULT NULL,
  PRIMARY KEY (`id_detalle`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_puntaje`
--

LOCK TABLES `detalle_puntaje` WRITE;
/*!40000 ALTER TABLE `detalle_puntaje` DISABLE KEYS */;
INSERT INTO `detalle_puntaje` VALUES (1,5,1,1,'OPE-1234567890','Nombre de prueba ','Mensaje para probar la nueva base de datos',NULL,NULL,NULL,'2018-10-02',2,'2018-10-04'),(2,5,1,1,NULL,NULL,NULL,1,'https://www.youtube.com/','enlace de prueba ','2018-10-04',2,'2018-10-04'),(3,5,2,1,NULL,NULL,NULL,1,'https://www.youtube.com/','enlace de prueba 2','2018-10-04',2,'2018-10-04');
/*!40000 ALTER TABLE `detalle_puntaje` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_quiz`
--

DROP TABLE IF EXISTS `detalle_quiz`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_quiz` (
  `id_quiz` int(11) NOT NULL,
  `pregunta` varchar(150) NOT NULL,
  `rpta1` varchar(75) NOT NULL,
  `rpta2` varchar(75) NOT NULL,
  `rpta3` varchar(75) NOT NULL,
  `rpta4` varchar(75) NOT NULL,
  `rpta5` varchar(75) NOT NULL,
  `flg_correcta` int(11) NOT NULL DEFAULT '0' COMMENT '1: CORRECTA',
  `fecha_ins` date NOT NULL,
  `id_user` int(11) NOT NULL,
  `fecha_updt` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_quiz`
--

LOCK TABLES `detalle_quiz` WRITE;
/*!40000 ALTER TABLE `detalle_quiz` DISABLE KEYS */;
/*!40000 ALTER TABLE `detalle_quiz` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empresa`
--

DROP TABLE IF EXISTS `empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empresa` (
  `id_empresa` int(11) NOT NULL AUTO_INCREMENT,
  `organizacion` varchar(75) NOT NULL,
  `email` varchar(75) NOT NULL,
  `pais` varchar(75) NOT NULL,
  `ciudad` varchar(75) NOT NULL,
  `sede` varchar(75) NOT NULL,
  `fecha_regis` date NOT NULL,
  PRIMARY KEY (`id_empresa`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresa`
--

LOCK TABLES `empresa` WRITE;
/*!40000 ALTER TABLE `empresa` DISABLE KEYS */;
INSERT INTO `empresa` VALUES (1,'test','test@test.com','test','test','test','2018-09-28'),(2,'BrainBlue','pyturrizaga@brainblue.com','Perú','Lima','Surco','2018-09-28');
/*!40000 ALTER TABLE `empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `publicaciones`
--

DROP TABLE IF EXISTS `publicaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publicaciones` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Titulo` varchar(255) DEFAULT NULL,
  `Texto` varchar(255) DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  `fecha_publi` date DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `publicaciones`
--

LOCK TABLES `publicaciones` WRITE;
/*!40000 ALTER TABLE `publicaciones` DISABLE KEYS */;
INSERT INTO `publicaciones` VALUES (1,'Hola mundo!','asasdads',1,'2018-09-27',3),(2,'Hola a todos!','fsfsdfsd',1,'2018-09-27',3);
/*!40000 ALTER TABLE `publicaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `puntaje_acumulado`
--

DROP TABLE IF EXISTS `puntaje_acumulado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `puntaje_acumulado` (
  `id_acumula` int(11) NOT NULL AUTO_INCREMENT,
  `id_detalle_puntaje` int(11) NOT NULL,
  `id_puntaje` int(11) NOT NULL,
  `comentario` varchar(150) NOT NULL,
  `fecha_ins` date NOT NULL,
  PRIMARY KEY (`id_acumula`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `puntaje_acumulado`
--

LOCK TABLES `puntaje_acumulado` WRITE;
/*!40000 ALTER TABLE `puntaje_acumulado` DISABLE KEYS */;
INSERT INTO `puntaje_acumulado` VALUES (1,1,1,'comentario de prueba para pasar a victoria ','2018-10-02'),(2,1,3,'comentario de prueba ','2018-10-04'),(3,2,6,'comentario de prueba del enlace ','2018-10-04'),(4,3,0,'medalla rechazada ','2018-10-04'),(5,3,0,'medalla rechazada ','2018-10-04');
/*!40000 ALTER TABLE `puntaje_acumulado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quiz`
--

DROP TABLE IF EXISTS `quiz`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quiz` (
  `id_quiz` int(11) NOT NULL AUTO_INCREMENT,
  `titulo_quiz` varchar(150) NOT NULL,
  `estado` int(11) NOT NULL COMMENT '0: BORRADOR; 1: PUBLICADO',
  `fecha_ins` date NOT NULL,
  `id_user` int(11) NOT NULL,
  `fecha_updt` date NOT NULL,
  PRIMARY KEY (`id_quiz`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quiz`
--

LOCK TABLES `quiz` WRITE;
/*!40000 ALTER TABLE `quiz` DISABLE KEYS */;
/*!40000 ALTER TABLE `quiz` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_medalla`
--

DROP TABLE IF EXISTS `tipo_medalla`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_medalla` (
  `id_tipo_medalla` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_medalla` varchar(50) NOT NULL,
  PRIMARY KEY (`id_tipo_medalla`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_medalla`
--

LOCK TABLES `tipo_medalla` WRITE;
/*!40000 ALTER TABLE `tipo_medalla` DISABLE KEYS */;
INSERT INTO `tipo_medalla` VALUES (1,'Video de Youtube'),(2,'Articulo Blog'),(3,'Publicación en Comunidad Airhead'),(4,'Publicación en Redes Sociales');
/*!40000 ALTER TABLE `tipo_medalla` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_solicitud`
--

DROP TABLE IF EXISTS `tipo_solicitud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_solicitud` (
  `id_tipo_solicitud` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_solicitud` varchar(75) NOT NULL,
  PRIMARY KEY (`id_tipo_solicitud`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_solicitud`
--

LOCK TABLES `tipo_solicitud` WRITE;
/*!40000 ALTER TABLE `tipo_solicitud` DISABLE KEYS */;
INSERT INTO `tipo_solicitud` VALUES (1,'Batallas'),(2,'Victorias'),(3,'Medallas'),(4,'Quiz');
/*!40000 ALTER TABLE `tipo_solicitud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(75) NOT NULL,
  `nombre` varchar(75) NOT NULL,
  `apellido` varchar(75) NOT NULL,
  `pass` varchar(75) NOT NULL,
  `rep_pass` varchar(75) NOT NULL,
  `email` varchar(75) NOT NULL,
  `active` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `terminos` int(11) NOT NULL,
  `fecha_ins` date NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'pyturrizaga','Pedro','Yturrizaga','123','123','pyturrizaga@brainblue.com',1,1,1,'2018-10-01'),(2,'test','test','test','test','test','test@test.com',1,0,1,'2018-10-01');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `valor_puntaje`
--

DROP TABLE IF EXISTS `valor_puntaje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `valor_puntaje` (
  `id_puntaje` int(11) NOT NULL AUTO_INCREMENT,
  `cantidad_puntaje` int(11) NOT NULL,
  PRIMARY KEY (`id_puntaje`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `valor_puntaje`
--

LOCK TABLES `valor_puntaje` WRITE;
/*!40000 ALTER TABLE `valor_puntaje` DISABLE KEYS */;
INSERT INTO `valor_puntaje` VALUES (1,50),(2,200),(3,250),(4,300),(5,25),(6,50),(7,75);
/*!40000 ALTER TABLE `valor_puntaje` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `hp_inc`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `hp_inc` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `hp_inc`;

--
-- Table structure for table `champion`
--

DROP TABLE IF EXISTS `champion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `champion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_completo` varchar(75) NOT NULL,
  `correo` varchar(75) NOT NULL,
  `password` varchar(50) NOT NULL,
  `cumpleanos` date NOT NULL,
  `pais` varchar(25) NOT NULL,
  `empresa` varchar(75) NOT NULL,
  `cargo` varchar(75) NOT NULL,
  `celular` varchar(15) NOT NULL,
  `PBM` varchar(120) NOT NULL,
  `ingreso_empresa` date NOT NULL,
  `sexo` varchar(15) NOT NULL,
  `foto` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=223 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `champion`
--

LOCK TABLES `champion` WRITE;
/*!40000 ALTER TABLE `champion` DISABLE KEYS */;
INSERT INTO `champion` VALUES (1,'Clarissa Esther Alberto Fuentes','calberto@gbs-ec.com','','2018-12-10','El Salvador','GBS','Asesor Comercial Hp SMB El Salvador','','rene.quesada@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(2,'Erick Adalid Canales Palma','ecanales@gbs-ec.com','','1986-11-13','Honduras','Global Business Solutions','Asesor comercial','','rene.quesada@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(3,'Julio Rene Quevedo Alvarado','julioquevedo.a@hotmail.com','','2018-06-20','Guatemala','GBS Centroamerica S.A.','Encargado de Proyecto','','rene.quesada@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(4,'Eduardo Ictech','eduardo@acosa.com.hn','','1966-08-28','Honduras','ACOSA','Gerente General','','dennys.valerio@hp.com; rene.quesada@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(5,'Fredy Saul Andrade Castellon','fredy@compusersa.com','','1973-07-03','Guatemala','COMPUSERSA','GERENTE GENERAL','','andrea.solano-irola@hp.com; rene.quesada@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(6,'Berly Maria Canek Estrada','berlymaria@dataflexsa.com','','1985-09-10','Guatemala','Dataflex, S.A.','GERENTE ADMINISTRATIVA','','andrea.solano-irola@hp.com; rene.quesada@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(7,'Marlon Adalberto Campos Bernabe','marlon.campos@intcomex.com','','1991-01-10','El Salvador','Intcomex','Product Manager','','rene.quesada@hp.com','0000-00-00','',''),(8,'Hector Rene Muralles Cute','hectormuralles@dataflexsa.com','','1983-05-01','Guatemala','Dataflex S.A','Gerente Comercial','','andrea.solano-irola@hp.com; rene.quesada@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(9,'Silvia Contreras','silvia.contreras@akroscorp.com','','1979-11-05','Ecuador','Akros','Gerente de Soluciones Hardware Volumen','','natalia-milena.florez.hernandez@hp.com;  estefania.yepes.valderrama@hp.com','0000-00-00','',''),(10,'Mercedes Samaniego','msamaniego@repycom.com.ec','','1972-08-08','Ecuador','REPYCOM CIA. LTDA.','JEFE DE MARCA','','natalia-milena.florez.hernandez@hp.com;  estefania.yepes.valderrama@hp.com','0000-00-00','',''),(11,'Pedro Trajano Velez Argudo','pedro.velez@intcomex.com','','1971-07-08','Ecuador','INTCOMEX DEL ECUADOR','GERENTE DE PRODUCTO HP CONSUMO COMERCIAL COMPUTO','','ferruccio.fontanot@hp.com','0000-00-00','',''),(12,'Olga Yandun España','oyandun@inacorpsa.con','','1966-03-05','Ecuador','INACORP','Gerente de Producto HP Impresi?n y Suministros','','jorge.villacis@hp.com','0000-00-00','',''),(13,'Paola Cruz','mcruz@compuequip.com','','1980-07-26','Ecuador','Compuequip DOS','Compras','','natalia-milena.florez.hernandez@hp.com;  estefania.yepes.valderrama@hp.com','0000-00-00','',''),(14,'Carlos Andrés Silva Donoso','carlos@gensystems.net','','1976-11-30','Ecuador','GENSYSTEMS S.A.','Ejecutivo de Cuentas Corporativas','','natalia-milena.florez.hernandez@hp.com;  estefania.yepes.valderrama@hp.com','0000-00-00','',''),(15,'Liliana Herrera','liliana.herrera@akroscorp.com','','1975-09-19','Ecuador','Akros Cia. Ltda.','Preventa, Gerente de producto c?mputo','','natalia-milena.florez.hernandez@hp.com;  estefania.yepes.valderrama@hp.com','0000-00-00','',''),(16,'Mariuxi Macias','mariuxi.macias@megamicro-ec.com','','1976-02-24','Ecuador','Megamicro','Jefe Producto HP CORPORATIVO E IMPRESION','','jorge.villacis@hp.com','0000-00-00','',''),(17,'Jenny Patricia Oliva Guerra','jennyoliva@cancomputers.hn','','1980-05-15','Honduras','Can Computers S de R.L.','Compras Internacionales','','dennys.valerio@hp.com; rene.quesada@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(18,'Henry Rafael Javier Matos','henry@gensystems.net','','2018-07-08','Ecuador','GENSYSTEMS S.A.','EJECUTIVO DE CUENTAS CORPORATIVAS','','natalia-milena.florez.hernandez@hp.com;  estefania.yepes.valderrama@hp.com','0000-00-00','',''),(19,'Marco Tulio Hernandez Galdamez','mhernandez.gt@ayp.com.mx','','1985-06-18','Guatemala','AYP','Programa Winback Guatemala','','rene.quesada@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(20,'Edison Armando Gordillo Correa','egordillo@dos.com.ec','','2018-08-29','Ecuador','COMPUEQUIP DOS','GERENTE REGIONAL DE VENTAS','','natalia-milena.florez.hernandez@hp.com;  estefania.yepes.valderrama@hp.com','0000-00-00','',''),(21,'Fernanda Pazmiño','mfpazmino@megacore.com.ec','','1976-03-14','Ecuador','MEGACORE','VENTAS','','natalia-milena.florez.hernandez@hp.com;  estefania.yepes.valderrama@hp.com','0000-00-00','',''),(22,'Pablo Andres Shuguli Caicedo','pablo.shuguli@akroscorp.com','','1994-09-10','Ecuador','Akros','Preventa Jr. Hardware Volumen','','natalia-milena.florez.hernandez@hp.com;  estefania.yepes.valderrama@hp.com; rene.vela@hp.com','0000-00-00','',''),(23,'Jenny Delgado','jdelgado@compufacil.com.ec','','1982-06-15','Ecuador','Compufacil Cia Ltda','Ventas Corporativas','','natalia-milena.florez.hernandez@hp.com;  estefania.yepes.valderrama@hp.com','0000-00-00','',''),(24,'Victor Hugo Guerrero','victor@megacore.com.ec','','1964-07-20','Ecuador','Megacore','Gerente General','','natalia-milena.florez.hernandez@hp.com;  estefania.yepes.valderrama@hp.com','0000-00-00','',''),(25,'Carlos Gutierrez Navia','gerencia@compulaser.com.ec','','1975-05-08','Ecuador','Compulaser','Ceo','','natalia-milena.florez.hernandez@hp.com;  estefania.yepes.valderrama@hp.com','0000-00-00','',''),(26,'Reinaldo Lopez Delgado','reynaldo@gensystems.net','','1962-08-10','Ecuador','GENSYSTEMS','GERENTE DE VENTAS','','natalia-milena.florez.hernandez@hp.com;  estefania.yepes.valderrama@hp.com','0000-00-00','',''),(27,'Karina Reyes','karina.reyes@cash-business.net','','2018-12-03','Honduras','CASH BUSINESS','COMPRAS','','dennys.valerio@hp.com; rene.quesada@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(28,'Herbert Velarde Herrera','herbert.velarde@tehikos.com','','1975-05-14','Ecuador','TEHIKOS C.A.','Gerente Comercial','','natalia-milena.florez.hernandez@hp.com;  estefania.yepes.valderrama@hp.com','0000-00-00','',''),(29,'Michelle Mazzeo','michelle.mazzeo@compucad-ec.com','','1992-11-22','Ecuador','CompuCAD Goods Comercial','Ventas consultivas','','natalia-milena.florez.hernandez@hp.com;  estefania.yepes.valderrama@hp.com','0000-00-00','',''),(30,'Marcos Gavela','marcos.gavela@compucad-ec.com','','1994-01-21','Ecuador','CompuCAD Goods Comercial','Ingeniero de soluciones','','natalia-milena.florez.hernandez@hp.com;  estefania.yepes.valderrama@hp.com','0000-00-00','',''),(31,'Angel David Condoy','angel@gensystems.net','','2018-01-29','Ecuador','GENSYSTEMS S.A.','GERENTE GENERAL','','natalia-milena.florez.hernandez@hp.com;  estefania.yepes.valderrama@hp.com','0000-00-00','',''),(32,'Lorna Virginia Gallardo Rodríguez','lgallardo@sistemasyservicios.com.ec','','1964-08-28','Ecuador','Sistemas y Servicios Erazo C.A.','Asesor Corporativo','','natalia-milena.florez.hernandez@hp.com;  estefania.yepes.valderrama@hp.com','0000-00-00','',''),(33,'Pedro Murillo','murillo@acosa.com.hn','','1981-05-08','Honduras','ACOSA','Gerente de Compras','','dennys.valerio@hp.com; rene.quesada@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(34,'Carlos Castro E.','ecastro@utimpor.com','','2018-12-09','Ecuador','Utimpor S.A','Gerente','','natalia-milena.florez.hernandez@hp.com;  estefania.yepes.valderrama@hp.com','0000-00-00','',''),(35,'Martha Vanessa Pesantez Drouet','martha.pesantez@brodacorp.com','','1978-08-03','Ecuador','BRODACORPSA','JEFE DE VENTAS','','natalia-milena.florez.hernandez@hp.com;  estefania.yepes.valderrama@hp.com','0000-00-00','',''),(36,'Mery del Consuelo Morales','mmorales@compuequip.com','','1972-10-08','Ecuador','Compuequip DOS','Asistente de MKT','','natalia-milena.florez.hernandez@hp.com;  estefania.yepes.valderrama@hp.com','0000-00-00','',''),(37,'Humberto Joselito Castro Flores','josecastro@techcomputer.com.ec','','1966-10-23','Ecuador','TECH COMPUTER','GERENTE GENERAL','','natalia-milena.florez.hernandez@hp.com;  estefania.yepes.valderrama@hp.com','0000-00-00','',''),(38,'Robertha Paola Villaquiran Sanchez','rvillaquiran@premiumtech.com.ec','','1984-12-29','Ecuador','Premiumtech s.a.','Ejecutiva de ventas corporativas','','natalia-milena.florez.hernandez@hp.com;  estefania.yepes.valderrama@hp.com','0000-00-00','',''),(39,'Rafael Orozco','rorozco@gbs-ec.com','','1981-08-15','Guatemala','GBS','Asesor Comercial SMB','','rene.quesada@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(40,'Luis Alberto Villela Reyes','luisvillela82@gmail.com','','1982-11-01','Honduras','ACOSA','Gerente de Ventas Corporativas','','dennys.valerio@hp.com; rene.quesada@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(41,'Manuel Alejandro Gonzales','mgonzales@equiposysistemas.hn','','1991-11-17','Honduras','EQUIPOS Y SISTEMAS S DE RL','GERENTE ADMINISTRATIVO','','dennys.valerio@hp.com; rene.quesada@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(42,'Arturo de la Guardia','afdelaguardia@gmail.com','','1984-05-08','Panamá','CDP Digital','Gerente General','','rodolfo.gonzalez1@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(43,'Melvin Cardoza','mcardoza@cymsistemas.net','','1987-05-13','El Salvador','C&M Sistemas','Gerente Comercial','','dennys.valerio@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(44,'Emilio Laínez','elainez@sistemascc.com','','1977-08-13','Honduras','Sistemas C & C S.A. de C.V.','Ejecutivo de Ventas','','dennys.valerio@hp.com; rene.quesada@hp.com; joseph.zuniga@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(45,'Vanessa Castañao','vcastano@compulab.com.pa','','1994-05-29','Panamá','Compulab','Ventas','','dennys.valerio@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(46,'Vanessa Henríquez','venriquez@gbs-ec.com','','1989-06-08','El Salvador','Global Business Solutions','Sales Specialty','','rene.quesada@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(47,'Ubelda Alvarez Guido','ualvarez@tecnasa.com','','1978-02-26','Nicaragua','TECNASA NICARAGUA S.A.','Especialista de Soluciones','','joseph.zuniga@hp.com; andrea.solano-irola@hp.com; laura.gomez.madrigal@hp.com; jeffrey.ruiz@hp.com','0000-00-00','',''),(48,'Gabriel Echevers','gechevers@suplidoragt.com','','1969-03-17','Panamá','SUPLIDORA GENERAL TECHNOLOGY S.A.','GERENTE','','dennys.valerio@hp.com; laura.gomez.madrigal@hp.com; jeffrey.ruiz@hp.com','0000-00-00','',''),(49,'Cindy Garcia','tecnologia@utilesdehonduras.com','','1987-09-11','Honduras','Utiles de Honduras','Oficial de Compra Tecnologia','','dennys.valerio@hp.com; rene.quesada@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(50,'Lilian Dariana Meza Ortega','teleoperadorategu5@gmail.com','','1992-01-22','Honduras','ACOSA','Telemarketing Corporativo','','dennys.valerio@hp.com; rene.quesada@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(51,'Isabella Gronchi','igronchi@dspma.com','','1993-03-18','Panamá','Dataserve, S.A.','Gerente de Desarrollo Comercial','','dennys.valerio@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(52,'Fernando Escoto','fescoto@asit.com.sv','','1983-02-15','El Salvador','ASIT SA DE CV','GERENTE DE VENTAS','','dennys.valerio@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(53,'Romy Aued','rkaued@tecnasa.com','','1976-07-03','Panamá','TECNASA','Gerente Regional de Alianzas','','dennys.valerio@hp.com; laura.gomez.madrigal@hp.com; rodolfo.gonzalez1@hp.com; santiago.chan@hp.com','0000-00-00','',''),(54,'Luis Rendon','lrendon@utilesdehonduras.com','','2018-06-06','Honduras','Utiles de Honduras','Sub-Gerente','','dennys.valerio@hp.com; rene.quesada@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(55,'Jorge Nuñez','jorge.nunez@yoytec.com','','2018-02-26','Panamá','Yoytec Computer','Compras','','dennys.valerio@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(56,'Jessica Prato Urdaneta','jprato@compulab.com.pa','','1988-09-07','Panamá','COMPULAB,S.A.','Ejecutiva de Venta','','dennys.valerio@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(57,'Silvia Elizabeth Piñeiros Pazmiño ','silvia.pineiros@gensystems.net','','1973-03-27','Ecuador','GENSYSTEMS','VENTAS','','natalia-milena.florez.hernandez@hp.com;  estefania.yepes.valderrama@hp.com','0000-00-00','',''),(58,'S.A.','lgonzalez@dspma.com','','1981-04-07','Panamá','Dataserve s.a','Gerente de ventas','','dennys.valerio@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(59,'Raquel Seferlis','raquel@compulab.com.pa','','1986-02-24','Panamá','Compulab','Gerente Ventas','','dennys.valerio@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(60,'Stephania Carolina Ron Sanchez','sron@compulab.com.pa','','1991-01-30','Panamá','Compulab, S. A.','Ejecutiva de ventas','','dennys.valerio@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(61,'César Escorcia Alvarado','cescorcia@pandataservices.com','','1963-11-20','Panamá','Pandata Services','Gerente General','','dennys.valerio@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(62,'Ricardo C. Amador','ramador@compulab.com.pa','','1972-12-30','Panamá','Compulab','Ejecutivo de ventas','','dennys.valerio@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(63,'Margarita Menjivar','margarita.menjivar@ids-ac.com','','2018-02-05','El Salvador','IDS de centroamerica','Ventas','','dennys.valerio@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(64,'Ana Ruth Pacheco','apacheco@tecnasa.com','','2018-03-26','El Salvador','Tecnasa ES SA de CV','Champion HPI','','dennys.valerio@hp.com; joseph.zuniga@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(65,'Hercilio Camacho','hercilio.camacho@supricom.com','','1982-01-15','Panamá','supricom, s.a.','Gerente','','dennys.valerio@hp.com; laura.gomez.madrigal@hp.com; jeffrey.ruiz@hp.com','0000-00-00','',''),(66,'Jose Luis Henriquez','jhenriquez@grupodpg.com','','1970-04-26','El Salvador','DPG','Gerente de Soluciones','','dennys.valerio@hp.com; rene.quesada@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(67,'Carlos Antonio Cañaate','ccanate@unisistemas.com.pa','','1962-05-23','Panamá','Computecnica de Panamá','Gerente de Ingenieria','','jeffrey.ruiz@hp.com; luispedro.flores@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(68,'José Mauricio Medrano Amaya','jose.medrano@grouppbs.com','','1987-08-28','El Salvador','PBS El Salvador','Analista/ Especialista Computo','','dennys.valerio@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(69,'Gissell Sthephany Zelaya Ordoñez','teleoperadorateg8@acosa.com.hn','','2018-09-26','Honduras','ACOSA','Telemarketing','','dennys.valerio@hp.com; rene.quesada@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(70,'Carlos Vitola','carlos.vitola@yoytec.com','','0000-00-00','Panamá','Yoytec Computer, S.A.','Gerente de Compras','','dennys.valerio@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(71,'Carelys Echeverry','cecheverry@orbe.com.pa','','2018-08-30','Panamá','Componentes El Orbe','Gerente de Cuentas','','dennys.valerio@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(72,'Angel Eduardo Vindel Acosta','angel.vindel@grouppbs.com','','2018-07-09','Honduras','PBS','Brand Manager','','dennys.valerio@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(73,'Juan Carlos Fait','jcfait@sidcr.com','','2018-04-21','Costa Rica','SID DE COSTA RICA SA','GERENTE GENERAL','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(74,'Sergio Estuardo Cardona O\'meany','sergio@ofimat.gt','','1976-10-09','Guatemala','OFIMAT','Gerente Comercial','','rodolfo.gonzalez1@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(75,'Gennie Paredes','gparedes@utilesdehonduras.com','','1978-06-30','Honduras','UTILES DE HONDURAS S.A. DE C.V.','JEFE DE COMPRAS','','dennys.valerio@hp.com; rene.quesada@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(76,'German Osorto','german.osorto@gruporaf.com','','1979-11-15','Honduras','RAF HONDURAS','ASESOR DE VENTAS','','dennys.valerio@hp.com; rene.quesada@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(77,'Manuel Sanic','msanic@canella.com.gt','','1983-05-29','Guatemala','Canella, S.A.','Preventa','','andrea.solano-irola@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(78,'Larry Larrinaga','llarrinaga@tecnasa.com','','1972-05-26','Panamá','TECNASA','Gerente de Servicios Administrados','','dennys.valerio@hp.com; laura.gomez.madrigal@hp.com; rodolfo.gonzalez1@hp.com; ','0000-00-00','',''),(79,'Luis Rendón','lrendon@utilesdehonduras.com','','1980-06-06','Honduras','Utiles de Honduras S.A de C.V.','Sub Gerente General','','dennys.valerio@hp.com; rene.quesada@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(80,'Jorge Arturo Rivera Argeñal','jorge.rivera@gruporaf.com','','1966-07-14','Honduras','GRUPO RAF DE HONDURAS','PM','','dennys.valerio@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(81,'Sayda Umaña','sumana@utilesdehonduras.com','','1974-08-27','Honduras','Utiles de Honduras S.A de C.V.','Gerente de Mercadeo','','dennys.valerio@hp.com; rene.quesada@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(82,'Nahum García','ngarcia@tecnasa.com','','1974-04-07','Guatemala','TECNASA Guatemala S.A.','Gerente Comercial','','rodolfo.gonzalez1@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(83,'Karen Sandoval','ksandoval@utilesdehonduras.com','','1972-04-17','Honduras','Utiles de Honduras S.A. de C.V.','Asistente de Jefe de Compras','','dennys.valerio@hp.com; rene.quesada@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(84,'Jesus Venencia','jesus.venencia@multitek.com.pa','','1971-02-23','Panamá','Multitek','Sales Director','','dennys.valerio@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(85,'Jeannette Marín Cantarero','jmarin@orbe.co.cr','','1964-11-22','Costa Rica','El Orbe.S.A','Directora Comercial','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com; santiago.chan@hp.com; rodolfo.gonzalez1@h','0000-00-00','',''),(86,'Carlos Roberto Rivera','carlos.rivera@gruporaf.com','','1972-06-20','Honduras','Grupo RAF','Gerente General','','dennys.valerio@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(87,'Jorge Salazar','jorge.salazar@digitalchangepty.com','','2018-12-31','Panamá','Digital Transformation','Gerente','','dennys.valerio@hp.com; laura.gomez.madrigal@hp.com; rodolfo.gonzalez1@hp.com; ','0000-00-00','',''),(88,'Marlene Salazar','msalazar@unisistemas.com.pa','','1972-05-14','Panamá','COMPUTECNICA','CONSULTOR EJECUTIVO','','dennys.valerio@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(89,'Fanny Gonzalez','f.gonzalez@yellow-technologies.com','','1974-06-28','Honduras','Random Industrial','MKT','','dennys.valerio@hp.com; rene.quesada@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(90,'Monica Villegas','monica.villegas@sonda.com','','1977-09-12','Costa Rica','Sonda','Gerencia MDS Sonda','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(91,'Mirna Cecibel Candanedo Medrano','mcandanedo@unisistemas.com.pa','','1984-02-14','Panamá','Unisistemas Panamá, S.A.','Ejecutiva de Ventas','','dennys.valerio@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(92,'Andres Lopez Aguilar','alopez@stecr.com','','1988-04-07','Costa Rica','STE','GERENTE DE MARCA','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com; rodolfo.gonzalez1@hp.com','0000-00-00','',''),(93,'Giovanny Tamayo Lopez','gtamayo@dinforsysmega.com','','1963-08-01','Ecuador','DINFORSYSMEGA S.A.','Gerente','','natalia-milena.florez.hernandez@hp.com;  estefania.yepes.valderrama@hp.com','0000-00-00','',''),(94,'Mizrachi','gerencia@quickservicepanama.com','','1975-05-12','Panamá','QUICK SERVICE SUPPLIES','Gerente','','dennys.valerio@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(95,'Pamela Nájera','mnajera@tecnasa.com','','1989-09-23','Guatemala','TECNASA','Especialista HP Inc','','andrea.solano-irola@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(96,'Maria de Lourdes Camacho Huerta','malu.camacho@fcs.com.ec','','1961-10-07','Ecuador','F.C.S.','GERENTE DE VENTAS','','natalia-milena.florez.hernandez@hp.com;  estefania.yepes.valderrama@hp.com','0000-00-00','',''),(97,'Gerardo Ramírez Arce','gramirez@stecr.com','','1968-12-05','Costa Rica','Servicios Tecnicos Especializados STE','Gerente','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com; santiago.chan@hp.com; rodolfo.gonzalez1@h','0000-00-00','',''),(98,'Cesar Augusto Nuques Larrea','cnuques@premiumtech.com.ec','','1975-01-19','Ecuador','PREMIUMTECH S.A.','GERENTE GENERAL','','natalia-milena.florez.hernandez@hp.com;  estefania.yepes.valderrama@hp.com','0000-00-00','',''),(99,'Nima Salman Zadeh','nima@superstarscomputers.com','','1978-04-03','Panamá','SUPER STARS COMPUTERS S.A.','Gerente','','dennys.valerio@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(100,'Fredy Ausberto Mestizo Aguilar','fredy.mestizo@gruporaf.com','','1976-02-09','El Salvador','RAF S.A de C.V.','Especialista','','dennys.valerio@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(101,'George Denyer','george.denyer@sonda.com','','1971-11-11','Costa Rica','Sonda','Supervisor de Desarrollo de Soluciones','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(102,'Yulia de Escorcia','yescorcia@pandataservices.com','','2018-05-30','Panamá','Pan Data Services, Inc.','Gerente de Soluciones de Negocios','','dennys.valerio@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(103,'María Cristina Villacís Vega','cristina.villacis@eksperter.com','','1987-09-12','Ecuador','EKSPERTER CÍA. LTDA','Gerente de Cuentas','','natalia-milena.florez.hernandez@hp.com;  estefania.yepes.valderrama@hp.com','0000-00-00','',''),(104,'Marcia Beatríz Marín Flores','marcia.marin@akroscorp.com','','1972-08-06','Ecuador','Akros Cia. Ltda.','Especialista de Producto','','natalia-milena.florez.hernandez@hp.com;  estefania.yepes.valderrama@hp.com; rene.vela@hp.com','0000-00-00','',''),(105,'Edgar Bolivar Altamiranda Snape','edgar.altamiranda@mt2005.net','','2018-12-18','Panamá','MT2005,S.A.','Gerente Comercial','','dennys.valerio@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(106,'Freddy Bajaña Mota','fbajana@siglo21.net','','1979-04-21','Ecuador','Electrónica Siglo XXI','Product Manager','','jorge.villacis@hp.com','0000-00-00','',''),(107,'Catalina Cruz Román','catalina.cruz@intcomex.com','','1981-10-28','Ecuador','INTCOMEX','Gerente de Producto','','jorge.villacis@hp.com','0000-00-00','',''),(108,'Julio C Ortiz','jortiz@ccpma.com','','1969-02-17','Panamá','Computer Connection','Manager','','dennys.valerio@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(109,'Víctor Raúl Jaén B.','vjaen@tecnasa.com','','1974-10-16','Panamá','TECNASA','Especialista de Producto','','dennys.valerio@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(110,'Daniel Gálvez','dgalvez@tecnasa.com','','1980-05-06','Honduras','Tecnasa Honduras','Gerente Comercial','','dennys.valerio@hp.com; rene.quesada@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(111,'Benjamin jose Sancho','bsancho@stbgroup.com.sv','','1978-05-16','El Salvador','STB COMPUTER','Gerente General','','dennys.valerio@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(112,'José Mansilla','jose.mansilla@greentech.com.gt','','1978-11-04','Guatemala','Greentech','Director','','andrea.solano-irola@hp.com; rodolfo.gonzalez1@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(113,'Carlos Paredes','cparedes@pccadla.com','','1969-11-27','Panamá','PCCAD','Gerente General','','dennys.valerio@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(114,'Kellyn Alejandra Rodriguez Rovelo','teleoperadorateg4@acosa.com.hn','','1995-10-17','Honduras','ACOSA','Telemarketing','','dennys.valerio@hp.com; rene.quesada@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(115,'Pablo Villacis','pablo.villacis@eksperter.com','','2018-04-04','Ecuador','Eksperter','Gerente General','','natalia-milena.florez.hernandez@hp.com;  estefania.yepes.valderrama@hp.com; rene.vela@hp.com','0000-00-00','',''),(116,'Javier Estuardo Mejía Martínez','javier.mejia@intcomex.com','','1993-08-02','Guatemala','Intcomex Guatemala','Product Manager HP Impresi?n y Consumibles','','rene.quesada@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(117,'Ana María Pereira de Hernández','ana@greentech.com.gt','','2018-03-04','Guatemala','Representaciones Comerciales F. MANSILLA Y CIA., S.A.','Gerente de Ventas','','rodolfo.gonzalez1@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(118,'Noe Oliva','noeoliva@corporacionoliva.hn','','1960-12-07','Honduras','Compuser S. de R. L. / Can Computer\'s S. de R. L.','Gerente General','','dennys.valerio@hp.com; rene.quesada@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(119,'kelly ramirez','kelly_ramirez@cecomsa.com','','1992-04-07','República Dominicana','cecomsa','analista de marca de impresion','','mayella.marquina@hp.com; gustavo.then-almonte@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(120,'Shyreswood Pujols G.','pujolss@sinergit.com.do','','2018-05-08','República Dominicana','Sinergit, S.A.','Especialista de Producto','','mayella.marquina@hp.com; gustavo.then-almonte@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(121,'Marcos','marcos.taurel@mt2005.net','','1969-02-22','Venezuela','MT 2005','CEO','','ronnie.avendano@hp.com; mayella.marquina@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(122,'Miguel Sanabria','msanabria@datasup.com','','1979-01-09','Puerto Rico','Data Supplies Corp','Technical Consultant','','mayella.marquina@hp.com; ronnie.avendano@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(123,'Angel Santiago','asantiago@datasup.com','','1967-02-22','Puerto Rico','Data Supplies Corp','Account Executive','','mayella.marquina@hp.com; ronnie.avendano@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(124,'Sharbel Fadul','sfadul@unicomputos.com','','1978-05-19','República Dominicana','Universal de Computos','Director de proyectos y licitaciones','','mayella.marquina@hp.com; gustavo.then-almonte@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(125,'Daniel Cruz Santoni','dcruz@datasup.com','','1969-05-29','Puerto Rico','Data Supplies Corp','account executive','','mayella.marquina@hp.com; ronnie.avendano@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(126,'Marlon Urena Salazar','marlon.urena@grupocma.com','','1973-11-17','Costa Rica','Grupo CMA','ESPECIALISTA DE PRODICTO','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(127,'Luis Carlos Alvarado Vargas','luiscaralvardo@hotmail.com','','2018-08-07','Costa Rica','ALFATEC','ejecutivo de ventas cooprporativas','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(128,'Jose Luis Caceros','jose.caceros@sim.com.gt','','1980-08-30','Guatemala','SIM','Gte Comercial','','andrea.solano-irola@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(129,'Juan Pablo Viquez Oviedo','juan.viquez@grupocma.com','','1982-12-20','Costa Rica','Grupo Computación Modular Avanzada, S.A.','Pre-venta','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(130,'Issurin Rebeca Gramajo Rodas','igramajo@tecnasa.com','','0000-00-00','Guatemala','Tecnasa','Consultora de Negocios','','andrea.solano-irola@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(131,'Mardaly Perez','mardaly.perez@alfarec.co.cr','','1975-05-21','Costa Rica','Alfatec','Gerente General','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com; santiago.chan@hp.com; rodolfo.gonzalez1@h','0000-00-00','',''),(132,'Eveleyn Figueroa','efigueroa@metrica.gt','','1977-07-04','Guatemala','METRICA, S.A.','VENDEDORA','','andrea.solano-irola@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(133,'Francisco Bolaños Molina','fbolanos@tecnasa.com','','1973-10-01','Nicaragua','Tecnasa Nicaragua','Especialista de Soluciones','','joseph.zuniga@hp.com; andrea.solano-irola@hp.com; laura.gomez.madrigal@hp.com; jeffrey.ruiz@hp.com','0000-00-00','',''),(134,'Julio Lam','julio.lam@sisteco.biz','','1959-08-23','Guatemala','SISTECO','Gerente','','andrea.solano-irola@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(135,'Eduardo Quiros B.','equiros@hsgcr.com','','1977-07-24','Costa Rica','Hardware Solutions Group SA','Ventas','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(136,'Daniel Molina','dmolina@unicomputos.com','','1990-10-16','República Dominicana','Universal de Computos','Gerente de Tienda','','mayella.marquina@hp.com; gustavo.then-almonte@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(137,'Alex Andrey Román Miranda','alex.roman@font-tecnologia.com','','1978-12-15','Costa Rica','Corporación Font, S.A.','Gerente de Infraestructura y Convergencia','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(138,'Ingrid Contreras','icontreras@tecnasa.com','','2018-03-29','Guatemala','Tecnasa Guatemala, S.A.','Consultor de Negocios','','andrea.solano-irola@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(139,'Gustavo Barboza','gustavo.barboza@font-tecnologia.com','','1991-06-03','Costa Rica','Corporacion Font','Jefe Operativo','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(140,'Jose Luis Ovalles Sr.','jlovalles@dusa.com.do','','1964-05-01','República Dominicana','Distribuidora Universal','VP','','mayella.marquina@hp.com; gustavo.then-almonte@hp.com; laura.gomez.madrigal@hp.com; rene.vela@hp.com','0000-00-00','',''),(141,'Hugo H. Cabezas Q.','hcabezasq@cdcinternacional.com','','2018-11-03','Costa Rica','CDC Internacional, S.A.','Ejecutivo de Cuenta','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(142,'Luis José Prieto Villegas','luisjose@puntomac.com','','1974-12-12','República Dominicana','PUNTOMAC, S. R. L.','CFO','','mayella.marquina@hp.com; gustavo.then-almonte@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(143,'Carlos Andres Ayala','gmarcas9@acosa.com.hn','','1988-08-21','Honduras','ACOSA','Gte. Categor?a Computo','','dennys.valerio@hp.com; rene.quesada@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(144,'Esmerlin Sanchez','esanchez@digiworldrd.com','','1993-11-22','República Dominicana','DigiWorld','Gerente general','','laura.gomez.madrigal@hp.com','0000-00-00','',''),(145,'Jamieson','wjamieson@datasup.com','','1958-12-05','Estados Unidos','DATA SUPPLIES CORP','PRESIDENTE','','mayella.marquina@hp.com; ronnie.avendano@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(146,'Angel A. Cortes Agosto','tcortes@datasup.com','','1945-01-20','Puerto Rico','Data Supplies Corp','VENTAS','','mayella.marquina@hp.com; ronnie.avendano@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(147,'Antonio Cortes','tcortes@datasup.com','','1945-01-20','Puerto Rico','Data Supplies Corp','Account Executive','','mayella.marquina@hp.com; ronnie.avendano@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(148,'Jose Luis Ovalles Tavarez','jovallesjr@dusa.com.do','','2018-12-04','República Dominicana','Distribuidora Universal S.A.','Gerente de Compras Internacionales','','mayella.marquina@hp.com; gustavo.then-almonte@hp.com; laura.gomez.madrigal@hp.com; rene.vela@hp.com','0000-00-00','',''),(149,'Francis Méndez Badilla','francis.mendez@sistemasanaliticos.cr','','1987-03-29','Costa Rica','Sistemas Analiticos S.A.','Ingeniero de Soporte','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(150,'Rolando E. Fonseca L.','rolando.fonseca@datagraphics.com.sv','','2018-10-11','El Salvador','Data & Graphics, S.A. de C.V.','Gerente de Venta','','dennys.valerio@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(151,'Julio A. Rodriguez','jrodz@datasup.com','','1967-08-10','Puerto Rico','Data Supplies Corp','Printing Solution Specialist','','mayella.marquina@hp.com; ronnie.avendano@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(152,'Josue David Rodriguez Torres','josue@walsmartpr.com','','1977-01-23','Puerto Rico','WAL-SMART INC','Presidente','','mayella.marquina@hp.com; ronnie.avendano@hp.com; laura.gomez.madrigal@hp.com; norman.pizarro@hp.com','0000-00-00','',''),(153,'Cristina Laboy Guzman','cristina@datasup.com','','1984-05-22','Puerto Rico','Data Supplies Corp','Compradora','','mayella.marquina@hp.com; ronnie.avendano@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(154,'Eli Gampel','eli.gampel@magnabyte.com','','1968-02-01','Puerto Rico','Magnabyte Puerto Rico','General Manager','','mayella.marquina@hp.com; ronnie.avendano@hp.com; laura.gomez.madrigal@hp.com; norman.pizarro@hp.com','0000-00-00','',''),(155,'Cristina Lucia Yanes Peñafiel','lucia.yanes@megamicro-ec.com','','1984-05-03','Ecuador','Megamicro','Jefe de Producto HP PSG Equipos de Computo','','jorge.villacis@hp.com','0000-00-00','',''),(156,'Gustavo Vilchez','Gvilchez@conzultek.com','','1978-02-22','Costa Rica','Conzultek','ejecutivo de Ventas','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(157,'Andrea Rodriguez Quesada','andrea.rodriguez@dataformas.com','','1988-12-03','Costa Rica','Dataformas','Ejecutiva de Ventas','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(158,'Luis Carlos Mesa','lmesa@conzultek.com','','1969-11-07','Costa Rica','conzultek','Sales Executive','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(159,'Francisco Jose Mejia Cordon','fmejia@orbe.com.gt','','2018-12-27','Guatemala','Componentes El Orbe, S.A.','Asesor','','andrea.solano-irola@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(160,'Viviana Montero','vmontero@conzultek.com','','1995-12-02','Costa Rica','Sistemas de Computación Conzultek','Ejecutivo de cuenta','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(161,'María Isabel Mora Calderón','mmoraca@orbe.co.cr','','1974-04-13','Costa Rica','Componentes El Orbe SA','Gerente de Producto, divisi?n Corporativa','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(162,'Jorge Samayoa','jorge.samayoa@sisteco.biz','','1970-06-17','Guatemala','SISTECO','Gerente de Ventas','','andrea.solano-irola@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(163,'Maritza Salas Duran','msalas@conzultek.com','','1968-08-04','Costa Rica','Sistemas de Computacion Conzultek de Centroamerica','Asistente Ventas Senior','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(164,'Sofia Ruiz','sofia.ruiz@tecnoventas.com','','1986-10-05','Costa Rica','BPC COMPAÑIA MAYORISTA','VENTAS','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(165,'Olman Nuñez Mata','olman.nunez@grupo3c.com','','1990-02-20','Costa Rica','Importadora de Tecnologia Global (Cococo)','Jefe proyectos / soporte tecnico','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(166,'Francisco Matarrita Brenes','francisco.matarrita@tecnoventas.net','','1986-01-23','Costa Rica','Compañia Mayorista BPC','Agente Vendedor','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(167,'Maricarmen Solorzano','msolorzano@actualidadtecnica.co.cr','','1972-02-18','Costa Rica','PR Actualidad Tecnica','Gerente de Ventas','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(168,'Roberto José Guzmán Jiménez','robertogj@compubetel.com','','1987-04-30','Costa Rica','Compubetel','Ejecutivo de Ventas','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(169,'Eunice Gabriela Salamanca Madriz','esalamanca@sumin-nic.com','','2018-11-20','Nicaragua','SUMIN','GTE DE VENTAS','','joseph.zuniga@hp.com; andrea.solano-irola@hp.com; laura.gomez.madrigal@hp.com; jeffrey.ruiz@hp.com','0000-00-00','',''),(170,'Jahaira Murillo Navarro','jahkenn0828@hotmail.com','','1988-09-08','Costa Rica','Intcomex Costa Rica','Product Manager Jr','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com; santiago.chan@hp.com','0000-00-00','',''),(171,'Yorleny Jimenez M','yjimenez@conzultek.com','','1982-08-07','Costa Rica','Sistemas de Computacion Conzultek','Ejecutiva de ventas Senior','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(172,'Gabriel Torres','gtorres@grupo3c.com','','1979-10-04','Costa Rica','COCOCO','Gerente Comercial','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(173,'Marlon Urena Salazar','marlon.urena@grupocma.com','','1973-11-17','Costa Rica','GRUPO CMA','PREVENTA','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(174,'Jeffrey Bonilla Alvarado','jeffrey.bonilla@dataformas.com','','0000-00-00','Costa Rica','Dataformas','Especialista Impresi?n HP','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(175,'Jessica Patricia Alemán Ortiz','jaleman@tecnasa.com','','1984-12-29','Nicaragua','TECNASA Nicaragua, S.A.','Especialista de soluciones','','joseph.zuniga@hp.com; andrea.solano-irola@hp.com; laura.gomez.madrigal@hp.com; jeffrey.ruiz@hp.com','0000-00-00','',''),(176,'Luis Roberto Bastos Hernández','luis.bastos@intcomex.com','','1990-11-02','Costa Rica','Intcomex','Agente de Ventas','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(177,'Marco Antonio Perez Salguera','mperez@conzultek.com','','1970-12-02','Costa Rica','Sistemas de Computacion Conzultek','Gerente de Ventas','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(178,'Marlon Arias Chaves','marlon.arias@intcomex.com','','1994-03-13','Costa Rica','Intcomex','Vendedor','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(179,'Yorleni Arroyo Hernandez','yarroyo@conzultek.com','','1982-01-15','Costa Rica','Sistemas de Computacion Conzultek de Centroamerica SA','Asistente Ventas / Compras','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(180,'Hector Andres Perez Soto','andres.perez@intcomex.com','','1981-05-29','Costa Rica','Intcomex','vendedor','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(181,'Rafael Moscatelli Castro','rafael.moscatelli@intcomex.com','','1983-10-26','Costa Rica','Intcomex','Ventas','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(182,'adrian jimenez jarquin','adrian.jimenez@dataformas.com','','2018-01-10','Costa Rica','Dataformas','gerente comercial','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(183,'Rocío González Rodríguez','rociogonzalez@intcomex.com','','1978-07-14','Costa Rica','Intcomex','Gerente de Ventas','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(184,'Humberto Matta','hmatta@centrodesoluciones.net','','1987-05-08','Guatemala','Centro de Soluciones, S.A.','Compras','','andrea.solano-irola@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(185,'Yorleny Jimenez Mendez','yjimenez@conzultek.com','','2018-08-11','Costa Rica','Conzultek','Ejecutiva de Cuenta Senior','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(186,'Gladys Santiesteban Santana','gsantiesteban@actualidadtecnica.co.cr','','1971-06-12','Costa Rica','PR Actualidad Tecnica','Ejecutiva de Cuenta','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(187,'Alejandra Castillo Loria','acastillo@tecnovasoluciones.com','','1980-12-31','Costa Rica','Tecnova Soluciones','Subgerente Comercial','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(188,'Ana Alvarez','aalvarez@actualidadtecnica.co.cr','','1984-07-13','Costa Rica','PR ACTUALDIAD TECNICA','VENTAS','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(189,'Richard Whitaker','richard.whitaker@intcomex.com','','1993-06-02','Costa Rica','Intcomex','Asesor de Ventas','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(190,'Cindy Fernández Ureña','cindy.fernandez@font-tecnologia.com','','2018-01-15','Costa Rica','Corporación Font S.A','Gerente Comercial','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(191,'Juan Fernando Grazioso Rey Rosa','jfgrazioso@canella.com.gt','','1992-01-07','Guatemala','Canella S.A,','Gerente Comercial','','rodolfo.gonzalez1@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(192,'Fernán Font Carranza','fernan.font@font-tecnologia.com','','2018-11-25','Costa Rica','Corporación Font S.A','Director','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(193,'Gerson Vladimir Garcia Arenales','gerson.garcia@servicomp.com.gt','','1973-01-24','Guatemala','Servicomp de Guatemala, S.A.','Director Comercial Area Privada','','andrea.solano-irola@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(194,'Jenny Lopez Zuñiga','jlopez@conzultek.com','','1981-02-06','Costa Rica','Conzultek','Ejecutiva de cuentas','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(195,'Greivin Obando Aguirre','greivin.obando@font-tecnologia.com','','1991-02-26','Costa Rica','Corporación Font','Ingeniero Preventa','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(196,'Ana Quiñonez','aquinonez@sumin-nic.com','','1993-06-12','Nicaragua','Sumin','Asistente Gerencia','','joseph.zuniga@hp.com; andrea.solano-irola@hp.com; laura.gomez.madrigal@hp.com; jeffrey.ruiz@hp.com','0000-00-00','',''),(197,'Mario Alberto Chinchilla Acuña','mario.chinchilla@grouppbs.com','','1995-04-01','Costa Rica','Productive Business Solutions','Preventa','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(198,'Mayte Donoso','mdonoso@datatech-usa.com','','1974-06-09','Costa Rica','DATATECH','Territory Manager','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(199,'David González','david.gonzalez@intcomex.com','','1993-11-10','Costa Rica','Intcomex','Vendedor','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(200,'Maria Luisa Gómez','maria.gomez@sintegradas.com','','1991-04-17','Guatemala','Sintegradas, S.A.','Asesora Comercial','','andrea.solano-irola@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(201,'Pablo Daniel Recinos Reyes','pablo.recinos@sintegradas.com','','1989-10-10','Guatemala','Sintegradas, S.A.','Asesor Comercial','','andrea.solano-irola@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(202,'Eilyn Navarro Brenes','enavarro@tecnoventas.net','','1986-04-02','Costa Rica','Compañia Mayorista BPC','Account Manager','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(203,'Ana Moraga Gomez','amoraga@tecnoventas.net','','1988-04-05','Costa Rica','Compañía Mayorista BPC S.A','Ejecutiva de cuenta','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(204,'Francisco Acevedo Chavarria.','facevedo@cdcinternacional.com','','1984-03-10','Costa Rica','CDC Internacional S.A.','Gerente de compras','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(205,'Giovanni Morales Vindas','gmorales@cspc.co.cr','','1986-05-18','Costa Rica','PC Central de Servicios','Solution Manager','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(206,'Abigail Mora Matamoros','amora@conzultek.com','','1998-10-02','Costa Rica','Conzultek S.A.','Televentas y Asistente de Mercadeo','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(207,'Raul Veliz','rveliz@canella.com.gt','','1971-03-11','Guatemala','Canella, S.A.','Director Soluciones TI Corporativas','','andrea.solano-irola@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(208,'Hans Molina Castro','hmolina@conzultek.com','','1968-10-14','Costa Rica','Sistemas de computacion Conzultek de Centroamerica','Ventas','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(209,'Alejandro Jimenez','alejandro.jimenez@dataformas.com','','1972-05-22','Costa Rica','DATAFORMAS','PRESIDENTE','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(210,'Zeidy Yorleny Brenes Astúa','yorleny.brenes@intcomex.com','','2018-07-29','Costa Rica','Intcomex CR','Ejecutiva de ventas','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(211,'Alvaro Otoniel Jacobo Flores','alvarocompusersa@hotmail.com','','1970-12-23','Guatemala','Compusersa, S.A','Ventas','','andrea.solano-irola@hp.com; rene.quesada@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(212,'Kim Ozseden','kim@intelaf.com','','1979-03-28','Guatemala','Intelaf, S.A.','Gerente de Compras','','andrea.solano-irola@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(213,'Minor Trejos Masis','minor@copiadorasmtm.com','','2018-02-01','Costa Rica','Servicio y mantenimiento tecnico MTM','Gerente General','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(214,'Madelin Pamela Nájera Hernández','mnajera@tecnasa.com','','1989-09-23','Guatemala','TECNASA','Especialista HP Inc','','andrea.solano-irola@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(215,'José David Avilés','javiles@etech.com.ni','','1984-01-21','Nicaragua','ETECH, S.A.','Gerente de Ventas','','joseph.zuniga@hp.com; andrea.solano-irola@hp.com; laura.gomez.madrigal@hp.com; jeffrey.ruiz@hp.com','0000-00-00','',''),(216,'Ronald Alvarado','ronald.alvarado@grouppbs.com','','1980-10-22','Guatemala','PBS GUATEMALA','Pre Sales Manager','','andrea.solano-irola@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(217,'Jose Nathan Cisneros Morales','jose.cisneros@mastertec.com.ni','','1982-11-04','Nicaragua','Vargas & Cia Ltda','Resp. Importaciones','','joseph.zuniga@hp.com; andrea.solano-irola@hp.com; laura.gomez.madrigal@hp.com; jeffrey.ruiz@hp.com','0000-00-00','',''),(218,'Oscar Padilla Zúniga','opadilla@orbe.com.ni','','2018-09-30','Nicaragua','Corporación El Orbe','Consultor de Negocios Corporativos','','joseph.zuniga@hp.com; andrea.solano-irola@hp.com; laura.gomez.madrigal@hp.com; jeffrey.ruiz@hp.com','0000-00-00','',''),(219,'Bertha Maria Sandoval Zuniga','bertha.sandoval@sintegradas.com','','1973-11-26','Guatemala','Sintegradas','Asesor en Ventas','','andrea.solano-irola@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(220,'Maureen Salazar Rojas','maureen.salazar@grupocma.com','','1984-02-20','Costa Rica','Grupo CMA','Consultira','','andrea.solano-irola@hp.com;  jeffrey.ruiz@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(221,'Gerber Linares','gl@imeqmo.com','','1983-01-17','Guatemala','Importaciones y Equipos Imeqmo, S.A.','Gerente General','','andrea.solano-irola@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','',''),(222,'Francel France','Francel@omega.com.do','','1991-10-22','República Dominicana','OmegaTech','Ejecutivo de Compras','','mayella.marquina@hp.com; gustavo.then-almonte@hp.com; laura.gomez.madrigal@hp.com','0000-00-00','','');
/*!40000 ALTER TABLE `champion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eblast`
--

DROP TABLE IF EXISTS `eblast`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eblast` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `id_tipo_saludo` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eblast`
--

LOCK TABLES `eblast` WRITE;
/*!40000 ALTER TABLE `eblast` DISABLE KEYS */;
INSERT INTO `eblast` VALUES (1,'sesonals1',1),(2,'sesonals2',1),(3,'sesonals3',1),(4,'cumpleanos1',2),(5,'cumpleanos2',2),(6,'cumpleanos3',2),(7,'aniversario1',3),(8,'aniversario2',3),(9,'aniversario3',3),(10,'incentivo1',4),(11,'incentivo2',4),(12,'incentivo3',4),(13,'objetivo1',5),(14,'objetivo2',5),(15,'objetivo3',5),(16,'promociones1',6),(17,'promociones2',6),(18,'promociones3',6),(19,'negocio1',7),(20,'negocio2',7),(21,'negocio3',7);
/*!40000 ALTER TABLE `eblast` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `festividades`
--

DROP TABLE IF EXISTS `festividades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `festividades` (
  `descripcion` varchar(250) NOT NULL,
  `fecha` date NOT NULL,
  `id_pais` int(11) NOT NULL,
  `id_eblast` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `festividades`
--

LOCK TABLES `festividades` WRITE;
/*!40000 ALTER TABLE `festividades` DISABLE KEYS */;
/*!40000 ALTER TABLE `festividades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pais`
--

DROP TABLE IF EXISTS `pais`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pais` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pais`
--

LOCK TABLES `pais` WRITE;
/*!40000 ALTER TABLE `pais` DISABLE KEYS */;
INSERT INTO `pais` VALUES (1,'Costa Rica'),(2,'República Dominicana'),(3,'Ecuador'),(4,'El Salvador'),(5,'Guatemala'),(6,'Honduras'),(7,'Nicaragua'),(8,'Panamá'),(9,'Puerto Rico'),(10,'Estados Unidos'),(11,'Venezuela');
/*!40000 ALTER TABLE `pais` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `saludo_programado`
--

DROP TABLE IF EXISTS `saludo_programado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `saludo_programado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_tipo_saludo` int(11) NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_fin` date NOT NULL,
  `id_user` varchar(1500) NOT NULL,
  `estado` varchar(15) NOT NULL,
  `id_eblast` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `saludo_programado`
--

LOCK TABLES `saludo_programado` WRITE;
/*!40000 ALTER TABLE `saludo_programado` DISABLE KEYS */;
INSERT INTO `saludo_programado` VALUES (1,3,'2018-09-11','2018-09-11','119,136,142,222','PENDIENTE',8),(2,5,'2018-09-11','2018-09-11','119,136,142,222,119,120,124,140','PENDIENTE',13),(3,5,'2018-09-11','2018-09-11','119,124,136,142','PENDIENTE',14);
/*!40000 ALTER TABLE `saludo_programado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_saludo`
--

DROP TABLE IF EXISTS `tipo_saludo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_saludo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_saludo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_saludo`
--

LOCK TABLES `tipo_saludo` WRITE;
/*!40000 ALTER TABLE `tipo_saludo` DISABLE KEYS */;
INSERT INTO `tipo_saludo` VALUES (1,'Sesonals'),(2,'Saludo Cumpleaños'),(3,'Saludo Aniversario'),(4,'Incentivos'),(5,'Saludo Objetivo Alcanzado'),(6,'Promociones'),(7,'Saludo Buen Negocio');
/*!40000 ALTER TABLE `tipo_saludo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `hp_tank`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `hp_tank` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `hp_tank`;

--
-- Table structure for table `anotacion`
--

DROP TABLE IF EXISTS `anotacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anotacion` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date DEFAULT NULL,
  `nro_factura` varchar(100) DEFAULT NULL,
  `modelo` varchar(255) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `spiff` varchar(255) DEFAULT NULL,
  `monto` int(11) DEFAULT NULL,
  `documento` varchar(255) DEFAULT NULL,
  `Id_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=363 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anotacion`
--

LOCK TABLES `anotacion` WRITE;
/*!40000 ALTER TABLE `anotacion` DISABLE KEYS */;
INSERT INTO `anotacion` VALUES (1,'2018-03-01','123','HP Tank 5820',2,'$16',100,'lapiz.pdf',1),(2,'2018-03-20','0','HP Tank 5810',2,'$10',56,'REFERENCE_SLIDE_TGW_New template CRP.pdf',2),(3,'2018-03-02','456','HP Tank 5810',5,'$25',1000,'lapiz.pdf',1),(4,'2018-03-03','123JH256','HP Tank 5820',1,'$8',20,'lapiz.pdf',1),(5,'2018-03-01','*08483','HP Tank 5810',3,'$15',827,'5810-8483.pdf',5),(6,'2018-03-01','*08484','HP Tank 5810',2,'$10',816,'5810-8484.pdf',5),(7,'2018-03-09','*08643','HP Tank 5810',1,'$5',1,'5810-8483.pdf',5),(8,'2018-03-10','*08659','HP Tank 5810',1,'$5',1,'8659-5810.pdf',5),(9,'2018-03-26','*09003','HP Tank 5810',2,'$10',779,'5810-9003.pdf',5),(10,'2018-03-26','*09006','HP Tank 5810',2,'$10',510,'5810-9006.pdf',5),(11,'2018-03-27','*09027','HP Tank 5810',2,'$10',1,'9027-5810.pdf',5),(12,'2018-03-09','8643','HP Tank 5820',1,'$8',1,'8643  5820.pdf',5),(13,'2018-03-09','11167','HP Tank 5820',1,'$8',1,'5820.pdf',5),(14,'2018-03-10','8658','HP Tank 5820',2,'$16',2,'8658 peter.pdf',5),(15,'2018-03-10','8660','HP Tank 5820',1,'$8',1,'8660.pdf',5),(16,'2018-03-10','8661','HP Tank 5820',2,'$16',1,'8661.pdf',5),(17,'2018-03-19','8841','HP Tank 5820',3,'$24',2,'8841.pdf',5),(18,'2018-03-21','8879','HP Tank 5820',2,'$16',2,NULL,5),(19,'2018-03-26','9003','HP Tank 5820',2,'$16',779,'9003.pdf',5),(20,'2018-03-27','9027','HP Tank 5820',2,'$16',1,'9027-5820.pdf',5),(21,'2018-04-03','9125','HP Tank 5820',2,'$16',593,'9125.pdf',5),(22,'2018-04-21','9498','HP Tank 5820',2,'$16',1,'9498.pdf',5),(23,'2018-04-11','9271','HP Tank 5810',2,'$10',1,'9271.pdf',5),(24,'2018-04-26','00012133','HP Tank 5820',2,'$16',601,'Factura.pdf',4),(25,'2018-04-26','0006311','HP Tank 5810',2,'$10',601,'Factura.pdf',4),(26,'2018-03-01','11009','HP Tank 5810',1,'$5',129,'HAF 11009 hp5810.pdf',3),(27,'2018-03-06','11077','HP Tank 5810',2,'$10',258,'Ecocenter 11077 HP 5810 = 2.pdf',3),(28,'2018-03-08','11135','HP Tank 5810',2,'$10',258,'HAF 11135 HP 5810 = 2.pdf',3),(29,'2018-03-17','11344','HP Tank 5810',1,'$5',129,'Tecno Center 11344 hp 5810 = 1.pdf',3),(30,'2018-03-26','11522','HP Tank 5810',2,'$10',230,NULL,3),(31,'2018-03-26','11524','HP Tank 5810',1,'$5',115,'Scl Telecoms 11524 hp 5810 = 1.pdf',3),(32,'2018-03-26','11522','HP Tank 5810',2,'$10',230,'Claudina Rosales Gonzalez ( Multiservicios Elvis David ) 11522 hp 5810 = 2.pdf',3),(33,'2018-04-02','11585','HP Tank 5810',2,'$10',230,'Happy Fashion 11585 hp5810 = 2.pdf',3),(34,'2018-04-04','11645','HP Tank 5810',1,'$5',115,'Electro Motors Divisa hp 5810 = 1.pdf',3),(35,'2018-04-04','11646','HP Tank 5810',1,'$5',115,'Golden Suppliers Hp 5810 = 1.pdf',3),(36,'2018-04-04','11646','HP Tank 5810',1,'$5',115,'Golden Suppliers Hp 5810 = 1.pdf',3),(37,'2018-04-04','11649','HP Tank 5810',2,'$10',230,'Ecocenter 11649 Hp 5810 = 2.pdf',3),(38,'2018-04-09','11727','HP Tank 5810',4,'$20',460,'Audio Maxx 11727 hp 5810 Hp 5810 = 4.pdf',3),(39,'2018-04-10','11754','HP Tank 5810',1,'$5',115,'Claudina Rosales Gonzalez ( Multiservicios Elvis David ) 11754 Hp5810 = 1.pdf',3),(40,'2018-04-13','11831','HP Tank 5810',6,'$30',690,'Super Audio 11831 Hp 5810 = 6.pdf',3),(41,'2018-04-17','11873','HP Tank 5810',1,'$5',119,'Movil Com 11873 Hp5810 =1.pdf',3),(42,'2018-04-18','11915','HP Tank 5810',1,'$5',119,'Universal Top 11915 hp 5810 = 1.pdf',3),(43,'2018-04-18','11920','HP Tank 5810',3,'$15',345,'Ecocenter 11920 hp 5810 = 3.pdf',3),(44,'2018-04-20','11960','HP Tank 5810',1,'$5',115,'Claudina Rosales Gonzalez ( Multiservicios Elvis David ) 11960 hp 5810 = 1.pdf',3),(45,'2018-04-20','11971','HP Tank 5810',1,'$5',115,NULL,3),(46,'2018-04-23','12824','HP Tank 5810',1,'$5',115,'Alex Luo Pcell Store 12824 hp5810 = 1.pdf',3),(47,'2018-03-10','11186','HP Tank 5820',4,'$32',596,'Super Audio 11186 hp 5820 = 4.pdf',3),(48,'2018-03-21','11418','HP Tank 5820',1,'$8',149,'Claudina Rosales Gonzalez ( Multiservicios Elvis David ) 11418 hp 5820 = 1.pdf',3),(49,'2018-03-22','11454','HP Tank 5820',1,'$8',149,'Ciber M@z 11454 Hp 5820 = 1.pdf',3),(50,'2018-04-24','12073','HP Tank 5810',2,'$10',214,NULL,3),(51,'2018-04-24','12073','HP Tank 5810',2,'$10',214,NULL,3),(52,'2018-04-24','12073','HP Tank 5810',2,'$10',214,NULL,3),(53,'1969-12-31','12129','HP Tank 5810',1,'$5',107,NULL,3),(54,'2018-04-26','12130','HP Tank 5810',2,'$10',214,NULL,3),(55,'2018-04-26','12134','HP Tank 5810',2,'$5',214,NULL,3),(56,'2018-04-27','12156','HP Tank 5810',1,'$5',107,NULL,3),(57,'2018-04-28','12167','HP Tank 5810',2,'$10',214,NULL,3),(58,'2018-04-24','9623','HP Tank 5810',2,'$10',429,'IMG-20180430-WA0138.jpg',5),(59,'2018-04-26','9630','HP Tank 5820',2,'$16',1,'IMG-20180430-WA0138.jpg',5),(60,'2018-04-30','12181','HP Tank 5810',1,'$5',107,NULL,3),(61,'2018-04-30','12182','HP Tank 5810',1,'$5',107,NULL,3),(62,'2018-04-30','12184','HP Tank 5810',5,'$25',534,'Grupo Tech Store 12184 hp5810 = 5.pdf',3),(63,'2018-01-06','CA-30-14322','HP Tank 5810',1,'$5',1229,'CA-30-14322.pdf',9),(64,'2018-02-24','CA-30-14519','HP Tank 5820',1,'$8',1499,'CA-30-14519.pdf',9),(65,'2011-02-18','CA-30-14460','HP Tank 5820',1,'$8',1499,'CA-30-14460.pdf',9),(66,'2018-05-03','123','HP Tank 5820',3,'$24',5900,'Header_3.jpg',10),(67,'2018-03-02','180000188782','HP Tank 5810',1,'$5',1245,NULL,11),(68,'2018-03-08','189242','HP Tank 5810',1,'$5',1256,NULL,11),(69,'2018-04-18','180000192075','HP Tank 5810',1,'$5',1224,NULL,11),(70,'2018-04-23','180000192473','HP Tank 5810',1,'$5',1175,NULL,11),(71,'2018-04-12','180000093089','HP Tank 5810',1,'$5',1655,NULL,11),(72,'2018-04-23','180000089831','HP Tank 5810',1,'$5',1224,NULL,11),(73,'2018-03-03','180000208226','HP Tank 5810',1,'$5',1245,NULL,11),(74,'2018-04-21','180000213480','HP Tank 5810',1,'$5',1175,NULL,11),(75,'2018-04-27','180000214043','HP Tank 5810',1,'$5',1224,NULL,11),(76,'2018-04-28','180000214130','HP Tank 5810',1,'$5',1175,NULL,11),(77,'2018-04-11','180000072898','HP Tank 5810',1,'$5',1270,NULL,11),(78,'2018-04-23','180000073357','HP Tank 5810',1,'$5',1138,NULL,11),(79,'2018-03-02','180000020304','HP Tank 5810',1,'$5',1286,NULL,11),(80,'2018-03-06','180000020600','HP Tank 5810',1,'$5',1195,NULL,11),(81,'2018-04-04','180000022259','HP Tank 5810',1,'$5',1195,NULL,11),(82,'2018-04-21','180000023528','HP Tank 5810',1,'$5',1175,NULL,11),(83,'2018-04-27','180000023884','HP Tank 5810',1,'$5',1175,NULL,11),(84,'2018-03-09','180000097311','HP Tank 5810',1,'$5',1256,NULL,11),(85,'2018-04-09','180000098652','HP Tank 5810',1,'$5',1175,NULL,11),(86,'2018-04-25','180000099482','HP Tank 5810',1,'$5',1175,NULL,11),(87,'2018-03-25','180000124457','HP Tank 5810',1,'$5',1245,NULL,11),(88,'2018-04-25','180000126500','HP Tank 5810',1,'$5',1175,NULL,11),(89,'2018-04-17','180000070341','HP Tank 5810',1,'$5',1175,NULL,11),(90,'2018-03-08','180000041320','HP Tank 5810',1,'$5',1195,NULL,11),(91,'2018-04-21','180000042827','HP Tank 5810',2,'$10',2350,NULL,11),(92,'2018-04-20','180000045071','HP Tank 5810',1,'$5',1175,NULL,11),(93,'2018-03-04','180000046553','HP Tank 5810',1,'$5',1340,NULL,11),(94,'2018-03-12','180000046900','HP Tank 5810',1,'$5',1206,NULL,11),(95,'2018-04-08','180000047917','HP Tank 5810',1,'$5',1245,NULL,11),(96,'2018-03-02','180000037402','HP Tank 5810',1,'$5',1468,NULL,11),(97,'2018-03-07','180000037687','HP Tank 5810',1,'$5',1195,NULL,11),(98,'2018-03-10','180000037842','HP Tank 5810',1,'$5',1195,NULL,11),(99,'2018-04-08','180000038991','HP Tank 5810',1,'$5',1245,NULL,11),(100,'2018-04-17','180000039363','HP Tank 5810',1,'$5',1318,NULL,11),(101,'2018-04-19','180000039430','HP Tank 5810',1,'$5',1175,NULL,11),(102,'2018-04-19','180000036245','HP Tank 5810',1,'$5',1274,NULL,11),(103,'2018-04-19','180000036246','HP Tank 5810',1,'$5',1175,NULL,11),(104,'2018-04-15','180000022143','HP Tank 5810',1,'$5',1203,NULL,11),(105,'2018-04-21','180000022417','HP Tank 5810',1,'$5',1515,NULL,11),(106,'2018-04-22','180000010507','HP Tank 5810',1,'$5',1224,NULL,11),(107,'2018-04-24','180000010525','HP Tank 5810',1,'$5',1175,NULL,11),(108,'2018-04-25','180000010561','HP Tank 5810',1,'$5',3643,NULL,11),(109,'2018-04-10','180000011496','HP Tank 5810',1,'$5',1224,NULL,11),(110,'2018-04-16','180000191941','HP Tank 5820',1,'$8',2390,NULL,11),(111,'2018-04-25','180000192645','HP Tank 5820',1,'$8',1950,NULL,11),(112,'2018-03-23','180000092679','HP Tank 5820',1,'$8',1529,NULL,11),(113,'2018-04-28','180000090243','HP Tank 5820',1,'$8',1510,NULL,11),(114,'2018-03-03','180000208230','HP Tank 5820',1,'$8',1796,NULL,11),(115,'2018-03-10','180000209115','HP Tank 5820',1,'$8',1488,NULL,11),(116,'2018-04-16','180000212894','HP Tank 5820',1,'$8',1510,NULL,11),(117,'2018-04-18','180000213052','HP Tank 5820',1,'$8',1510,NULL,11),(118,'2018-03-09','180000020839','HP Tank 5820',1,'$8',1450,NULL,11),(119,'2018-03-09','180000020840','HP Tank 5820',1,'$8',1510,NULL,11),(120,'2018-04-16','180000023119','HP Tank 5820',1,'$8',1506,NULL,11),(121,'2018-03-13','180000097470','HP Tank 5820',1,'$8',3499,NULL,11),(122,'2018-03-19','180000097766','HP Tank 5820',1,'$8',1510,NULL,11),(123,'2018-03-20','180000097850','HP Tank 5820',1,'$8',1861,NULL,11),(124,'2018-04-12','180000098848','HP Tank 5820',1,'$8',1450,NULL,11),(125,'2018-03-08','180000113460','HP Tank 5820',1,'$8',1895,NULL,11),(126,'2018-04-03','180000042192','HP Tank 5820',1,'$8',1450,NULL,11),(127,'2018-04-18','180000067336','HP Tank 5820',1,'$8',1450,NULL,11),(128,'2018-03-16','180000065676','HP Tank 5820',1,'$8',2335,NULL,11),(129,'2018-04-12','180000067097','HP Tank 5820',1,'$8',1450,NULL,11),(130,'2018-04-15','180000048190','HP Tank 5820',1,'$8',1450,NULL,11),(131,'2018-04-16','180000039346','HP Tank 5820',1,'$8',1450,NULL,11),(132,'2018-04-17','180000039379','HP Tank 5820',1,'$8',1581,NULL,11),(133,'2018-04-28','180000039760','HP Tank 5820',1,'$8',1506,NULL,11),(134,'2018-03-07','180000034810','HP Tank 5820',1,'$8',1526,NULL,11),(135,'2018-04-05','180000044805','HP Tank 5820',1,'$8',1450,NULL,11),(136,'2018-04-16','180000045332','HP Tank 5820',1,'$8',1699,NULL,11),(137,'2018-04-22','180000045694','HP Tank 5820',1,'$8',1540,NULL,11),(138,'2018-04-22','180000045695','HP Tank 5820',1,'$8',1450,NULL,11),(139,'2018-03-06','180000020693','HP Tank 5820',1,'$8',1510,NULL,11),(140,'2018-04-27','HPOAKL-5862','HP Tank 5820',1,'$8',0,'Factura de deudores 5862.pdf',14),(141,'2018-05-16','HPOAKL-5971','HP Tank 5820',1,'$8',0,'Factura de deudores 5971.pdf',14),(142,'2018-05-27','CA-27-1-1838','HP Tank 5820',1,'$8',0,'CA-27-1-1838.pdf',15),(143,'2018-05-06','CA-27-1-1745','HP Tank 5820',1,'$8',0,'CA-27-1-1745.pdf',25),(144,'2018-05-02','09722','HP Tank 5810',1,'$5',454,'mega movil joyeria   mayo.pdf',5),(145,'2018-05-04','09769','HP Tank 5820',2,'$16',1488,NULL,5),(146,'2018-05-04','12159','HP Tank 5810',1,'$5',1,NULL,5),(147,'2018-05-07','09824','HP Tank 5810',1,'$5',1,'mobil center  mayo.pdf',5),(148,'2018-05-09','09868','HP Tank 5810',1,'$5',118,'micro computer mayo.pdf',5),(149,'2018-05-10','09906','HP Tank 5810',3,'$15',651,'electro center  mayo.pdf',5),(150,'2018-05-16','10014','HP Tank 5810',1,'$5',1,'lili store  mayo.pdf',5),(151,'2018-05-16','10036','HP Tank 5810',5,'$25',1132,'grupo emma mayo.pdf',5),(152,'2018-05-16','12423','HP Tank 5820',1,'$8',1,NULL,5),(153,'2018-05-29','10317','HP Tank 5820',4,'$32',2367,'tecno movil center mayo.pdf',5),(154,'2018-05-16','10037','HP Tank 5820',1,'$8',2982,'iman poderoso  mayo.pdf',5),(155,'2018-05-16','12424','HP Tank 5810',1,'$5',2982,'iman poderoso  mayo.pdf',5),(156,'2018-05-16','10028','HP Tank 5810',2,'$10',819,'tecnology house   mayo.pdf',5),(157,'2018-05-15','9989','HP Tank 5820',1,'$8',496,'jhony cell computer  mayo.pdf',5),(158,'2018-05-31','10351','HP Tank 5820',4,'$32',556,'iman poderoso 2 mayo.pdf',5),(159,'2018-06-04','64464','HP Tank 5810',2,'$10',100,'2013-II_SistOperativos_P.pdf',2),(160,'2018-05-02','12196','HP Tank 5810',5,'$25',572,'Nexxt Solution 2 de Mayo 12196.pdf',3),(161,'2018-05-05','12387','HP Tank 5810',3,'$15',343,'Econocell 5 de Mayo 12387.pdf',3),(162,'2018-05-11','12425','HP Tank 5810',1,'$5',107,'Internet Egosline 11 de mayo Fact 12425.pdf',3),(163,'2018-05-14','12466','HP Tank 5810',5,'$25',535,'Ecocenter 14 de Mayo Fact 12466.pdf',3),(164,'2018-05-17','12565','HP Tank 5810',3,'$15',321,'Super Audio 17 mayo 12565.pdf',3),(165,'2018-05-18','12576','HP Tank 5810',3,'$15',321,'PcPlanet 18 Mayo Fact 12576.pdf',3),(166,'2018-05-24','12690','HP Tank 5810',1,'$5',107,'Internet Egosline 24 Mayo Fact 12690 .pdf',3),(167,'2018-05-28','12267','HP Tank 5810',3,'$15',321,'Zolution Store 28 Mayo FAct 12267 .pdf',3),(168,'2018-05-28','12273','HP Tank 5810',1,'$5',107,'ZonaTEc 28 de Mayo 12273.pdf',3),(169,'2018-05-28','12279','HP Tank 5810',2,'$10',214,'Electrp Motors D 28 de Mayo Fact 12279.pdf',3),(170,'2018-05-30','12817','HP Tank 5820',1,'$8',130,'JC Computer 30 de Mayo 12817 .pdf',3),(171,'2018-05-03','09745','HP Tank 5810',1,'$5',107,'NuevoDocumento 2018-06-05 (2)_1.pdf',4),(172,'2018-05-16','10011','HP Tank 5810',1,'$5',107,'NuevoDocumento 2018-06-05 (2)_2.pdf',4),(173,'2018-05-17','10042','HP Tank 5810',5,'$25',535,'NuevoDocumento 2018-06-05 (2)_3.pdf',4),(174,'2018-05-25','10250','HP Tank 5810',1,'$5',107,'NuevoDocumento 2018-06-05 (2)_4.pdf',4),(175,'2018-05-09','09884','HP Tank 5820',1,'$8',130,'NuevoDocumento 2018-06-05 (2)_5.pdf',4),(176,'2018-05-16','10011','HP Tank 5820',1,'$8',130,'NuevoDocumento 2018-06-05 (2)_6.pdf',4),(177,'2018-05-25','10250','HP Tank 5810',2,'$10',260,'NuevoDocumento 2018-06-05 (2)_7.pdf',4),(178,'2018-05-26','10263','HP Tank 5820',2,'$16',260,'NuevoDocumento 2018-06-05 (2)_8.pdf',4),(179,'2018-05-28','HPOAKL-6033','HP Tank 5820',1,'$8',1300,'HPOAKL-6033.pdf',35),(180,'2018-05-31','HPOAKL-6055','HP Tank 5820',1,'$8',1300,'HPOAKL-6055.pdf',35),(181,'2018-06-03','HOAKL-19','HP Tank 5820',1,'$8',1300,'HPOAKL-19.pdf',35),(182,'2018-05-13','215645','HP Tank 5820',1,'$8',1526,NULL,11),(183,'2018-05-20','117727','HP Tank 5820',1,'$8',1510,NULL,11),(184,'2018-05-09','45767','HP Tank 5820',1,'$8',1599,NULL,11),(185,'2018-05-28','41057','HP Tank 5820',1,'$8',1450,NULL,11),(186,'2018-05-04','10818','HP Tank 5820',1,'$8',1450,NULL,11),(187,'2018-05-05','24423','HP Tank 5820',1,'$8',1541,NULL,11),(188,'2018-05-02','193068','HP Tank 5810',2,'$10',4230,NULL,11),(189,'2018-05-22','91414','HP Tank 5810',1,'$5',1224,NULL,11),(190,'2018-05-08','68275','HP Tank 5810',1,'$5',1175,NULL,11),(191,'2018-05-22','68910','HP Tank 5810',1,'$5',1175,NULL,11),(192,'2018-05-29','69258','HP Tank 5810',1,'$5',1280,NULL,11),(193,'2018-05-07','43423','HP Tank 5810',1,'$5',1224,NULL,11),(194,'2018-05-20','46173','HP Tank 5810',1,'$5',1457,NULL,11),(195,'2018-05-07','49128','HP Tank 5810',1,'$5',1175,NULL,11),(196,'2018-05-12','37039','HP Tank 5810',1,'$5',1224,NULL,11),(197,'2018-05-09','40291','HP Tank 5810',1,'$5',1224,NULL,11),(198,'2018-05-20','40757','HP Tank 5810',1,'$5',1219,NULL,11),(199,'2018-05-25','40922','HP Tank 5810',1,'$5',1175,NULL,11),(200,'2018-05-30','47657','HP Tank 5810',1,'$5',2305,NULL,11),(201,'2018-05-16','23357','HP Tank 5810',1,'$5',5000,NULL,11),(202,'2018-05-03','10784','HP Tank 5810',1,'$5',1437,NULL,11),(203,'2018-05-15','11056','HP Tank 5810',1,'$5',1449,NULL,11),(204,'2018-05-28','13122','HP Tank 5810',1,'$5',1342,NULL,11),(205,'2018-05-09','24601','HP Tank 5810',1,'$5',1175,NULL,11),(206,'2018-05-09','24655','HP Tank 5810',1,'$5',1214,NULL,11),(207,'2018-05-18','25204','HP Tank 5810',1,'$5',1175,NULL,11),(208,'2018-05-28','25890','HP Tank 5810',1,'$5',1175,NULL,11),(209,'2018-06-08','CA-27-1-1893','HP Tank 5810',1,'$5',1,'CA-27-1-1893.pdf',15),(210,'2018-06-10','CA-27-1-','HP Tank 5820',1,'$8',0,'CA-27-1-1897.pdf',15),(211,'2018-05-16','10016','HP Tank 5820',3,'$24',653,'fed importmayo 2018.pdf',5),(212,'2018-05-06','9769','HP Tank 5820',2,'$16',1488,'decoling mayo nuevo.pdf',5),(213,'2018-05-16','12407','HP Tank 5810',2,'$10',653,'decoling mayo nuevo.pdf',5),(214,'2018-05-04','12267','HP Tank 5810',1,'$5',1488,'decoling mayo nuevo.pdf',5),(215,'2018-06-01','3971','HP Tank 5810',1,'$5',128,NULL,16),(216,'2018-06-02','9067','HP Tank 5810',1,'$5',128,NULL,22),(217,'2018-06-04','3647','HP Tank 5810',1,'$5',128,NULL,22),(218,'2018-06-04','15072','HP Tank 5810',1,'$5',113,NULL,22),(219,'2018-06-06','3669','HP Tank 5810',1,'$5',137,NULL,22),(220,'2018-06-06','15158','HP Tank 5810',1,'$5',121,NULL,22),(221,'2018-06-06','15134','HP Tank 5810',1,'$5',113,NULL,22),(222,'2018-06-08','7350','HP Tank 5810',1,'$5',128,NULL,17),(223,'2018-06-09','9145','HP Tank 5810',1,'$5',128,NULL,17),(224,'2018-06-09','1627','HP Tank 5810',1,'$5',128,NULL,17),(225,'2018-06-11','6158','HP Tank 5810',1,'$5',113,NULL,17),(226,'2018-06-12','15303','HP Tank 5810',2,'$10',226,NULL,17),(227,'2018-03-27','ca-06-27200','HP Tank 5810',2,'$10',2398,'1725_0002.pdf',7),(228,'2018-04-05','2398','HP Tank 5810',2,'$10',2398,'1725_0003.pdf',7),(229,'2018-06-11','ca-09-18397','HP Tank 5820',1,'$8',1475,'1725_0001.pdf',7),(230,'2018-04-17','ca-09-18043','HP Tank 5810',1,'$5',1199,'1726_0001.pdf',7),(231,'2018-06-19','ca-09-18442','HP Tank 5820',1,'$8',1983,'1726_0002.pdf',7),(232,'2018-04-03','000-001-01-00036667','HP Tank 5810',1,'$5',3421,'Crystal Reports - Yellow Tech - Factura.pdf',37),(233,'2018-04-10','000-001-01-00036871','HP Tank 5810',21,'$105',319,'Crystal Reports - Yellow Tech - Factura2.pdf',37),(234,'2018-04-25','000-001-01-00036894','HP Tank 5810',1,'$5',10376,'Crystal Reports - Yellow Tech - Factura3.pdf',37),(235,'2018-04-11','000-001-01-00036950','HP Tank 5810',1,'$5',6594,'Crystal Reports - Yellow Tech - Factura4.pdf',37),(236,'2018-04-11','000-001-01-00036906','HP Tank 5810',5,'$25',17106,'Crystal Reports - Yellow Tech - Factura5.pdf',37),(237,'2018-04-18','000-001-01-00037151','HP Tank 5810',3,'$15',18834,'Crystal Reports - Yellow Tech - Factura6.pdf',37),(238,'2018-05-28','000-001-01-00038446','HP Tank 5810',1,'$5',6,'Crystal Reports - Yellow Tech - Factura7.pdf',37),(239,'2018-06-02','000-001-01-00038681','HP Tank 5810',1,'$5',7,'Crystal Reports - Yellow Tech - Factura8.pdf',37),(240,'2018-06-07','000-001-01-00038865','HP Tank 5810',1,'$5',3,'Crystal Reports - Yellow Tech - Factura9.pdf',37),(241,'2018-06-13','000-001-01-00039043','HP Tank 5810',1,'$5',6,'Crystal Reports - Yellow Tech - Factura10.pdf',37),(242,'2018-06-13','000-001-01-00039028','HP Tank 5810',4,'$20',11,'Crystal Reports - Yellow Tech - Factura11.pdf',37),(243,'2018-06-14','000-001-01-00039062','HP Tank 5810',1,'$5',3,'Crystal Reports - Yellow Tech - Factura12.pdf',37),(244,'2018-04-06','000-001-01-00036823','HP Tank 5820',3,'$24',56,'Crystal Reports - Yellow Tech - Factura.pdf',37),(245,'2018-04-16','000-001-01-00037065','HP Tank 5820',1,'$8',4,'Crystal Reports - Yellow Tech - Factura2.pdf',37),(246,'2018-04-18','000-001-01-00037183','HP Tank 5820',1,'$8',4,'Crystal Reports - Yellow Tech - Factura3.pdf',37),(247,'2018-04-19','000-001-01-00037190','HP Tank 5820',1,'$8',4,'Crystal Reports - Yellow Tech - Factura4.pdf',37),(248,'2018-04-19','000-001-01-00037215','HP Tank 5820',1,'$8',4,'Crystal Reports - Yellow Tech - Factura5.pdf',37),(249,'2018-04-21','000-001-01-00037297','HP Tank 5820',1,'$8',4,'Crystal Reports - Yellow Tech - Factura6.pdf',37),(250,'2018-04-21','000-001-01-00037298','HP Tank 5820',1,'$8',4,'Crystal Reports - Yellow Tech - Factura7.pdf',37),(251,'2018-05-04','000-001-01-00037617','HP Tank 5820',1,'$8',11,'Crystal Reports - Yellow Tech - Factura8.pdf',37),(252,'2018-05-07','000-001-01-00037667','HP Tank 5820',1,'$8',4,'Crystal Reports - Yellow Tech - Factura9.pdf',37),(253,'2018-05-08','000-001-01-00037769','HP Tank 5820',2,'$16',16,'Crystal Reports - Yellow Tech - Factura10.pdf',37),(254,'2018-05-12','000-001-01-00037911','HP Tank 5820',1,'$8',8,'Crystal Reports - Yellow Tech - Factura12.pdf',37),(255,'2018-05-28','000-001-01-00038446','HP Tank 5820',1,'$8',6,NULL,37),(256,'2018-05-30','000-001-01-00038530','HP Tank 5820',10,'$80',39,'Crystal Reports - Yellow Tech - Factura13.pdf',37),(257,'2018-06-07','000-001-01-00038859','HP Tank 5820',1,'$8',5,'Crystal Reports - Yellow Tech - Factura14.pdf',37),(258,'2018-06-13','000-001-01-00039024','HP Tank 5820',1,'$8',12,'Crystal Reports - Yellow Tech - Factura15.pdf',37),(259,'2018-06-07','10485','HP Tank 5810',5,'$25',530,NULL,4),(260,'2018-06-07','10485','HP Tank 5820',5,'$40',650,'Factura.pdf',4),(261,'2018-06-24','HOAKL-125','HP Tank 5810',1,'$5',1300,'HOAKL-125.pdf',35),(265,'2018-01-14','000-001-01-00039062','HP Tank 5810',1,'$5',3220,'Crystal Reports - Yellow Tech - Factura13.pdf',37),(266,'2018-06-25','000-001-01-00039450','HP Tank 5810',1,'$5',3505,'Crystal Reports - Yellow Tech - Factura14.pdf',37),(267,'2018-06-25','000-001-01-00039457','HP Tank 5810',1,'$5',2875,'Crystal Reports - Yellow Tech - Factura15.pdf',37),(268,'2018-06-27','000-001-01-00039562','HP Tank 5810',1,'$5',6780,'Crystal Reports - Yellow Tech - Factura16.pdf',37),(269,'2018-06-28','000-001-01-00039606','HP Tank 5810',1,'$5',2826,'Crystal Reports - Yellow Tech - Factura18.pdf',37),(270,'2018-06-28','000-001-01-00039620','HP Tank 5810',1,'$5',2826,NULL,37),(271,'2018-04-05','666666','HP Tank 5820',2,'$16',500,NULL,1),(272,'2018-06-16','1697','HP Tank 5810',1,'$5',113,NULL,22),(273,'2018-06-16','15442','HP Tank 5810',1,'$5',113,NULL,22),(274,'2018-06-21','6424','HP Tank 5810',1,'$5',113,NULL,22),(275,'2018-06-21','1737','HP Tank 5810',1,'$5',113,NULL,22),(276,'2018-06-22','9265','HP Tank 5810',1,'$5',121,NULL,17),(277,'2018-06-22','7555','HP Tank 5810',1,'$5',113,NULL,17),(278,'2018-06-19','15545','HP Tank 5820',1,'$8',190,NULL,17),(279,'2018-04-29','123456','HP Tank 5810',2,'$10',100,'PRUEBA.pdf',1),(280,'2018-06-29','00-001-01-00039653','HP Tank 5810',2,'$10',7388,'Crystal Reports - Yellow Tech - Factura19.pdf',37),(281,'2018-07-02','000-001-01-00039713','HP Tank 5810',1,'$5',2826,'Crystal Reports - Yellow Tech - Factura20.pdf',37),(282,'2018-07-02','000-001-01-00039716','HP Tank 5810',1,'$5',2853,'Crystal Reports - Yellow Tech - Factura21.pdf',37),(283,'2018-07-05','000-001-01-00039847','HP Tank 5810',1,'$5',3424,'Crystal Reports - Yellow Tech - Factura22.pdf',37),(284,'2018-06-23','7479','HP Tank 5810',1,'$5',128,NULL,22),(285,'2018-06-25','7556','HP Tank 5810',1,'$5',113,NULL,22),(286,'2018-06-25','9312','HP Tank 5810',1,'$5',128,NULL,22),(287,'2018-06-26','1782','HP Tank 5810',1,'$5',128,NULL,22),(288,'2018-06-29','9348','HP Tank 5810',1,'$5',128,NULL,17),(289,'2018-06-29','9356','HP Tank 5810',1,'$5',128,NULL,17),(290,'2018-07-02','15718','HP Tank 5810',1,'$5',113,NULL,17),(291,'2018-07-02','9377','HP Tank 5810',1,'$5',128,NULL,17),(292,'2018-06-06','12945','HP Tank 5810',2,'$10',229,NULL,3),(293,'2018-06-13','13080','HP Tank 5810',1,'$5',114,NULL,3),(294,'2018-06-18','13151','HP Tank 5810',1,'$5',116,NULL,3),(295,'2018-06-19','131171','HP Tank 5810',1,'$5',114,'HP GT Junio (3).pdf',3),(296,'2018-06-20','13193','HP Tank 5810',1,'$5',114,NULL,3),(297,'2018-06-19','13171','HP Tank 5810',1,'$5',114,NULL,3),(298,'2018-06-21','13230','HP Tank 5810',2,'$10',229,'HP GT Junio (5).pdf',3),(299,'2018-06-21','13234','HP Tank 5810',1,'$5',114,'HP GT Junio (6).pdf',3),(300,'2018-06-28','13379','HP Tank 5810',1,'$5',114,'HP GT Junio (7).pdf',3),(301,'2018-06-14','13108','HP Tank 5820',1,'$8',139,'HP GT Junio (8).pdf',3),(302,'2018-06-06','12945','HP Tank 5810',2,'$10',229,NULL,3),(303,'2018-06-13','13080','HP Tank 5810',1,'$5',114,'HP GT Junio (1).pdf',3),(304,'2018-06-18','13151','HP Tank 5810',1,'$5',114,'HP GT Junio (2).pdf',3),(305,'2018-06-18','13151','HP Tank 5810',1,'$5',114,NULL,3),(306,'1969-12-31','13385','HP Tank 5820',2,'$16',513,'fed import junio.jpg',5),(307,'1969-12-31','13284','HP Tank 5810',2,'$10',513,'fed import junio.jpg',5),(308,'2018-06-13','13883','HP Tank 5810',4,'$20',2840,'zona libre mall jinio.jpg',5),(309,'2018-06-05','12919','HP Tank 5810',4,'$20',507,'iman poderoso  junio.jpg',5),(310,'2018-06-05','12921','HP Tank 5810',2,'$10',1277,'electro center junio.jpg',5),(311,'2018-07-06','9415','HP Tank 5810',1,'$5',128,NULL,22),(312,'2018-07-07','9425','HP Tank 5810',1,'$5',128,NULL,22),(313,'2018-07-09','9441','HP Tank 5810',1,'$5',128,NULL,22),(314,'2018-07-11','9473','HP Tank 5810',1,'$5',128,NULL,22),(315,'2018-07-11','7619','HP Tank 5810',1,'$5',121,NULL,22),(316,'2018-07-11','15918','HP Tank 5810',1,'$5',113,NULL,22),(317,'2018-07-12','9474','HP Tank 5810',1,'$5',128,NULL,17),(318,'2018-07-12','1631','HP Tank 5810',1,'$5',113,NULL,17),(319,'2018-07-16','1646','HP Tank 5810',1,'$5',113,NULL,17),(320,'2018-07-18','9549','HP Tank 5810',1,'$5',9549,NULL,17),(321,'2018-07-10','000-001-01-00040024','HP Tank 5820',1,'$8',4016,'Crystal Reports - Yellow Tech - Factura16.pdf',37),(322,'2018-07-11','000-001-01-00040068','HP Tank 5820',1,'$8',46058,'Crystal Reports - Yellow Tech - Factura17.pdf',37),(323,'2018-07-13','000-001-01-00040143','HP Tank 5820',1,'$8',4922,'Crystal Reports - Yellow Tech - Factura18.pdf',37),(324,'2018-10-02','asdfasdf','HP DeskJet GT 5810',654,'$3270',66,NULL,1),(325,'2018-10-02','ivan','HP DeskJet GT 5810',45,'$225',54,NULL,1),(326,'2018-10-02','ronald','HP DeskJet GT 5810',34,'$170',34,NULL,1),(327,'2018-10-02','carhua','HP Tank 315',45,'$225',45,'152384811501.pdf',1),(328,'2018-10-01','carhua','HP DeskJet GT 5810',1,'$5',1111,'152384811501.pdf',1),(329,'2018-10-02','ivan','HP DeskJet GT 5810',54,'$270',54,'Brief_para_logotipo.pdf',1),(330,'2018-10-02','asdsda','HP DeskJet GT 5810',54,'$270',65,'Brief_para_logotipo.pdf',1),(331,'2018-10-02','ronald','HP DeskJet GT 5810',45,'$225',45,NULL,1),(332,'2018-10-02','ronald','HP DeskJet GT 5810',45,'$225',45,NULL,1),(333,'2018-10-02','ronald','HP DeskJet GT 5810',98,'$490',45,NULL,1),(334,'2018-10-01','asdsadsad','HP DeskJet GT 5810',98,'$490',6,NULL,1),(335,'2018-10-01','asdsadsad','HP DeskJet GT 5810',98,'$490',6,NULL,1),(336,'2018-10-01','asdsadsad','HP DeskJet GT 5810',98,'$490',6,NULL,1),(337,'2018-10-02','asdsad','HP DeskJet GT 5810',78,'$390',788,NULL,1),(338,'2018-10-02','asdsad','HP DeskJet GT 5810',78,'$390',788,NULL,1),(339,'2018-10-02','asdsad','HP DeskJet GT 5810',78,'$390',788,NULL,1),(340,'2018-10-02','asdsad','HP DeskJet GT 5810',78,'$390',788,NULL,1),(341,'2018-10-02','asdsda','HP DeskJet GT 5810',4,'$20',8,NULL,1),(342,'2018-10-02','asdsda','HP DeskJet GT 5810',4,'$20',8,NULL,1),(343,'2018-10-02','sdafsdf','HP DeskJet GT 5810',4,'$20',78,NULL,1),(344,'2018-10-02','asd','HP DeskJet GT 5810',98,'$490',98,NULL,1),(345,'2018-10-02','sdfsdf','HP DeskJet GT 5810',9879,'$49395',9879,NULL,1),(346,'2018-10-02','sdfsdf','HP DeskJet GT 5810',9879,'$49395',9879,NULL,1),(347,'2018-10-02','asdsad','HP DeskJet GT 5810',988,'$4940',78,NULL,1),(348,'2018-10-03','sdfsdf','HP DeskJet GT 5810',98,'$490',7,NULL,1),(349,'2018-10-02','asdfasfd','HP DeskJet GT 5810',5,'$25',98,NULL,1),(350,'2018-10-03','asdsad','HP DeskJet GT 5810',78,'$390',78,NULL,1),(351,'2018-10-02','asdfasdf','HP DeskJet GT 5810',97987,'$489935',987,NULL,1),(352,'2018-10-02','asdfasdf','HP DeskJet GT 5810',97987,'$489935',987,NULL,1),(353,'2018-10-02','asdfasdf','HP DeskJet GT 5810',97987,'$489935',9877,NULL,1),(354,'2018-10-03','demosfinal','HP DeskJet GT 5810',98,'$490',98,'Brief_para_logotipo21.pdf',1),(355,'2018-10-03','demosfinal','HP DeskJet GT 5810',98,'$490',98,'Brief_para_logotipo22.pdf',1),(356,'2018-10-03','98','HP DeskJet GT 5810',98,'$490',98,'Brief_para_logotipo23.pdf',1),(357,'2018-10-03','fdsf','HP DeskJet GT 5810',646,'$3230',654645,'Brief_para_logotipo24.pdf',1),(358,'2018-10-03','asdf','HP DeskJet GT 5810',98,'$490',6,'Brief_para_logotipo25.pdf',1),(359,'2018-10-03','sdfgsfdg','HP DeskJet GT 5810',987,'$4935',987498,'Brief_para_logotipo26.pdf',1),(360,'2018-10-02','ronald ivan','HP DeskJet GT 5810',65,'$325',65,'Brief_para_logotipo27.pdf',1),(361,'2018-10-03','sad','HP DeskJet GT 5810',987,'$4935',46,'Brief_para_logotipo28.pdf',1),(362,'2018-10-03','987','HP DeskJet GT 5810',544654,'$2723270',654654,'Brief_para_logotipo29.pdf',1);
/*!40000 ALTER TABLE `anotacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(100) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `Pais` varchar(100) DEFAULT NULL,
  `pass` text,
  `Nombre_canal` varchar(100) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Belen Gutierrez Vásquez','belen.gutierrez@merino.com.pe','Nicaragua','belen987','Suministros De Informatica Quinonez Rl',525),(2,'Jhonatan Iberico','jhiberico@gmail.com','Guatemala','123','Dataprint De El Salvador Sa De CV',20),(3,'Santiago Castro Rodriguez ','ventas03@supricom.com','Panamá','sagedari10','Supricom Sa',429),(4,'Yulixa Caballero','ventas08@supricom.com','Panamá','20202020','Supricom Sa',148),(5,'Amilcar Fuentes','amilcarfuentes12@gmail.com','Panamá','654321','Supricom Sa',531),(6,'Juan Bautista Larrazabal Gonzalez ','jlarrazabalg@gmail.com','Guatemala','larrazabal84.0','Dpg Sa De Cv',NULL),(7,'Juan Bautista Larrazabal Gonzalez ','unidiseno@canella.com.gt','Guatemala','29061984','Canella Sa',41),(8,'Eddy Godoy ','unidiseno@canella.com.gt','Guatemala','1234','Canella Sa',NULL),(9,'Oliver Vásquez','praderahome@canella.com.gt','Guatemala','123','Canella Sa',21),(10,'jose','jose.minayac15@gmail.com','Costa Rica','123','Compubetel Sa',24),(11,'Kivanc Ozseden','kiwi@intelaf.com','Guatemala','Intelaf89','Intelaf Sa',10),(12,'Johnny Andrade','johnnycompusersa@gmail.com','Guatemala','johnnyac280975','Compusersa Sa',NULL),(13,'Johnny Andrade','jhonnycompusersa@gmail.com','Guatemala','hp1234','Compusersa Sa',NULL),(14,'Diego Cardona','diegolin_92@hotmail.com','Guatemala','diego1234','Canella Sa',16),(15,'Laura Reyes','lauratohon@gmail.com','Guatemala','Guate2525','Canella Sa',21),(16,'Guadalupe Duran','gdeduran@valdes.com.sv','El Salvador','gdeduran','Equipos Electronicos Valdes Sa De Cv',NULL),(17,'Sonia de Valdes','svaldes@valdes.com.sv','El Salvador','svaldes','Equipos Electronicos Valdes Sa De Cv',NULL),(18,'Sonia de Valdes','svaldes@valdes.com.sv','El Salvador','svaldes','Equipos Electronicos Valdes Sa De Cv',NULL),(19,'Jorge Parker','jparker@valdes.com.sv','El Salvador','jparker','Equipos Electronicos Valdes Sa De Cv',NULL),(20,'Manuel Merino','mmerino@valdes.com.sv','El Salvador','mmerino','Equipos Electronicos Valdes Sa De Cv',NULL),(21,'María Elena de Garcia ','mdegarcia@valdes.com.sv','El Salvador','mdegarcia','Equipos Electronicos Valdes Sa De Cv',NULL),(22,'Maria Isabel Valdes de Iglesias','miglesias@valdes.com.sv','El Salvador','miglesias','Equipos Electronicos Valdes Sa De Cv',NULL),(23,'Wendy Alvarado','walvarado@valdes.com.sv','El Salvador','walvarado','Equipos Electronicos Valdes Sa De Cv',NULL),(24,'Cristina de Mejia','amejia@valdes.com.sv','El Salvador','amejia','Equipos Electronicos Valdes Sa De Cv',NULL),(25,'Emerson Zuñiga','ez869662@gmail.com','Guatemala','popodeperro','Canella Sa',8),(26,'Wendy Cruz','wcruz@cymsistemas.net','El Salvador','IsaacyW3nd1','C&M Sistemas Sa De Cv',NULL),(27,'Maria Luisa Ramirez','mramirez@cymsistemas.net','El Salvador','Mr4m1r3z','C&M Sistemas Sa De Cv',NULL),(28,'Maria Luisa Ramirez','mramirez@cymsistemas.net','El Salvador','M4ryLu','C&M Sistemas Sa De Cv',NULL),(29,'Cecilia Alfaro','calfaro@cymsistemas.net','El Salvador','C4lf4r0','C&M Sistemas Sa De Cv',NULL),(30,'Delmy Quintanilla','dquintanilla@cymsistemas.net','El Salvador','Dqu1nt4n1ll4','C&M Sistemas Sa De Cv',NULL),(31,'Aracely Guzmán','aguzman@cymsistemas.net','El Salvador','Aguzm4n','C&M Sistemas Sa De Cv',NULL),(32,'Lorena Cruz','lcruz@cymsistemas.net','El Salvador','Lcruz@123','C&M Sistemas Sa De Cv',NULL),(33,'Alejandra Hernández','ahernandez@cymsistemas.net','El Salvador','Ah3rnand3z','C&M Sistemas Sa De Cv',NULL),(34,'José Miguel Navarro Avilés','jm@dataprint.com.sv','El Salvador','Dataprinthp','Dataprint De El Salvador Sa De CV',NULL),(35,'Rosa Angelica Lopez Cartajena','rosancartagena@gmail.com','Guatemala','rosangelica','Canella Sa',29),(36,'Eddy Alberto Godoy','ventasunihome@canella.com.gt','Guatemala','123','Canella Sa',NULL),(37,'Josue Flores','j.flores@yellow-technologies.com','Honduras','OLIMPIA1984','Random Industrial S de Rl',392);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `hpe1`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `hpe1` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `hpe1`;

--
-- Table structure for table `tb_cotizacion`
--

DROP TABLE IF EXISTS `tb_cotizacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_cotizacion` (
  `id_cotizacion` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `no_vendedor` varchar(75) NOT NULL,
  `canal` varchar(75) NOT NULL,
  `pais` varchar(50) NOT NULL,
  `tipo_documento` int(11) NOT NULL COMMENT '1 es cotización; 0 es Facturación;',
  `nu_cotizacion` varchar(15) NOT NULL,
  `fecha` date NOT NULL,
  `monto` double NOT NULL,
  `mayorista` varchar(50) NOT NULL,
  `_id_vendedor` int(11) NOT NULL,
  `puntos_cotizados` int(11) NOT NULL DEFAULT '0',
  `puntos_cerrados` int(11) NOT NULL DEFAULT '0',
  `documento` text NOT NULL,
  PRIMARY KEY (`id_cotizacion`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_cotizacion`
--

LOCK TABLES `tb_cotizacion` WRITE;
/*!40000 ALTER TABLE `tb_cotizacion` DISABLE KEYS */;
INSERT INTO `tb_cotizacion` VALUES (32,'idea@book.com','Idea Book','Canal 1','Panama',1,'123','2018-06-05',5000.45,'INTCOMEX (PA)',5,2500,0,''),(33,'zte@zte.com','Zte','Canal 2','Costa Rica',0,'124','2018-06-05',850.5,'INTCOMEX (CR)',4,0,850,''),(34,'trom@correo.com','Trome','Canal 1','Panama',0,'125','2018-06-05',2904,'LOGISTICA (PA)',5,0,1250,''),(35,'mac@mac.com','Mac','Canal 3','Costa Rica',1,'126','2018-06-05',126,'INTCOMEX (CR)',4,1900,0,''),(36,'andrea@gmail.com','Andrea','Canal 1','Panama',0,'1995','2018-06-05',1995,'LOGISTICA (PA)',5,0,800,''),(37,'prueba@prueba.com','Prueba','Canal De Prueba','Panama',0,'1999','2018-06-05',1230.5,'INTCOMEX (PA)',5,0,1850,''),(38,'publimetro@publimetro.com','Publimetro','Canal 3','Costa Rica',0,'12341','2018-06-07',1500,'INTCOMEX (CR)',4,0,150,''),(39,'yealink@yealink.com','Yealink','Canal 2','Guatemala',0,'1992','2018-06-07',1992,'TECNOBODEGA (GT)',6,0,150,''),(40,'celular@celular.com','Celular','Canal 2','Panama',1,'1234','2018-06-11',160,'INTCOMEX (PA) ',5,150,0,''),(41,'hpe@hpe.com','Hpe','Canal Ventas','Panama',1,'12512','2018-06-11',1923,'INTCOMEX (PA)',5,150,0,''),(42,'planta@planta.com','Planta','Canal 1','Panama',0,'12412','2018-06-11',1823,'INTCOMEX (PA) ',5,0,150,''),(43,'jhonatan@iberico.com','Jhonatan@iberico.com','Canal','Panama',0,'14123','2018-06-11',235,'INTCOMEX (PA)',5,0,150,''),(44,'tecnobodega@tecnobodega.com','Tecnobodega','Canales','Guatemala',1,'12321','2018-06-11',634,'INTCOMEX (GT)',6,150,0,''),(45,'hp@hp.com','Hp','Canales','Guatemala',1,'16234','2018-06-11',125,'INTCOMEX (GT)',6,150,0,''),(46,'hp@hp.com','Hp','Canales','Guatemala',0,'12836','2018-06-11',650,'INTCOMEX (GT)',6,0,150,''),(47,'municipalidad@municipalidad.com','Municipalidad','Canales','Guatemala',1,'12412','2018-06-11',7452,'INTCOMEX (GT)',6,150,0,''),(48,'municipalidad@municipalidad.com','Municipalidad','Canales','Guatemala',1,'12412','2018-06-11',7452,'INTCOMEX (GT)',6,150,0,''),(53,'mauricio@merino.com.pe','Jose','Canal 3','Costa Rica',1,'12341','2018-06-05',1234,'INTCOMEX (CR)',4,150,0,'');
/*!40000 ALTER TABLE `tb_cotizacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_mayorista`
--

DROP TABLE IF EXISTS `tb_mayorista`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_mayorista` (
  `id_mayorista` int(11) NOT NULL AUTO_INCREMENT,
  `pais` varchar(50) NOT NULL,
  `mayorista` varchar(50) NOT NULL,
  PRIMARY KEY (`id_mayorista`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_mayorista`
--

LOCK TABLES `tb_mayorista` WRITE;
/*!40000 ALTER TABLE `tb_mayorista` DISABLE KEYS */;
INSERT INTO `tb_mayorista` VALUES (1,'Costa Rica','INTCOMEX (CR)'),(2,'Guatemala','TECNOBODEGA (GT)'),(3,'Guatemala','INTCOMEX (GT)'),(4,'Panama','INTCOMEX (PA)'),(5,'Panama','LOGISTICA (PA)');
/*!40000 ALTER TABLE `tb_mayorista` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_producto`
--

DROP TABLE IF EXISTS `tb_producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_producto` (
  `no_producto` varchar(75) NOT NULL,
  `cantidad` int(11) DEFAULT '0',
  `_id_cotizacion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_producto`
--

LOCK TABLES `tb_producto` WRITE;
/*!40000 ALTER TABLE `tb_producto` DISABLE KEYS */;
INSERT INTO `tb_producto` VALUES ('Windows Server Essentials Edition',2,32),('Windows Server Standard Edition',3,32),('Windows Server Datacenter Edition',4,32),('CALs',5,32),('Windows Server Essentials Edition',5,33),('Windows Server Standard Edition',2,33),('Windows Server Datacenter Edition',5,33),('CALs',0,33),('Windows Server Essentials Edition',5,34),('Windows Server Standard Edition',0,34),('Windows Server Datacenter Edition',3,34),('CALs',7,34),('Windows Server Essentials Edition',5,35),('Windows Server Standard Edition',0,35),('Windows Server Datacenter Edition',0,35),('CALs',12,35),('Windows Server Essentials Edition',2,36),('Windows Server Standard Edition',0,36),('Windows Server Datacenter Edition',4,36),('CALs',3,36),('Windows Server Essentials Edition',5,37),('Windows Server Standard Edition',0,37),('Windows Server Datacenter Edition',7,37),('CALs',9,37),('Windows Server Essentials Edition',1,38),('Windows Server Standard Edition',2,38),('Windows Server Datacenter Edition',0,38),('CALs',4,38),('Windows Server Essentials Edition',4,39),('Windows Server Standard Edition',2,39),('Windows Server Datacenter Edition',5,39),('CALs',0,39),('Windows Server Essentials Edition',2,40),('Windows Server Standard Edition',0,40),('Windows Server Datacenter Edition',0,40),('CALs',6,40),('Windows Server Essentials Edition',5,41),('Windows Server Standard Edition',0,41),('Windows Server Datacenter Edition',25,41),('CALs',12,41),('Windows Server Essentials Edition',5,42),('Windows Server Standard Edition',0,42),('Windows Server Datacenter Edition',4,42),('CALs',12,42),('Windows Server Essentials Edition',4,43),('Windows Server Standard Edition',3,43),('Windows Server Datacenter Edition',0,43),('CALs',2,43),('Windows Server Essentials Edition',3,44),('Windows Server Standard Edition',0,44),('Windows Server Datacenter Edition',2,44),('CALs',4,44),('Windows Server Essentials Edition',2,45),('Windows Server Standard Edition',0,45),('Windows Server Datacenter Edition',5,45),('CALs',0,45),('Windows Server Essentials Edition',10,46),('Windows Server Standard Edition',0,46),('Windows Server Datacenter Edition',15,46),('CALs',0,46),('Windows Server Essentials Edition',12,47),('Windows Server Standard Edition',0,47),('Windows Server Datacenter Edition',42,47),('CALs',12,47),('Windows Server Essentials Edition',12,48),('Windows Server Standard Edition',0,48),('Windows Server Datacenter Edition',42,48),('CALs',12,48),('Windows Server Essentials Edition',14,49),('Windows Server Standard Edition',0,49),('Windows Server Datacenter Edition',14,49),('CALs',0,49),('Windows Server Essentials Edition',0,50),('Windows Server Standard Edition',15,50),('Windows Server Datacenter Edition',0,50),('CALs',15,50),('Windows Server Essentials Edition',0,51),('Windows Server Standard Edition',15,51),('Windows Server Datacenter Edition',0,51),('CALs',15,51),('Windows Server Essentials Edition',13,52),('Windows Server Standard Edition',0,52),('Windows Server Datacenter Edition',0,52),('CALs',13,52),('Windows Server Essentials Edition',12,53),('Windows Server Standard Edition',12,53),('Windows Server Datacenter Edition',12,53),('CALs',12,53);
/*!40000 ALTER TABLE `tb_producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_vendedores`
--

DROP TABLE IF EXISTS `tb_vendedores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_vendedores` (
  `id_vendedor` int(11) NOT NULL AUTO_INCREMENT,
  `no_vendedor` varchar(75) NOT NULL,
  `usuario` varchar(50) NOT NULL,
  `pass` varchar(50) NOT NULL,
  `id_rol` int(1) NOT NULL COMMENT '0 champions; 1 partner',
  `pais` text,
  `_id_mayorista` int(11) NOT NULL,
  PRIMARY KEY (`id_vendedor`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_vendedores`
--

LOCK TABLES `tb_vendedores` WRITE;
/*!40000 ALTER TABLE `tb_vendedores` DISABLE KEYS */;
INSERT INTO `tb_vendedores` VALUES (4,'José Minaya','jose@minaya.com','123',0,'Costa Rica',1),(5,'Jhonatan Iberico','jhonatan@iberico.com','123',1,'Panama',4),(6,'Pedro Yturrizaga','pyf136@gmail.com','123',0,'Guatemala',3),(7,'Mauricio Merino','mauricio@merino.com','123',1,'Costa Rica',1),(8,'Juan Pablo Viquez','jviquez@intcomex.com','123',0,'Costa Rica',1),(9,'Alex Caceres','acaceres@tecnobodega.com.gt','123',0,'Guatemala',2),(10,'Maria Jose Garcia','Mariaj.garcia@intcomex.com','123',0,'Guatemala',3),(11,'Jorge Santamaria','Jorge.santamaria@intcomex.com','123',0,'Panama',4),(12,'Gabriel Castrejón','gcastrejon@logistica.com.pa','123',0,'Panama',5),(13,'Dayana DeGracia','ddegracia@logistica.com.pa','123',0,'Panama',5),(14,'Ariel Engelsztajn','aengelsztajn@intcomex.com','123',1,'Costa Rica',1),(15,'Iris Diaz','idiaz@tecnobodega.com.gt','123',1,'Guatemala',2),(16,'Vivian Mejia','vivian.Mejia@intcomex.com','123',1,'Guatemala',3),(17,'Raúl Lorenzo','raul.lorenzo@intcomex.com','123',1,'Panama',4),(18,'Shaky Daya','sdaya@logistica.com.pa','123',1,'Panama',5),(19,'Administrador HPE','admin@hpe.com','123',0,'',0);
/*!40000 ALTER TABLE `tb_vendedores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `hpe_certificados`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `hpe_certificados` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `hpe_certificados`;

--
-- Table structure for table `emails`
--

DROP TABLE IF EXISTS `emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emails` (
  `Nombres` varchar(255) NOT NULL,
  `Apellidos` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `certificados` varchar(100) DEFAULT NULL,
  `Pais` varchar(100) DEFAULT NULL,
  `nom_certf` varchar(100) DEFAULT NULL,
  `empresa` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emails`
--

LOCK TABLES `emails` WRITE;
/*!40000 ALTER TABLE `emails` DISABLE KEYS */;
INSERT INTO `emails` VALUES ('Daniel','Santana','daniel-arturo.santana@hpe.com',1,'certificado_participacion.pdf','Perú','BASHO: Prospecte de Modo Infalible','SAP'),('Edwin','Rosero','edwin.rosero@hpe.com',2,NULL,NULL,NULL,NULL),('Nora','Hernandez','nora.hdz@hpe.com',3,NULL,NULL,NULL,NULL),('Eric Rene','Lopez','eric.rene.lopez@hpe.com',4,NULL,NULL,NULL,NULL),('Claudia','Magnere Gross','claudia.magnere@hpe.com',5,NULL,NULL,NULL,NULL),('Alejandro','Lomelin','alejandro.lomelin@hpe.com',6,NULL,NULL,NULL,NULL),('Alfredo','Yepez','alfredo.yepez@hpe.com',7,NULL,NULL,NULL,NULL),('Radhika','Saksena','radhika.saksena@hpe.com',8,NULL,NULL,NULL,NULL),('Andres','Campuzano','andres.campuzano@hpe.com',9,NULL,NULL,NULL,NULL),('Marcos','Gaspar','marcos.gaspar@hpe.com',10,NULL,NULL,NULL,NULL),('Cesar','Suarez','cesar.suarez@hpe.com',11,NULL,NULL,NULL,NULL),('Renata Isabel','Durán','renata.duran@hpe.com',12,NULL,NULL,NULL,NULL),('Karla','García','karla-jazmin.garcia@hpe.com',13,NULL,NULL,NULL,NULL),('Jessica','Salinas','jessica.salinas@hpe.com',14,NULL,NULL,NULL,NULL),('Joao','Lopes','joao.lopes@hpe.com',15,NULL,NULL,NULL,NULL),('Eduardo','Mattos Duarte','eduardo@hpe.com',16,NULL,NULL,NULL,NULL),('Russell','Velazquez','russell.velazquez@hpe.com',17,NULL,NULL,NULL,NULL),('Aldo','Sanchez','aldo.sanchez@hpe.com',18,NULL,NULL,NULL,NULL),('Ricardo','Davalos','ricardo.davalos@hpe.com',19,NULL,NULL,NULL,NULL),('Juan Martin','Barrero','juan.barrero@hpe.com',20,NULL,NULL,NULL,NULL),('Lucas','Sanchez Dauria','lucas.sanchez@hpe.com',21,NULL,NULL,NULL,NULL),('Gustavo','Gomez','gustavo.gomez-lopez@hpe.com',22,NULL,NULL,NULL,NULL),('Marcos','Barros Reyes','markos@hpe.com',23,NULL,NULL,NULL,NULL),('Ivonne','Rangel','ivonne.rangel@hpe.com',24,NULL,NULL,NULL,NULL),('Camilo','Martinez T','camilo-david.martinez-troncoso@hpe.com',25,NULL,NULL,NULL,NULL),('Paulina','Muñoz','paulina.munoz@hpe.com',26,NULL,NULL,NULL,NULL),('Anayd','Guerrero','anayd.guerrero@hpe.com',27,NULL,NULL,NULL,NULL),('Gabriela','Zertuche','g.zertuche@hpe.com',28,NULL,NULL,NULL,NULL),('Jose Isaac','Benavides','isaac.benavides@hpe.com',29,NULL,NULL,NULL,NULL),('Javier','Jardon','javier.jardon@hpe.com',30,NULL,NULL,NULL,NULL),('Jose','Lopes','jose.lopes@hpe.com',31,NULL,NULL,NULL,NULL),('Gabriel','Loza','gloza@hpe.com',32,NULL,NULL,NULL,NULL),('Gualberto','Huerta','gualberto.huerta@hpe.com',33,NULL,NULL,NULL,NULL),('Everardo','Cruz','everardo.cruz@hpe.com',34,NULL,NULL,NULL,NULL),('Jorge','Rojas','jorge.rojas@hpe.com',35,NULL,NULL,NULL,NULL),('Marcelo','Fernandez','marcelo.fernandez@hpe.com',36,NULL,NULL,NULL,NULL),('Sebastian','Molina','sebastian.vic.molina@hpe.com',37,NULL,NULL,NULL,NULL),('Noelle','Stecca Teixeira','noelle.stecca@hpe.com',38,NULL,NULL,NULL,NULL),('Mauricio','Conejo','scm@hpe.com',39,NULL,NULL,NULL,NULL),('David','Soto','david.soto@hpe.com',40,NULL,NULL,NULL,NULL),('Luis Pablo','Alcala','luis.alcala@hpe.com',41,NULL,NULL,NULL,NULL),('Carla','Madeo','carla.madeo@hpe.com',42,NULL,NULL,NULL,NULL),('Oscar','Ruiz','oruiz@hpe.com',43,NULL,NULL,NULL,NULL),('Gerardo','Trevino','gerardo.trevino@hpe.com',44,NULL,NULL,NULL,NULL),('Daniel','Jurado','daniel.jurado@hpe.com',45,NULL,NULL,NULL,NULL),('Ivana','Kristoff','IVANA.KRISTOFF@HPE.COM',46,NULL,NULL,NULL,NULL),('Cecilia','Freire','cecilia.freire@hpe.com',47,NULL,NULL,NULL,NULL),('Jorge Luis','Delgado','jorgeluis.delgado@hpe.com',48,NULL,NULL,NULL,NULL),('Danilo','Tomasulo','danilo.tomasulo@hpe.com',49,NULL,NULL,NULL,NULL),('Mariano','Gerber','mariano.gerber@hpe.com',50,NULL,NULL,NULL,NULL),('Vanessa','Crusius','vanessa.crusius@hpe.com',51,NULL,NULL,NULL,NULL),('Oscar','Bolivar','oscar.bolivar@hpe.com',52,NULL,NULL,NULL,NULL),('Gaston','Chague','gaston.chague@hpe.com',53,NULL,NULL,NULL,NULL),('Mauricio','Walder','mauriciowalder@hpe.com',54,NULL,NULL,NULL,NULL),('Ismael','Hernandez','ismael.hernandez@hpe.com',55,NULL,NULL,NULL,NULL),('Joaquin','Namihas','joaquin.namihas@hpe.com',56,NULL,NULL,NULL,NULL),('Alfonso','Camacho','alfonso.camacho@hpe.com',57,NULL,NULL,NULL,NULL),('Leonardo','Jardino','leonardo.jardino@hpe.com',58,NULL,NULL,NULL,NULL),('Jose Francisco','Salmeron','jose.salmeron@hpe.com',59,NULL,NULL,NULL,NULL),('Mariano','Figallo','mariano.figallo@hpe.com',60,NULL,NULL,NULL,NULL),('Alexandre','Bonassa','bonassa@hpe.com',61,NULL,NULL,NULL,NULL),('Bernardo','Miretzky','bernardo@hpe.com',62,NULL,NULL,NULL,NULL),('Edyson','Polistchuk','edyson.polistchuk@hpe.com',63,NULL,NULL,NULL,NULL),('Roberto','Storniolo','roberto.storniolo@hpe.com',64,NULL,NULL,NULL,NULL),('Monica','Costa','monica.costa@hpe.com',65,NULL,NULL,NULL,NULL),('Carlos','Trejos','carlos.trejos@hpe.com',66,NULL,NULL,NULL,NULL),('Fabio','Alves','fabio.alves@hpe.com',67,NULL,NULL,NULL,NULL),('Alberto','Tapia','alberto.tapia@hpe.com',68,NULL,NULL,NULL,NULL),('Jesus','Marquez','jesus.marquez@hpe.com',69,NULL,NULL,NULL,NULL),('Andres','Uribe','andres.uribe@hpe.com',70,NULL,NULL,NULL,NULL),('Alvaro','Wong','alvaro.wong@hpe.com',71,NULL,NULL,NULL,NULL),('Helio','Schor','helio.schor@hpe.com',72,NULL,NULL,NULL,NULL),('Ronan','Marques','ronan.marques@hpe.com',73,NULL,NULL,NULL,NULL),('Salvador','Arllentar','salvador.arllentar@hpe.com',74,NULL,NULL,NULL,NULL),('Pedro','Rozati','pedro.rozati@hpe.com',75,NULL,NULL,NULL,NULL),('David','Moreno','dmoreno@hpe.com',76,NULL,NULL,NULL,NULL),('Fabian','Teran','fabian.teran@hpe.com',77,NULL,NULL,NULL,NULL),('Andre','Chamoun','andre.chamoun@hpe.com',78,NULL,NULL,NULL,NULL),('Venilton','de Carvalho Junior','venilton.junior@hpe.com',79,NULL,NULL,NULL,NULL),('Fernando','Maia','fernando.maia@hpe.com',80,NULL,NULL,NULL,NULL),('Rubens','Monteiro','rubens.monteiro@hpe.com',81,NULL,NULL,NULL,NULL),('Leonardo','Soto','leonardo.soto@hpe.com',82,NULL,NULL,NULL,NULL),('Maria Jose','Menendez','maria.menendez@hpe.com',83,NULL,NULL,NULL,NULL),('Richard','Greenwalt','richard.greenwalt@hpe.com',84,NULL,NULL,NULL,NULL),('Jonathan','Alonso','jonathan.alonso.salgado@hpe.com',85,NULL,NULL,NULL,NULL),('Jairo','Deaza','jairo.deaza@hpe.com',86,NULL,NULL,NULL,NULL),('Antonio Mariano','Carlos Junior','antonio.mariano@hpe.com',87,NULL,NULL,NULL,NULL),('Ramón','Quintana','ramon.quintana@hpe.com',88,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `emails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `hpe_intel_cr`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `hpe_intel_cr` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `hpe_intel_cr`;

--
-- Table structure for table `participante`
--

DROP TABLE IF EXISTS `participante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `participante` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `telefono` varchar(50) NOT NULL,
  `empresa` varchar(50) NOT NULL,
  `cargo` varchar(50) NOT NULL,
  `pais` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `flg_correo` int(11) NOT NULL COMMENT 'Si es 1 si tiene correo, Si es 2 no tiene correo',
  `flg_telefono` int(11) NOT NULL COMMENT 'Si es 1 si tiene telefono , Si es 2 no tiene telefono',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participante`
--

LOCK TABLES `participante` WRITE;
/*!40000 ALTER TABLE `participante` DISABLE KEYS */;
INSERT INTO `participante` VALUES (3,'Mauricio','Merino','mauricio@merino.com.pe','836738728','MerinoBrain','DGA','Peru','2018-09-12',0,0),(6,'test','test','jose.minayac15@gmail.com','test','test','test','test','2018-09-14',1,2);
/*!40000 ALTER TABLE `participante` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `hpe_intel_pr`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `hpe_intel_pr` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `hpe_intel_pr`;

--
-- Table structure for table `participante`
--

DROP TABLE IF EXISTS `participante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `participante` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `telefono` varchar(50) NOT NULL,
  `empresa` varchar(50) NOT NULL,
  `cargo` varchar(50) NOT NULL,
  `pais` varchar(50) NOT NULL,
  `flg_correo` int(11) NOT NULL COMMENT 'Si es 1 si tiene correo, Si es 2 no tiene correo',
  `flg_telefono` int(11) NOT NULL COMMENT 'Si es 1 si tiene telefono , Si es 2 no tiene telefono',
  `fecha` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participante`
--

LOCK TABLES `participante` WRITE;
/*!40000 ALTER TABLE `participante` DISABLE KEYS */;
INSERT INTO `participante` VALUES (7,'ronald','carhua','ricv.sistemas@gmail.com','987654321','ricv','jefe','peru',1,1,'2018-10-10 17:08:34');
/*!40000 ALTER TABLE `participante` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `hpe_oportunidades`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `hpe_oportunidades` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `hpe_oportunidades`;

--
-- Table structure for table `tb_cotizacion`
--

DROP TABLE IF EXISTS `tb_cotizacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_cotizacion` (
  `id_cotizacion` int(11) NOT NULL AUTO_INCREMENT,
  `_id_vendedor` int(11) NOT NULL,
  `Nombre` varchar(65) NOT NULL,
  `compania` varchar(50) NOT NULL,
  `pais` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `flg_cuenta_eg` int(11) NOT NULL COMMENT '1: Activa;  0: inactiva',
  `no_mayorista` varchar(50) NOT NULL,
  `no_contacto_mayo` varchar(65) NOT NULL,
  `email_contacto` varchar(65) NOT NULL,
  `nu_factura` varchar(15) NOT NULL,
  `fecha_factura` date NOT NULL,
  `monto_final` double NOT NULL,
  `puntos` int(11) NOT NULL,
  `documento` varchar(120) NOT NULL,
  PRIMARY KEY (`id_cotizacion`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_cotizacion`
--

LOCK TABLES `tb_cotizacion` WRITE;
/*!40000 ALTER TABLE `tb_cotizacion` DISABLE KEYS */;
INSERT INTO `tb_cotizacion` VALUES (1,1,'Jose','jose','Jose','jose.minayac15@gmail.com','test',1,'Solution Box','Test','test@gmail.com','test','2018-08-25',12,50,'Guia_colores_clientes.pdf'),(2,1,'Jose','jose','Jose','jose.minayac15@gmail.com','test',1,'Intcomex','Test','test@gmail.com','test','2018-08-26',12,50,'Guia_colores_clientes.pdf');
/*!40000 ALTER TABLE `tb_cotizacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_mayorista`
--

DROP TABLE IF EXISTS `tb_mayorista`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_mayorista` (
  `id_mayorista` int(11) NOT NULL AUTO_INCREMENT,
  `pais` varchar(50) NOT NULL,
  `mayorista` varchar(50) NOT NULL,
  PRIMARY KEY (`id_mayorista`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_mayorista`
--

LOCK TABLES `tb_mayorista` WRITE;
/*!40000 ALTER TABLE `tb_mayorista` DISABLE KEYS */;
INSERT INTO `tb_mayorista` VALUES (1,'Centro américa','LOGISTICA (PA)'),(2,'Centro américa','INTCOMEX (PA)'),(3,'Centro américa','INTCOMEX (CR)'),(4,'Centro américa','INTCOMEX (SV)'),(5,'Centro américa','TECNOBODEGA (SV)'),(6,'Centro américa','DIGIX (SV)'),(7,'Centro américa','INTCOMEX (GT)'),(8,'Centro américa','TECNOBODEGA (GT)'),(9,'Caribe','INTCOMEX'),(10,'Caribe','INGRAM MICRO'),(11,'Caribe','SOLUTION BOX'),(12,'Caribe','TECHDATA'),(13,'Caribe','WESTHAM TRADE COMPANY');
/*!40000 ALTER TABLE `tb_mayorista` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_vendedores`
--

DROP TABLE IF EXISTS `tb_vendedores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_vendedores` (
  `id_vendedor` int(11) NOT NULL AUTO_INCREMENT,
  `no_vendedor` varchar(75) NOT NULL,
  `usuario` varchar(50) NOT NULL,
  `pass` varchar(50) NOT NULL,
  `id_rol` int(1) NOT NULL COMMENT '0 champions; 1 partner',
  `pais` text,
  `region` varchar(50) NOT NULL,
  `compania` varchar(65) NOT NULL,
  PRIMARY KEY (`id_vendedor`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_vendedores`
--

LOCK TABLES `tb_vendedores` WRITE;
/*!40000 ALTER TABLE `tb_vendedores` DISABLE KEYS */;
INSERT INTO `tb_vendedores` VALUES (1,'jose','jose.minayac15@gmail.com','jose',1,'jose','Caribe','jose');
/*!40000 ALTER TABLE `tb_vendedores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `hpe_westham`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `hpe_westham` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `hpe_westham`;

--
-- Table structure for table `participante`
--

DROP TABLE IF EXISTS `participante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `participante` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `telefono` varchar(50) NOT NULL,
  `empresa` varchar(50) NOT NULL,
  `cargo` varchar(50) NOT NULL,
  `ciudad` varchar(50) NOT NULL,
  `pais` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participante`
--

LOCK TABLES `participante` WRITE;
/*!40000 ALTER TABLE `participante` DISABLE KEYS */;
INSERT INTO `participante` VALUES (1,'test','test','jose.minayac15@gmail.com','test','test','test','Panamá','Panamá','2018-09-14'),(2,'test','test','pyf136@gmail.com','test','test','test','El Salvador','El Salvador','2018-09-14'),(3,'test','test','ricv.sistemas@gmail.com','test','test','test','Panamá','Panamá','2018-09-14');
/*!40000 ALTER TABLE `participante` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `laboratoriosbootcamp`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `laboratoriosbootcamp` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `laboratoriosbootcamp`;

--
-- Table structure for table `emails`
--

DROP TABLE IF EXISTS `emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emails` (
  `Nombres` varchar(255) NOT NULL,
  `Apellidos` varchar(255) DEFAULT NULL,
  `Pais` varchar(100) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emails`
--

LOCK TABLES `emails` WRITE;
/*!40000 ALTER TABLE `emails` DISABLE KEYS */;
INSERT INTO `emails` VALUES ('Leonardo','Soto','Colombia','leonardo.soto@hpe.com',1),('Javier','Ramírez','','',2),('Maria Jose','Menendez','','',3),('Jonathan','Alonso','Mexico','jonathan.alonso.salgado@hpe.com',4),('Daniel','Santana','Mexico','daniel-arturo.santana@hpe.com',5),('Edwin','Rosero','Colombia','edwin.rosero@hpe.com',6),('Candida','Valois','United States','candida.valois@scality.com',7),('Juan Manuel','Romero','United States','jromero@vmware.com',8),('Juan','Altamar','United States','juan@scality.com',9),('Nora','Hernandez','Mexico','nora.hdz@hpe.com',10),('Eric Rene','Lopez','Mexico','eric.rene.lopez@hpe.com',11),('Claudia','Magnere Gross','Chile','claudia.magnere@hpe.com',12),('Alejandro','Lomelin','Mexico','alejandro.lomelin@hpe.com',13),('Alfredo','Yepez','United States','alfredo.yepez@hpe.com',14),('Radhika','Saksena','United States','radhika.saksena@hpe.com',15),('Andres','Campuzano','United States','andres.campuzano@hpe.com',16),('Marcos','Gaspar','Brazil','marcos.gaspar@hpe.com',17),('Cesar','Suarez','Colombia','cesar.suarez@hpe.com',18),('Renata Isabel','Durán','Mexico','renata.duran@hpe.com',19),('Karla','García','Mexico','karla-jazmin.garcia@hpe.com',20),('Jessica ','Salinas','Mexico','jessica.salinas@hpe.com',21),('Joao','Lopes','Brazil','joao.lopes@hpe.com',22),('Eduardo','Mattos Duarte','United States','eduardo@hpe.com',23),('Russell','Velazquez','Mexico','russell.velazquez@hpe.com',24),('Aldo','Sanchez','Mexico','aldo.sanchez@hpe.com',25),('Ricardo','Davalos','Mexico','ricardo.davalos@hpe.com',26),('Juan Martin','Barrero','Latin America','juan.barrero@hpe.com',27),('Lucas','Sanchez Dauria','Puerto Rico','lucas.sanchez@hpe.com',28),('Gustavo','Gomez','Mexico','gustavo.gomez-lopez@hpe.com',29),('Marcos','Barros Reyes','Argentina','markos@hpe.com',30),('Ivonne','Rangel','Mexico','ivonne.rangel@hpe.com',31),('Camilo','Martinez T','Colombia','camilo-david.martinez-troncoso@hpe.com',32),('Paulina','Muñoz','Mexico','paulina.munoz@hpe.com',33),('Anayd','Guerrero','Mexico','anayd.guerrero@hpe.com',34),('Gabriela','Zertuche','Mexico','g.zertuche@hpe.com',35),('Jose Isaac','Benavides','Mexico','isaac.benavides@hpe.com',36),('Javier','Jardon','Mexico','javier.jardon@hpe.com',37),('Jose','Lopes','Brazil','jose.lopes@hpe.com',38),('Gabriel','Loza','Chile','gloza@hpe.com',39),('Gualberto','Huerta','United States','gualberto.huerta@hpe.com',40),('Everardo','Cruz','Mexico','everardo.cruz@hpe.com',41),('Jorge','Rojas','United States','jorge.rojas@hpe.com',42),('Marcelo','Fernandez','Argentina','marcelo.fernandez@hpe.com',43),('Sebastian','Molina','Argentina','sebastian.vic.molina@hpe.com',44),('Noelle','Stecca Teixeira','Brazil','noelle.stecca@hpe.com',45),('Mauricio','Conejo','Costa rica','scm@hpe.com',46),('David','Soto','Mexico','david.soto@hpe.com',47),('Luis Pablo','Alcala','United States','luis.alcala@hpe.com',48),('Carla','Madeo','Argentina','carla.madeo@hpe.com',49),('Oscar','Ruiz','Costa Rica','oruiz@hpe.com',50),('Gerardo','Trevino','Mexico','gerardo.trevino@hpe.com',51),('Daniel','Jurado','Colombia','daniel.jurado@hpe.com',52),('Ivana','Kristoff','Argentina','IVANA.KRISTOFF@HPE.COM',53),('Cecilia','Freire','Argentina','cecilia.freire@hpe.com',54),('Jorge Luis','Delgado','Peru','jorgeluis.delgado@hpe.com',55),('Danilo','Tomasulo','Brazil','danilo.tomasulo@hpe.com',56),('Mariano','Gerber','Argentina','mariano.gerber@hpe.com',57),('Vanessa','Crusius','Brazil','vanessa.crusius@hpe.com',58),('Oscar','Bolivar','Colombia','oscar.bolivar@hpe.com',59),('Gaston','Chague','Argentina','gaston.chague@hpe.com',60),('Mauricio','Walder','Brazil','mauriciowalder@hpe.com',61),('ismael','hernandez','Puerto Rico','ismael.hernandez@hpe.com',62),('Joaquin','Namihas','Peru','joaquin.namihas@hpe.com',63),('Alfonso','Camacho','Peru','alfonso.camacho@hpe.com',64),('Leonardo','Jardino','Brazil','leonardo.jardino@hpe.com',65),('Jose Francisco','Salmeron','Mexico','jose.salmeron@hpe.com',66),('Mariano','Figallo','Argentina','mariano.figallo@hpe.com',67),('Alexandre','Bonassa','brazil','bonassa@hpe.com',68),('Alexandre','Bonassa','brazil','bonassa@hpe.com',69),('Edyson','Polistchuk','Brazil','edyson.polistchuk@hpe.com',70),('Roberto','Storniolo','Brazil','roberto.storniolo@hpe.com',71),('Monica','Costa','Brazil','monica.costa@hpe.com',72),('Carlos','Trejos','Costa Rica','carlos.trejos@hpe.com',73),('Fabio','Alves','Brazil','fabio.alves@hpe.com',74),('Alberto','Tapia','Puerto Rico','alberto.tapia@hpe.com',75),('Jesus','Marquez','Peru','jesus.marquez@hpe.com',76),('Andres','Uribe','Chile','andres.uribe@hpe.com',77),('Alvaro','Wong','Peru','alvaro.wong@hpe.com',78),('Helio','Schor','BRAZIL','helio.schor@hpe.com',79),('Ronan','Marques','Brazil','ronan.marques@hpe.com',80),('Salvador','Arllentar','Peru','salvador.arllentar@hpe.com',81),('Pedro','Rozati','United States','pedro.rozati@hpe.com',82),('David','Moreno','Chile','dmoreno@hpe.com',83),('Fabian','Teran','Chile','fabian.teran@hpe.com',84),('Andre','Chamoun','Brazil','andre.chamoun@hpe.com',85),('Venilton','de Carvalho Junior','Brazil','venilton.junior@hpe.com',86),('Fernando','Maia','Brazil','fernando.maia@hpe.com',87),('Rubens','Monteiro','Brazil','rubens.monteiro@hpe.com',88);
/*!40000 ALTER TABLE `emails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eventos`
--

DROP TABLE IF EXISTS `eventos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eventos` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date DEFAULT NULL,
  `event_name` varchar(255) DEFAULT NULL,
  `vacantes` int(8) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eventos`
--

LOCK TABLES `eventos` WRITE;
/*!40000 ALTER TABLE `eventos` DISABLE KEYS */;
INSERT INTO `eventos` VALUES (1,'2018-01-15','LAB (DAPR) ',25),(2,'2018-01-15','LAB (simplivity) ',23),(3,'2018-01-15','VMWare - HIT Mgmnt, Brocade - HIT Mgmnt',20),(5,'2018-01-16','LAB (nimble)',25),(6,'2018-01-16','LAB (oneview)',23),(7,'2018-01-16','LAB (DAPR)',24),(8,'2018-01-16','LAB (simplivity)',25),(9,'2018-01-16','3PAR Competitive Lab',25),(10,'2018-01-16','Veeam - HIT Mgmnt, Scality - HIT Mgmnt',25),(12,'2018-01-17','LAB (nimble)',25),(13,'2018-01-17','LAB (oneview)',24),(14,'2018-01-17','LAB (DAPR)',25),(15,'2018-01-17','LAB (simplivity)',23),(16,'2018-01-17','Veeam - HIT Mgmnt, Scality - HIT Mgmnt',25);
/*!40000 ALTER TABLE `eventos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inscritos`
--

DROP TABLE IF EXISTS `inscritos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inscritos` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `id_evento` int(11) NOT NULL,
  `id_pers` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inscritos`
--

LOCK TABLES `inscritos` WRITE;
/*!40000 ALTER TABLE `inscritos` DISABLE KEYS */;
INSERT INTO `inscritos` VALUES (4,3,76),(5,3,1),(6,2,14),(7,4,18),(8,6,18),(9,15,18),(10,3,21),(11,3,23),(12,2,20),(13,6,20),(14,13,20),(15,3,8),(16,7,8),(17,15,8);
/*!40000 ALTER TABLE `inscritos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `lacf`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `lacf` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `lacf`;

--
-- Table structure for table `asistentes`
--

DROP TABLE IF EXISTS `asistentes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asistentes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `apellido` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `celular` varchar(50) DEFAULT NULL,
  `empresa` varchar(100) DEFAULT NULL,
  `cargo` varchar(50) DEFAULT NULL,
  `pais` varchar(50) DEFAULT NULL,
  `ciudad` varchar(50) DEFAULT NULL,
  `pasaporte` varchar(25) DEFAULT NULL,
  `vencimiento` date DEFAULT NULL,
  `visa` char(4) DEFAULT NULL,
  `reunion` char(4) DEFAULT NULL,
  `detalle` varchar(150) DEFAULT NULL,
  `fecreg` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asistentes`
--

LOCK TABLES `asistentes` WRITE;
/*!40000 ALTER TABLE `asistentes` DISABLE KEYS */;
INSERT INTO `asistentes` VALUES (1,'Rodrigo','Loyola','rloyola@unab.cl','998209941','Universidad Andrés Bello','CIO','Chile','Santiago','P07735843','2019-05-28','Si','NULL','NULL','2018-05-18 08:36:00'),(2,'Ariani','akakakaak','ariani.beraun@merino.com.pe','68999','jkjjn','llll','PERU','Lima','6666','2018-11-21','Si','NULL','NULL','2018-05-18 06:28:00'),(3,'sdsd','sds','ariani.beraun@gmail.com','5666','kjnknj','lknlmklm','kmkm','kmlm','5555','2018-05-31','Si','NULL','sds','2018-05-21 08:11:00'),(4,'ashdadjka','asdasdasd','asdasda@merino.com','39393939','merino','asdhjkadhjk','ashdajks','ashdkasjd','8989789','2018-05-17','Si','No','NULL','2018-05-21 08:11:00'),(5,'ashdjkadhajk','ashdkasjd','asddadasd@merino.com','93939393','merino','sdhkajdjk','ashdjkasj','ashdkasdhjk','3939393','2018-05-31','Si','No','NULL','2018-05-21 08:13:00'),(6,'ashdadahk','ahskdashdjkas','adadad@merino.com.pe','39393939','merino','ahsdjkahdkj','ashdjksdhjk','asjlkdasjdkl','393939393','2018-05-27','Si','No','NULL','2018-05-21 08:16:00'),(7,'Juan Eduardo','Galvez Bustamante','juan.galvez@cl.pwc.com','56998174438','PwC','Gerente - Informática','Chile','Santiago','P08893300','2020-11-20','No','No','SEGURIDAD','2018-05-22 03:02:00'),(8,'Ricardo','Ortiz Jimenez','rortiz@alfa.com.mx','5218110805505','ALFA','Gerente de TI','Mexico','Monterrey','G09815712','2018-07-02','Si','No','.','2018-05-24 02:29:00'),(9,'victor','martinez','victor.martinez@gis.com.mx','8441225081','GIS','CIO','MEXICO','MONTERREY','G12391978','2023-07-03','Si','Si','HP','2018-05-24 05:57:00'),(10,'carlos','granados villanueva','carlos.granados@talisis.com','5545549410','Talisis','CIO','Mexico','Monterrey','G043541170','2020-04-01','Si','No','Computo personal','2018-05-25 09:19:00'),(11,'ALBERTO','ZAMBRANO','alberto.zambrano@uanl.mx','8118000822','UNIVERSIDAD AUTÓNOMA DE NUEVO LEÓN','DIRECTOR DE TECNOLOGÍAS DE INFORMACIÓN','MEXICO','MONTERREY','G08862437','2022-03-02','Si','NULL','NULL','2018-05-28 09:23:00'),(12,'Jessica','Villarroel','jvilla@med.puc.cl','+56 9 5238 1120','Red de Salud UC CHRISTUS','Subgerente de Servicios TI','Chile','Santiago','F12646013','2021-05-30','Si','NULL','NULL','2018-05-28 09:41:00'),(13,'JAIR','FIERRO','jair_fierro@penoles.com.mx','5540803904','PENOLES','IT Manager','México','MEXICO','G09351122','2022-05-04','Si','NULL','NULL','2018-05-29 09:35:00'),(14,'Pamela','Barrientos','pamela.barrientos@laureate.net','+504 94730225','Laureate Education Inc','Manager, Global IT Procurement','Honduras','Tegucigalpa','C893605','2021-09-05','Si','NULL','NULL','2018-06-06 03:12:00'),(15,'Renato','Samel','renato.samel@accenture.com','5521992934913','Accenture','LATAM Delivery Lead | Brazil TS Lead','Brasil','Rio de Janeiro','FP831429','2026-05-11','Si','NULL','NULL','2018-06-07 04:37:00'),(16,'Francisco','Toro','ftoror@bci.cl','56992404199','BCI','Subgerente de Proyectos Estratégicos','Chile','Santiago','P018001667','2019-09-14','Si','NULL','NULL','2018-06-08 06:04:00'),(17,'Fernando','Calvo','fcalvo@bci.cl','+56 9 6908 5466','BCI','Gerente de Operaciones Banco Retail','Chile','Santiago','P05080606','2020-08-25','Si','NULL','NULL','2018-06-08 06:07:00'),(18,'Jorge','Silva','jorge.silva@bci.cl','+56 9 9289 3886','BCI','Gerente de Sucursales','Chile','Santiago','76866125','2018-08-12','Si','NULL','NULL','2018-06-08 06:14:00'),(19,'Jesus','Miranda','jesus.mirandagonzalez@halliburton.com','+58 414 8450052','Halliburton','Landamrk Service Manager','Venezuela','Puerto la Cruz','111585267','2019-12-07','Si','NULL','NULL','2018-06-08 07:14:00'),(20,'Hugo','Amezcua','hugo.amezcua@ge.com','3052157632','GE','Digital Operations Director - LATAM','US','Miami','Already in the US','2018-12-03','Si','NULL','NULL','2018-06-08 09:08:00'),(21,'Federico','Fuentes','federico.fuentes@tata.com.uy','092 233 699','Tata','Jefe de Tecnología','Uruguay','Montevideo','C654150','2020-04-21','Si','NULL','NULL','2018-06-08 10:19:00'),(22,'shdkajsdhasjk','ahsdasjkd','asdasdasda@merino.com','9393939393','merinio','asdhasjk','ashdjkashk','ashdjkashjk','89789789','2018-06-15','No','Si','haskdjashdjkasdhjk','2018-06-11 08:00:00'),(23,'Test','B','yanela4@gmail.com','98778888','merino','Mkt','PERU','Lima','5222','2018-12-27','Si','Si','rrrrr','2018-06-11 10:34:00'),(24,'Eduardo','Rodrigues','eduardo.rodrigues@ge.com','5511964318379','General Eletric','Director Digital Operations','Brasil','Sao Paulo','FS087314','2026-12-07','No','No','.','2018-06-12 11:03:00'),(25,'Patricio','Tapia Jahiatt','patricio.tapia@uai.cl','966764229','Universidad Adolfo Ibañez','SubDirector Infraestructura','Chile','Santiago','P07049757','2020-04-10','Si','Si','Otras universidades','2018-06-12 02:41:00'),(26,'Carlos Andres','Duque Quintero','carlosandres.duque@confa.co','3122572658','CONFA - Caja de compensación familiar de Caldas','Gerente Tecnología','Colombia','Manizales','AM621105','2021-01-01','Si','No','Innovación','2018-06-13 10:58:00'),(27,'Francesca','Gatica','fgatica@ccu.cl','56982878855','CCU S.A.','Jefe de seguridad informatica y soporte ti','Chile','Santiago','F19457120','1983-09-12','No','Si','relacionado con seguridad de información y soprote ti','2018-06-14 03:55:00'),(28,'carlos','granados villanueva','carlosgravi@hotmail.com','5545549410','Talisis','CIO','Mexico','Monterrey','g04354117','2020-03-19','Si','Si','computo personal y de escritorio ','2018-06-15 03:38:00'),(29,'Pedro','Calderón F.','pedro.calderon@bci.cl','56996306610','Banco Bci','Gerente Soporte Servicios Tecnológicos','Chile','Santiago','F10720142','2021-03-10','Si','No','HP Y SOCIOS','2018-06-18 07:34:00'),(30,'Efrain','Mercado Quispe','emercado@cmac-cusco.com.pe','984922778','Caja Municipal de Ahorro y Credito Cusco','Sub Gerencia de I.T','Peru','Cusco','6073031','2019-06-19','Si','Si','innovacion y seguridad','2018-06-20 11:33:00'),(31,'Edward','Alarcon','edward.alarcon@hocplc.com','+51 995076413','Hochschild Mining plc','Gerente Corporativo de Tecnología de Información','Perú','Lima','117205535','2023-05-18','Si','No','A','2018-06-20 07:52:00'),(32,'Jorge','Martinez','jorge.antonio@outlook.com','7876906620','Banco Popular de Puerto Rico','Technology Management','Puerto Rico','San Juan','NA','2019-01-01','No','No','NA','2018-06-25 10:45:00'),(33,'asdajsdkl','ajsdklasjdkla','sadfsadadsd@merino.com','39393939','ashdjkashjk','ashdjkahjk','ashdkjasj','ashdashjk','ashdjkashjk','2018-06-28','Si','No','jlkjlkjlkjlk','2018-06-25 01:25:00'),(34,'Rafael','Castillo','racastillom@banconal.com.pa','5075185250','Banco Nacional de Panama','Gerente de Area de Infraestrucutra Descentralizada','Panama','Panama','8-220-1014','2021-05-12','Si','No','servicios de impresion','2018-06-25 02:31:00'),(35,'Jorge','Cadavid','Jicadavid@tcc.com.co','3117641896','GRUPO LOGISTICO TCC','Director Tecnología','COLOMBIA','Medellin','Aq785321','2025-03-31','Si','Si','Megatendencias','2018-06-25 05:42:00'),(36,'MIGUEL','BORBOLLA OLEA','mborbolla@cie.com.mx','5544507500','OCESA','Director de Sistemas','MEXICO','Ciudad de Mexico','G09103898','2019-04-04','Si','Si','HP, otras empresas del sector','2018-06-26 10:11:00'),(37,'Saul','Rangel Gallardo','saul.rangel@ttec.com','5515103576','TTEC Mexico','IT Director','Mexico','CDMX','G12588933','2019-07-10','Si','No','.','2018-06-26 02:40:00'),(38,'J. Salvador','Lopez-Portillo','slopezportillo@cie.com.mx','5591993916','GRUPO CIE','CIO','México','MEXICO','G10180782','1964-12-22','Si','Si','Seguridad de la Información, BI+AI, Data Governance, Digital Transforamation','2018-06-26 06:04:00'),(39,'Gaston','Corales','gastoncorales@macro.com.ar','5491137660107','Banco Macro','Gerente IT','Argentina','Ciudad Autonoma de Buenos Aires','AAB12345','2020-06-27','Si','Si','HP','2018-06-27 03:15:00'),(40,'Arturo','Marroquin','arturo.marroquin@softtek.com','8119324444','SOFTTEK','Global IT Director','Mexico','Monterrey','g19591318','2018-02-26','Si','No','.','2018-06-27 05:19:00'),(41,'Mariano','Magaña','mariano.magana@telusinternational.com','±50250197857','TELUS','VP of Technology','Guatemala','Guatemala','162786182','2022-05-02','Si','Si','Llamada','2018-06-27 06:23:00'),(42,'Juan','Padilla Castillo','juan.padilla@telusinternational.com','+503 78425471','TELUS International','IT Director','El Salvador','San Salvador','B01363326','2022-05-12','Si','Si','Otras empresas del sector','2018-06-27 11:06:00'),(43,'Federico','Restrepo','federico.restrepo@crystal.com.co','3147403109','Crystal SAS','Director TI','Colombia','Medellin','AR275000','2025-07-16','Si','Si','Equipos por servicio - Virtualización','2018-06-28 03:21:00'),(44,'Leonardo','Sanguino','lforero@hp.com','316 4400195','Jazzplast Colombia SAS','Gerente General','Colombia','Bogotá','TBD1234567','2020-06-30','Si','No','N/A','2018-06-28 06:53:00'),(45,'William','Escobar','maria.florez@hp.com','3155358098','Banco Davivienda','Gerente de Micro Informática','Colombia','Bogotá','TBD1234567','2020-06-30','Si','No','NA','2018-06-28 06:57:00'),(46,'Saúl','Reyes','maria.claudia.florez.quinonez@hp.com','3155358098','Coorporación Universitaria Minuto de Dios','Gerente de Servicios Tecnológicos','Colombia','Bogotá','TBD1234567','2021-06-30','Si','No','NA','2018-06-28 07:03:00'),(47,'Edgar','Sosa Rojas','esosa@gruma.com','8110506339','GRUMA','IT Corporate Manager','MEXICO','Monterrey','G17234338','2025-05-19','Si','Si','Desktop as a service','2018-06-29 11:26:00'),(48,'Gustavo','Lima Osman','gustavo.osman@andritz.com','5511964857102','Andritz','Gerente de Seguranca da Informacao','Brasil','Barueri','FS435013','2027-01-30','Si','No','.','2018-06-29 12:29:00'),(49,'Daniel','Moreira','daniel.moreira@kof.com.mx','11993574075','Coca Cola Femsa Brasil','Gerente de IT','Brasil','São Paulo','FS458415','2018-06-03','Si','No','DAAS','2018-06-29 12:30:00'),(50,'Diego Pablo','Taurizano','subsecadmin@abc.gob.ar','54 9 11 5331-1135','DGCyE','Subsecretario Administrativo','Argentina','CABA','AAA827422','2023-04-09','Si','Si','reuniones educativas','2018-06-29 01:09:00'),(51,'João Paulo','Ribeiro Hoelz Lyrio','joao.lyrio@loreal.com','5524999694775','L´Oréal','IT Coordinator','Brazil','Rio de Janeiro','fb475668','2018-06-29','No','No','no','2018-06-29 02:46:00'),(52,'Art','Quintero','art.quintero@ephesoft.com','7143009242','Ephesoft','Channel Manager Latin America','United States','Irvine','533611211','1969-03-22','Si','No','Sergio Jose Sotero','2018-06-29 04:20:00'),(53,'Alberto','Martinez','jose.martinez@xpertal.com','5218120011006','Xpertal','Gerente de Proyectos de TI','México','Monterrey','G14335573','2020-05-08','Si','Si','Cloud','2018-06-29 04:45:00'),(54,'Mauricio','Santos','mauricio.santosb@heineken.com','8120226744','Heineken','CIO','México','Monterrey','G17491766','2018-06-15','Si','Si','Cómputo Personal, Mobilidad y Seguridad','2018-06-29 07:01:00'),(55,'Marcelo','Carvajal Monge','marcelo.carvajal@fod.ac.cr','(506), 2527-6103','Fundación Omar Dengo','Gerente General','Costa Rica','San José','111210914','2021-05-24','Si','Si','Contacto con posibles socios del sector educativo','2018-07-01 01:12:00'),(56,'Ricardo Daniel','Spartano','ricardo.spartano@educacion.gob.ar','5491144910129','Ministerio de Educacion de la nacion','Director de Gestión Informática','Argentina','Autonoma de Buenos Aires','YA5332705','2020-03-15','Si','No','Hardware','2018-07-02 11:56:00'),(57,'Kenneth','Maldonado','kenmaldonado@suagm.edu','7872950566','SUAGM','CIO','Puerto Rico','San Juan','No aplica','2020-07-20','No','No','None','2018-07-02 03:25:00'),(58,'VANESSA ALMEIDA DA SILVA','VANESSA ALMEIDA DA SILVA','vanessa.almeida@continental-corporation.com','11949551563','CONTINENTAL','Comprador','Brasil','São Paulo','FV187079','2028-02-18','Si','No','Ruedas de negocios','2018-07-02 07:35:00'),(59,'Maria Victoria','Corral Otálora','mvictoriacorral@comfandi.com.co','3125888161','Comfandi','TBD','Colombia','Cali','AQ806132','2025-04-07','Si','Si','NA','2018-07-03 09:55:00'),(60,'Carlos','Gonzales','cgonzalez@colvatel.com','3168346031','Covatel','Presidente','Colombia','Bogotá','TBD1234567','2019-07-03','Si','No','NA','2018-07-03 09:59:00'),(61,'Nibaldo','Toledo','ntoledo@azteca-comunicaciones.com','?(300), 897-7568?','Azteca Comunicaciones','Presidente','Colombia','Bogotá','TBD1234567','2019-07-03','Si','Si','NA','2018-07-03 10:01:00'),(62,'Gabriel','Giraldo','gabriel.giraldo@atento.com','?(313), 397-0033?','Atento','Director general compras IT','Colombia','Bogotá','TBD1234567','2019-07-03','Si','Si','NA','2018-07-03 10:02:00'),(63,'German','Castro','german.castro@lrs.com','573153054753','LRS','Alliance Manager South America','Colombia','Bogota','AN558719','2022-02-02','Si','No','No Aplica','2018-07-03 12:13:00'),(64,'Leonel David','Iankelevich','liankelevich@santanderrio.com.ar','1565286298','Banco Santander Rio','Gestión Proyectos de TI','Argentina','CABA','XDC363146','2026-03-22','Si','No','NULL','2018-07-03 01:57:00'),(65,'Welington','Fecchio','welington.fecchio@unilever.com','+55 11 97610 0223','Unilever Brazil Ltda','Americas Mobile & Device Service Manager','Brazil','Sao Paulo','FI894494','2018-10-10','Si','No','HP Meetings related to stragegy','2018-07-03 09:25:00'),(66,'Jair','Trujillo','jair.trujillo@femsa.com.mx','5218117992811','FEMSA Xpertal','Gerente Abastecimiento','Mexico','Monterrey','G221220275','2026-09-01','Si','Si','Negocios','2018-07-04 01:47:00'),(67,'Noe','Contreras Lopez','noe.contreras@yza.mx','8180986314','Farmacias YZA FEMSA COMERCIO','CIO','Mexico','Monterrey','G14042987','2020-03-28','Si','No','.','2018-07-04 09:07:00'),(68,'Javier Estanislao','Aleman','ealeman@aa2000.com.ar','1148526506','Aeropuertos Argentina','Director Sistemas','Argentina','Buenos Aires','AAC688346','2025-03-05','Si','No','.','2018-07-04 04:03:00'),(69,'David Carlos','Silva Perez','carlos.silva@volaris.com','-18003940','VOLARIS','CIO','Mexico','Ciudad de México','P07120128','2020-02-24','Si','Si','Innovación','2018-07-04 04:44:00'),(70,'Leonardo','Scapato','leonardo.scapato@cognizant.com','1131335716','Cognizant','Commercial Head','Argentina','CABA','AAC491476','2025-01-14','Si','Si','Empresas de Industria, no tecnología (insurance, Banking, MAnlog, etc.),','2018-07-05 08:26:00'),(71,'Edna Lucía','Prieto','ELPRIETOO@COMPENSAR.com','3153558472','Compensar','Gerente TI','Colombia','Bogotá','TBD1234567','2019-07-05','Si','No','NA','2018-07-05 11:22:00'),(72,'Fernando','Palacio','fpalacios@cafam.com.co','3153558472','Cafam','Gerente TI','Colombia','Bogotá','TBD1234567','2019-07-05','Si','No','NA','2018-07-05 11:33:00'),(73,'Olga Lucía','Calero','olcalero@colsanitas.com','3153558472','Colsánitas','Directora IT','Colombia','Bogotá','TBD1234567','2019-07-05','Si','No','NA','2018-07-05 11:34:00'),(74,'Diana','Vergara','diana.vergara@avianca.com','3153558472','Avianca','Directora TI','Colombia','Bogotá','TBD1234567','2019-07-05','Si','Si','NA','2018-07-05 11:36:00'),(75,'Jaime','Uribe','jaime.uribe@alkosto.com.co','3153558472','Corbeta','Gerente IT','Colombia','Bogotá','TBD1234567','2019-07-05','Si','Si','NA','2018-07-05 11:38:00'),(76,'Jose Carlos','Ballesteros Rodriguez','cballesteros@alpekpolyester.com','5218110662856','Alpek Polyester','Information Security & Infrastructure Manager','Mexico','Monterrey','G13513673','2020-01-31','Si','No','Gracias','2018-07-05 05:50:00'),(77,'Carlos Alberto','Rodríguez Maillard','carlos.rodriguezmaillard@solistica.com','8112556500','Solistica','Director GBS','México','Monterrey','G25836382','2027-07-13','Si','No','Leasing ','2018-07-05 11:51:00'),(78,'Jose Carlos','Paula Correia','jose.correia@nissan.com.br','+55 41 991795957','Renault-Nissan-Mitsubishi','IT Sr. Manager','Brasil','Curitiba','FP610175','2026-04-11','Si','Si','Regional Sales and Operations Manager/Director','2018-07-08 06:04:00'),(79,'VICTOR','TORRES','victor_torres@coomeva.com.co','3155695502','COOMEVA SERVICIOS ADMINISTRATIVOS','Gerente General','Colombia','Cali','AR672812','2025-10-05','Si','Si',' Innovación en servicios y soluciones','2018-07-09 09:40:00'),(80,'luis','Vidaurreta','lvidaurreta@ext.sancorseguros.com','541161552804','Sancor Seguros','Gerente de Proyectos','Argentina','Sunchales','AAB357802','2023-10-25','Si','Si','a definir de acurdo a los temas y profundiada de los temas','2018-07-09 12:41:00'),(81,'David','Rodriguez','drodriguez@contentobps.com','3206968565','Contentobps','CFO','Colombia','Medellin','AO375860','2023-02-15','Si','Si','BI Analitic, Big Data','2018-07-09 02:36:00'),(82,'Ramiro','de la Rosa Hernández','ramiro.delarosa@cemex.com','+52(81),12126092','Cemex','Collaboration & End User Services Manager - Proces','México','Monterrey','G04679101','2018-07-01','Si','No','.','2018-07-09 03:44:00'),(83,'Sergio','Moya de la Lanza','sergio.moya@kof.com.mx','+52 1 55 5412 1466','Coca Cola FEMSA','CIO LATAM','México','Mexico City','G29028093','2024-04-03','Si','No','Las que pudieran aparecer importantes','2018-07-09 04:56:00'),(84,'Luis Carlos','Silva Lopez','luis.silva@solistica.com','8115313902','SOLISTICA','GLOBAL CATEGORY MANAGER','MEXICO','MONTERREY','G28951962','2021-04-05','Si','Si','EMPRESAS DEL SECTOR','2018-07-09 08:52:00'),(85,'RAFAEL','BERNAL','rbernal@elektra.com.mx','5215530171083','GRUPO ELEKTRA','DIRECTOR SOPORTE TECNICO','MEXICO','CDMX','G09009929','2022-03-22','Si','Si','Ejecutivos de HP Responsables de la estrategia de Servidores y Estaciones de Trabajo.','2018-07-10 09:00:00'),(86,'Adrian','Muñoz','amunozb@interior.gov.cl','56995425310','Ministerior del Interior','Jefe Soporte TI','Chile','Santiago','14126243-2','2023-07-09','Si','Si','soluciones de seguridad y equipamiento ','2018-07-10 02:10:00'),(87,'Raul','Palza','raul.palza@unacem.com.pe','51989306282','UNACEM S.A.A.','Gerente TIC','Peru','Lima','116375318','2022-07-03','Si','No','Tecnología IoT (Microsoft -  HP -SAP),','2018-07-10 06:18:00'),(88,'Ricardo Fabio','GIACOBBE','giacobbericardo@gmail.com','0221-155040470','Registro Nacional de las Personas','Subdirector Nacional','Argentina','CABA','AAD772027','2026-01-20','No','Si','Visa en trámite ','2018-07-11 07:39:00'),(89,'Rene','Alfaro','rene.alfaro@grupocassa.com','78518149','Grupo CASSA','Director de Tecnologia de Información','El Salvador','San Salvador','A01775946','2021-05-02','Si','Si','?','2018-07-11 02:11:00'),(90,'Sergio','Sotelo','sergio.jos.sotelo@hp.com','573175138417','HP Inc','Solution Specialist','Colombia','Bogota','115322169','2020-02-05','Si','Si','Clientes que lo requieran','2018-07-12 10:39:00'),(91,'JESSICA JEANETTE','ALARCON GARZA','jessica.alarcon@solistica.com','8120713887','SOLISTICA','CATEGORY EXPERT TI PROCUREMENT','MEXICO','NUEVO LEON','G09843100','2028-08-09','Si','No','X','2018-07-12 03:21:00'),(92,'Enrique','Martinez Trejo','emartinezt@elektra.com.mx','52-5530171167','Grupo Elektra','Gerente de Evaluacion Tecnologica','MEXICO','Ciudad de Mexico','G05513262','2020-08-19','Si','Si','ruedas con negocios','2018-07-12 10:59:00'),(93,'Julio Cesar','Abanto Palomino','jabanto@ulima.edu.pe','987411996','Universidad de Lima','Jefe del Departamento de Servicios TI','Perú','Lima','1','1974-03-11','Si','Si','Blockchain, 4ta Revolución Industrial.','2018-07-13 10:20:00'),(94,'Carlos','Ricardo','carlosandaline@gmail.com','8583423219','HP','Head of Print Marketing Americas','United States','San Diego','YC660604','2028-04-18','Si','Si','Interese en conocer las necesidades de marketing de nuestros retail partners','2018-07-13 12:27:00'),(95,'Jose','Gomez','agomez@cinemarkca.com','50378533937','Cinemark Centro America y Caribe','Gerente Regional de TI','El Salvador','Antiguo Cuscatlan','A02392431','2019-01-18','Si','Si','Equipos Todo en Uno para Interperie','2018-07-13 12:35:00'),(96,'Leda','Muñoz','leda.munoz@fod.ac.cr','506 8384 9140','Fundación Omar Dengo','Directora Ejcutiva','Costa Rica','San José','561316503','2018-07-17','Si','No','ninguna','2018-07-13 02:28:00'),(97,'Alejandro','Ramirez','abeltran@bancoazteca.com.mx','5215530095499','Grupo Salinas','IT Manager LatinAmerica','Mexico','CDMX','G11166757','2019-01-10','Si','Si','NIveles de Servicio , propuestas de tecnologia aplicadas a nuestro negocio','2018-07-13 02:52:00'),(98,'Celeste Soledad','Petringa','petringacs@gmail.com','011-1523081695','Renaper','Asesora','Argentina','Buenos Aires','AAD957221','2026-04-29','Si','Si','. ','2018-07-13 03:22:00'),(99,'Jaime','Peña Espinosa','Jaime.pena@mindefensa.gov.co','315 0111 Ext 40857','Jaime.pena@mindefensa.gov.co','IT','Colombia','Bogotá','JP1558468','2025-09-24','Si','Si','Na','2018-07-13 05:44:00'),(100,'Leandro Isaac','Melamed','limelamed@gmail.com','5491154255053','Asociación ORT Argentina','Jefe de Compras','Argentina','Ciudad autonoma de Buenos Aires','26365294N','2022-05-25','Si','No','Ninguna en especial','2018-07-14 01:18:00'),(101,'Ernesto','Blanco','ernesto.blanco@hp.com','(54911), 50629750','HP Argentina','End User Sales Manager','Argentina','Buenos Aires','No lo tengo conmigo ahora','2019-07-03','Si','No','No.','2018-07-16 07:45:00'),(102,'Yaniselly','Cueto','ycueto@globalbank.com.pa','(507), 67807699','Global Bank Corp.','VPA Infraestructura y Operaciones de TI','Panama','Panama','PA0085662','2019-10-27','Si','No','Con empresas del Sector Bancario en temas de Seguridad','2018-07-16 05:19:00'),(103,'marcelo','Olguin','molguin@metlife.cl','56982992304','Metlife','CTO','Chile','Santiago','P03489290','2019-08-21','Si','Si','Cloud y Digitalizacion','2018-07-17 06:34:00'),(104,'John Fernando','Londoño Restrepo.','John.londono@confiar.com.co','311 747.53.37','CONFIAR Cooperativa Financiera.','Gerente de Informática.','Colombia','Bogotá','AP912402','2024-07-30','Si','Si','Estoy interesado pero no conozco la agenda','2018-07-17 12:05:00'),(105,'Gloria Yaneth','Franco Rua','gloria.franco@crystal.com.co','3104422423','Crystal SAS','Jefe Infraestructura y Operaciones','Colombia','Sabaneta','AQ180701','2024-10-22','Si','Si','Empresas de retail','2018-07-17 07:09:00'),(106,'Alexandra','Olmos Mora','aolmos@azteca-comunicaciones.com','316 3502644','Azteca Comunicaciones','Directora IT Gobierno','Colombia','Bogotá','AO931275','2023-09-04','Si','Si','No conozco la agenda','2018-07-18 10:13:00'),(107,'Juan','Iparraguirre','juanm@hyland.com','+1 (216), 233-7379','Hyland Software','Latin America Sales Manager','USA','Westlake, OH','116269690','2018-01-22','Si','Si','Con clientes/prospectos HP+Hyland','2018-07-18 11:16:00'),(108,'Guillermo','Bogado','guillermo.c.bogado@accenture.com','+54911 53288546','Accenture','CIO LATAM','Argentina','Buenos Aires','20250862N','2021-03-15','Si','Si','HP con alcance LATAM (productos, especialmente pcs)','2018-07-18 02:27:00'),(109,'Alejandra','Aguirre','alejandra.aguirre@hp.com','NULL','HP','NULL','NULL','NULL','NULL','2018-10-02','Si','NULL','NULL','2018-10-02 07:57:00'),(110,'Levi','Oliveira','levi.oliveira@kof.com.mx','NULL','KOF','NULL','NULL','NULL','NULL','2018-10-02','Si','NULL','NULL','2018-10-02 08:33:00'),(111,'Rodrigo','Loyola','rloyola@unab.cl','998209941','Universidad Andrés Bello','CIO','Chile','Santiago','P07735843','2019-05-28','Si','NULL','NULL','2018-05-18 08:36:00'),(112,'Rodrigo','Loyola','rloyola@unab.cl','998209941','Universidad Andrés Bello','CIO','Chile','Santiago','P07735843','2019-05-28','Si','NULL','NULL','2018-05-18 08:36:00');
/*!40000 ALTER TABLE `asistentes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `expositor`
--

DROP TABLE IF EXISTS `expositor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `expositor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) DEFAULT NULL,
  `tema` varchar(200) DEFAULT NULL,
  `foto` varchar(200) DEFAULT NULL,
  `dia` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `expositor`
--

LOCK TABLES `expositor` WRITE;
/*!40000 ALTER TABLE `expositor` DISABLE KEYS */;
INSERT INTO `expositor` VALUES (1,'Vincent Gaillardot','Entrevistas y lecciones de Renault','Vincent_Gaillardot.jpg','1'),(2,'Jason O’keeffe','Tendencias en seguridad y cómo proteger a tu empresa','Jason_O_keeffe.jpg','1'),(3,'Jorge Hurtado','Soluciones de administración de Absolute','Jorge_Hurtado.jpg','1'),(4,'Luis Rico','Device as a Server: La solución total de PC para su empresa','Luis_Rico.jpg','1'),(5,'Alfredo Cors','Estrategia de HP para Impresión','Alfredo_Cors.jpg','1'),(6,'Kevin Rausch','Demostraciones de Prototipos de la impresión del futuro','Kevin_Rausch.jpg','1'),(7,'Carlos Andres Velasquez','Tendencias en servicios gestionados y soluciones para su empresa','Carlos_Andres_Velasquez.jpg','1'),(8,'Sergio Sotelo','Product and Solution Showcase Print','Vincent_Gaillardot.jpg','1'),(9,'Ephesoft','Product and Solution Showcase Print','Ephesoft.jpg','1'),(10,'Hyland','teProduct and Solution Showcase Printma','Hyland.jpg','1'),(11,'LRS','Product and Solution Showcase Print','LRS.jpg','1'),(12,'Christine Hawkins','Mega tendencias en tecnología','Christine_Hawkins.jpg','2'),(13,'John Snaider','La oficina del futuro e innovación del portafolio HP','John_Snaider.jpg','2'),(14,'Bruce Michelson','La oficina del futuro e innovación del portafolio HP','Bruce_Michelson.jpg','2'),(15,'Alvaro Celi (Microsoft)','Solutions Innovation Microsoft','Alvaro_Celi_Microsoft.jpg','2'),(16,'Marcelo Vertolami (Intel)','Solutions Innovation Intel','Marcelo_Vertolami_Intel.jpg','2'),(17,'Kate (Dreamworks)','Tecnology use Case','Kate_Dreamworks.jpg','2'),(18,'Max (Notebooks)','Product and Solution Showcase PC','Max_Notebooks.jpg','2'),(19,'Carlos (Desktops)','Product and Solution Showcase PC','Carlos_Desktops.jpg','2'),(20,'Daniel (RPOS)','Product and Solution Showcase PC','Daniel_RPOS.jpg','2'),(21,'Manny R (Workstations)','Product and Solution Showcase PC','Manny_R_Workstations.jpg','2');
/*!40000 ALTER TABLE `expositor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pregunta_suelta`
--

DROP TABLE IF EXISTS `pregunta_suelta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pregunta_suelta` (
  `id_asistente` int(11) NOT NULL,
  `preg1a` int(11) NOT NULL,
  `preg1b` int(11) NOT NULL,
  `preg1c` int(11) NOT NULL,
  `preg2` varchar(150) NOT NULL,
  `preg3` varchar(150) NOT NULL,
  `preg4a` varchar(25) NOT NULL,
  `preg4b` varchar(25) NOT NULL,
  `preg5` varchar(5) NOT NULL,
  `preg6` varchar(5) NOT NULL,
  `preg7` varchar(5) NOT NULL,
  `fecreg` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pregunta_suelta`
--

LOCK TABLES `pregunta_suelta` WRITE;
/*!40000 ALTER TABLE `pregunta_suelta` DISABLE KEYS */;
/*!40000 ALTER TABLE `pregunta_suelta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `voto_x_expositor`
--

DROP TABLE IF EXISTS `voto_x_expositor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `voto_x_expositor` (
  `id_asistente` int(11) NOT NULL,
  `id_expositor` int(11) NOT NULL,
  `puntuacion` int(11) NOT NULL,
  `fecreg` date NOT NULL,
  `fecmod` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `voto_x_expositor`
--

LOCK TABLES `voto_x_expositor` WRITE;
/*!40000 ALTER TABLE `voto_x_expositor` DISABLE KEYS */;
/*!40000 ALTER TABLE `voto_x_expositor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `lacf2018`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `lacf2018` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `lacf2018`;
